package com.example.raviprmr21.hub20.Network;

public class NativeReceiver extends Thread {
    private OnReceiveListener listener;
    private int port;

    public interface OnReceiveListener {
        void onError(String str);

        void print(String str);

        void setPid(String str);

        void stop();

        void wasStartedBefore();
    }

    public NativeReceiver(int port) {
        this.port = port;
    }

    public void kill() {
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
        r10 = this;
        r9 = 1024; // 0x400 float:1.435E-42 double:5.06E-321;
        r8 = 1;
        r3 = new java.net.DatagramPacket;	 Catch:{ IOException -> 0x0070 }
        r6 = 4;
        r6 = new byte[r6];	 Catch:{ IOException -> 0x0070 }
        r7 = 4;
        r3.<init>(r6, r7);	 Catch:{ IOException -> 0x0070 }
        r2 = new java.net.DatagramPacket;	 Catch:{ IOException -> 0x0070 }
        r6 = 1024; // 0x400 float:1.435E-42 double:5.06E-321;
        r6 = new byte[r6];	 Catch:{ IOException -> 0x0070 }
        r7 = 1024; // 0x400 float:1.435E-42 double:5.06E-321;
        r2.<init>(r6, r7);	 Catch:{ IOException -> 0x0070 }
        r4 = new java.net.DatagramSocket;	 Catch:{ IOException -> 0x0070 }
        r6 = r10.port;	 Catch:{ IOException -> 0x0070 }
        r4.<init>(r6);	 Catch:{ IOException -> 0x0070 }
        r1 = 0;
    L_0x001f:
        r4.receive(r3);	 Catch:{ IOException -> 0x0070 }
        r6 = r3.getData();	 Catch:{ IOException -> 0x0070 }
        r5 = com.myprog.netutils.Utils.byte_to_int(r6);	 Catch:{ IOException -> 0x0070 }
        if (r5 <= r9) goto L_0x0042;
    L_0x002c:
        r6 = r10.listener;	 Catch:{ IOException -> 0x0070 }
        if (r6 == 0) goto L_0x0035;
    L_0x0030:
        r6 = r10.listener;	 Catch:{ IOException -> 0x0070 }
        r6.wasStartedBefore();	 Catch:{ IOException -> 0x0070 }
    L_0x0035:
        r6 = r10.listener;	 Catch:{ IOException -> 0x0070 }
        if (r6 == 0) goto L_0x003e;
    L_0x0039:
        r6 = r10.listener;	 Catch:{ IOException -> 0x0070 }
        r6.stop();	 Catch:{ IOException -> 0x0070 }
    L_0x003e:
        r4.close();	 Catch:{ IOException -> 0x0070 }
    L_0x0041:
        return;
    L_0x0042:
        r2.setLength(r5);	 Catch:{ IOException -> 0x0070 }
        r4.receive(r2);	 Catch:{ IOException -> 0x0070 }
        r1 = new java.lang.String;	 Catch:{ IOException -> 0x0070 }
        r6 = r2.getData();	 Catch:{ IOException -> 0x0070 }
        r7 = 0;
        r1.<init>(r6, r7, r5);	 Catch:{ IOException -> 0x0070 }
        r0 = r1.length();	 Catch:{ IOException -> 0x0070 }
        if (r0 <= r8) goto L_0x0072;
    L_0x0058:
        r6 = 0;
        r6 = r1.charAt(r6);	 Catch:{ IOException -> 0x0070 }
        r7 = 126; // 0x7e float:1.77E-43 double:6.23E-322;
        if (r6 != r7) goto L_0x0072;
    L_0x0061:
        r6 = r10.listener;	 Catch:{ IOException -> 0x0070 }
        if (r6 == 0) goto L_0x001f;
    L_0x0065:
        r6 = r10.listener;	 Catch:{ IOException -> 0x0070 }
        r7 = 1;
        r7 = r1.substring(r7);	 Catch:{ IOException -> 0x0070 }
        r6.setPid(r7);	 Catch:{ IOException -> 0x0070 }
        goto L_0x001f;
    L_0x0070:
        r6 = move-exception;
        goto L_0x0041;
    L_0x0072:
        if (r0 <= r8) goto L_0x008c;
    L_0x0074:
        r6 = 0;
        r6 = r1.charAt(r6);	 Catch:{ IOException -> 0x0070 }
        r7 = 64;
        if (r6 != r7) goto L_0x008c;
    L_0x007d:
        r6 = r10.listener;	 Catch:{ IOException -> 0x0070 }
        if (r6 == 0) goto L_0x0035;
    L_0x0081:
        r6 = r10.listener;	 Catch:{ IOException -> 0x0070 }
        r7 = 1;
        r7 = r1.substring(r7);	 Catch:{ IOException -> 0x0070 }
        r6.onError(r7);	 Catch:{ IOException -> 0x0070 }
        goto L_0x0035;
    L_0x008c:
        if (r0 <= 0) goto L_0x0097;
    L_0x008e:
        r6 = 0;
        r6 = r1.charAt(r6);	 Catch:{ IOException -> 0x0070 }
        r7 = 35;
        if (r6 == r7) goto L_0x0035;
    L_0x0097:
        r6 = r10.listener;	 Catch:{ IOException -> 0x0070 }
        if (r6 == 0) goto L_0x001f;
    L_0x009b:
        r6 = r10.listener;	 Catch:{ IOException -> 0x0070 }
        r6.print(r1);	 Catch:{ IOException -> 0x0070 }
        goto L_0x001f;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.myprog.netutils.NativeReceiver.run():void");
    }

    public void setReceiveListener(OnReceiveListener l) {
        this.listener = l;
    }
}
