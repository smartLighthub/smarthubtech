package com.example.raviprmr21.hub20.recreation;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.raviprmr21.hub20.R;

/**
 * Created by raviprmr21 on 3/6/17.
 */

public class recreationMain  extends android.support.v4.app.Fragment implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        final View rootView = getActivity().getLayoutInflater()
                .cloneInContext(new ContextThemeWrapper(getActivity(), R.style.Recreation_Style))
                .inflate(R.layout.recreation_main, container, false);
        return rootView;

    }

    public void onStart() {
        super.onStart();
        ImageView xylophoneView = (ImageView)getActivity().findViewById(R.id.XylophoneIcon);
        ImageView shakeView =(ImageView)getActivity().findViewById(R.id.ShakeIcon);
        ImageView alarmView =(ImageView)getActivity().findViewById(R.id.AlarmClockIcon);
        ImageView clapView =(ImageView)getActivity().findViewById(R.id.clapIcon);
        ImageView microphoneView =(ImageView)getActivity().findViewById(R.id.MicrophoneIcon);
        ImageView moreView =(ImageView)getActivity().findViewById(R.id.MoreIcon);

        xylophoneView.setOnClickListener(this);
        shakeView.setOnClickListener(this);
        alarmView.setOnClickListener(this);
        clapView.setOnClickListener(this);
        microphoneView.setOnClickListener(this);
        moreView.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch(v.getId()) {
            case  R.id.XylophoneIcon:
                 Intent xylophone = new Intent(getActivity(),xylophone.class);
                 startActivity(xylophone);
               break;

            case  R.id.ShakeIcon:
                Intent shake = new Intent(getActivity(),shake.class);
                startActivity(shake);
                break;
            case  R.id.AlarmClockIcon:
                Intent alarmclock = new Intent(getActivity(),alarm.class);
                startActivity(alarmclock);
                break;
            case  R.id.clapIcon:
                Intent clap = new Intent(getActivity(),clap.class);
                startActivity(clap);
                break;
            case  R.id.MoreIcon:
                more dialogFragment = new more();
                dialogFragment.show(getActivity().getFragmentManager(), "ipAddressSelect");
                break;
            case  R.id.MicrophoneIcon:
                Intent microphone = new Intent(getActivity(),microphone.class);
                startActivity(microphone);
                break;

        }

    }
}