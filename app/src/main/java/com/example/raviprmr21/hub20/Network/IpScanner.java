package com.example.raviprmr21.hub20.Network;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;

import com.example.raviprmr21.hub20.BuildConfig;
import com.example.raviprmr21.hub20.R;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class IpScanner {
    private int LINK_MODE = 0;
    int LOOPS = 1;
    private int NS_TIMEOUT = 750;
    String PING_PATH = "ping";
    private int[] PORTS = null;
    private int PORTSCAN_FILTER = 0;
    private int PORTS_TM = 300;
    int REACHABLE_TIMEOUT = 1;
    private boolean RESOLVE_MAC = false;
    private boolean RESOLVE_NAME = false;
    int SCAN_TIMEOUT = 2000;
    int THREADS = 64;
    private boolean USE_NETBIOS = true;
    boolean WAS_STARTED = false;
    protected Context context;
    ThreadConveyor conveyor;
    private String gateway;
    private Linker linker = null;
    private static onUpdateListener listener = null;
    protected final Object lock = new Object();
    String my_ip;
    byte[] my_ipByte;
    String mymac;
    long start_ip = Long.MAX_VALUE;

    private long start_lan = Long.MAX_VALUE;
    long stop_ip = Long.MAX_VALUE;
    private long stop_lan = Long.MAX_VALUE;
    private Vendors vendorResolve = null;

    public interface onUpdateListener {
        void onUpdate(String str, String str2, String str3, String str4);
    }

    private class CheckPorts {
        private ArrayList<Integer> PORTS_RES;

        private CheckPorts() {
            this.PORTS_RES = new ArrayList();
        }

        public int[] scanPorts(String host, int time) {
            int[] iArr = null;
            if (IpScanner.this.PORTS != null) {
                int i;
                for (int port : IpScanner.this.PORTS) {
                    try {
                        Socket s = new Socket();
                        s.connect(new InetSocketAddress(host, port), time);
                        if (s.isConnected()) {
                            this.PORTS_RES.add(Integer.valueOf(port));
                            s.close();
                        }
                    } catch (Exception e) {
                    }
                }
                if (this.PORTS_RES.size() != 0) {
                    Collections.sort(this.PORTS_RES);
                    iArr = new int[this.PORTS_RES.size()];
                    int len = this.PORTS_RES.size();
                    for (i = 0; i < len; i++) {
                        iArr[i] = ((Integer) this.PORTS_RES.get(i)).intValue();
                    }
                }
            }
            return iArr;
        }
    }

    protected class ResolveThread extends Thread {
        private String ip;
        private String mac;
        private String name;

        ResolveThread(String ip, String mac, String name) {
            this.ip = ip;
            this.mac = mac;
            this.name = name;
        }

        @SuppressLint("PrivateResource")
        public void run() {
            String ports = BuildConfig.FLAVOR;
            int[] i_ports = IpScanner.this.scanPorts(this.ip);
            switch (IpScanner.this.PORTSCAN_FILTER) {
                case R.styleable.View_android_focusable /*1*/:
                    if (i_ports == null) {
                        return;
                    }
                    break;
                case R.styleable.View_paddingStart /*2*/:
                    if (!IpScanner.this.check_ports_count(i_ports)) {
                        return;
                    }
                    break;
            }
            if (i_ports != null) {
                ports = "Ports: " + IpScanner.this.ports_to_string(i_ports);
            }
            long ip_now = Utils.str_to_ip(this.ip);
            boolean in_lan = ip_now >= IpScanner.this.start_lan && ip_now <= IpScanner.this.stop_lan;
            char icon = 'o';
            String vendor = "Vendor";
            String linkstr = BuildConfig.FLAVOR;
            if (IpScanner.this.RESOLVE_MAC && in_lan && this.mac.isEmpty()) {
                if (this.ip.equals(IpScanner.this.my_ip)) {
                    this.mac = IpScanner.this.mymac;
                } else {
                    this.mac = Utils.get_arp_mac(this.ip);
                }
            }
            switch (IpScanner.this.LINK_MODE) {
                case R.styleable.View_android_theme /*0*/:
                    linkstr = this.mac;
                    break;
                case R.styleable.View_android_focusable /*1*/:
                    linkstr = this.ip;
                    break;
            }
            if (in_lan) {
                if (this.ip.equals(IpScanner.this.gateway)) {
                    icon = '1';
                } else if (this.ip.equals(IpScanner.this.my_ip)) {
                    icon = '0';
                } else {
                    icon = '2';
                }
            }
            if (!linkstr.isEmpty()) {
                icon = IpScanner.this.linker.get_icon(linkstr, icon);
            }
            if (IpScanner.this.RESOLVE_NAME && this.name.isEmpty()) {
                if (!IpScanner.this.linker.contains_name(linkstr)) {
                    this.name = Utils.get_hostname(this.ip, IpScanner.this.USE_NETBIOS, IpScanner.this.NS_TIMEOUT);
                    if (this.ip.equals(IpScanner.this.my_ip)) {
                        this.name += " (This device)";
                    } else if (this.ip.equals(IpScanner.this.gateway)) {
                        this.name += " (Gateway)";
                    }
                } else if (!linkstr.isEmpty()) {
                    this.name = IpScanner.this.linker.get_name(linkstr, this.ip);
                }
            }
            if (IpScanner.this.RESOLVE_MAC && !this.mac.isEmpty()) {
                if (!this.mac.equals(IpScanner.this.mymac)) {
                    vendor = IpScanner.this.vendorResolve.get_vendor_by_mac(this.mac);
                }
                else {
                    vendor = !this.mac.equals(IpScanner.this.mymac) ? IpScanner.this.vendorResolve.get_vendor_by_mac(this.mac) : Utils.get_my_vendor();
                }
            }
            synchronized (IpScanner.this.lock) {
                if (listener != null) {
                    listener.onUpdate(this.ip, this.mac, this.name, vendor);
                }
            }
        }
    }

    public void setOnUpdateListener(onUpdateListener l) {
        listener = l;
    }

    IpScanner(Context ctx) {
        this.context = ctx;
    }

    public void configureArp(int loops, int reach_timeout, int scan_timeout) {
        this.LOOPS = loops;
        this.REACHABLE_TIMEOUT = reach_timeout;
        this.SCAN_TIMEOUT = scan_timeout;
    }

    public void configureResolver(int ns_timeout, boolean use_netbios) {
        this.NS_TIMEOUT = ns_timeout;
        this.USE_NETBIOS = use_netbios;
        this.RESOLVE_NAME = true;
        this.RESOLVE_MAC = true;
    }

    public void configureLinker(int link_mode) {
        this.LINK_MODE = link_mode;
    }

    public void start_scan() {
        long[] range = Utils.get_lan_range(this.context);
        if (range != null) {
            start_scan(range[0], range[1]);
        }
    }

    public void start_scan(String start, String stop) {
        start_scan(Utils.str_to_ip(start), Utils.str_to_ip(stop));
    }

    protected void start_scan(long start, long stop) {
        this.start_ip = start;
        this.stop_ip = stop;
        long[] r = Utils.get_lan_range(this.context);
        if (r != null) {
            this.start_lan = r[0];
            this.stop_lan = r[1];
        }
        try {
            Context context = this.context;
            Context context2 = this.context;
            WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            this.my_ipByte = Utils.ip_to_byte((long) wm.getConnectionInfo().getIpAddress());
            this.my_ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
            this.gateway = Formatter.formatIpAddress(wm.getDhcpInfo().gateway);
            this.mymac = Utils.get_my_mac(this.context);
        } catch (Exception e) {
            this.my_ipByte = Utils.ip_to_byte(0);
            this.my_ip = "255.255.255.255.1";
            this.gateway = "255.255.255.255.1";
            this.mymac = "ff:ff:ff:ff:ff:ff:1";
        }
        this.vendorResolve = new Vendors(this.context);
        this.linker = new Linker(this.context, this.LINK_MODE);
        this.WAS_STARTED = true;
    }

    protected void stop_scan() {
        this.WAS_STARTED = false;
        this.USE_NETBIOS = false;
        try {
            Thread.currentThread().interrupt();
        } catch (Exception e) {
        }
    }

    public void setScanPorts(int[] ports, int tm, int view_mode) {
        this.PORTS = (int[]) ports.clone();
        Arrays.sort(this.PORTS);
        this.PORTS_TM = tm;
        this.PORTSCAN_FILTER = view_mode;
    }

    private int[] scanPorts(String host) {
        if (this.PORTS == null) {
            return null;
        }
        return new CheckPorts().scanPorts(host, this.PORTS_TM);
    }

    private String ports_to_string(int[] ports) {
        if (ports == null) {
            return BuildConfig.FLAVOR;
        }
        String res = BuildConfig.FLAVOR;
        for (int num : ports) {
            res = res + Integer.toString(num) + " ";
        }
        return res;
    }

    private boolean check_ports_count(int[] ports) {
        if (ports == null || this.PORTS == null || ports.length != this.PORTS.length) {
            return false;
        }
        for (int i = 0; i < ports.length; i++) {
            if (this.PORTS[i] != ports[i]) {
                return false;
            }
        }
        return true;
    }
}
