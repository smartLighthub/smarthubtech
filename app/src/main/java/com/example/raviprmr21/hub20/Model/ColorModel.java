package com.example.raviprmr21.hub20.Model;

import java.io.Serializable;

/**
 * Created by ravi_ on 11-08-2017.
 */

public class ColorModel implements Serializable {

    private int Red;
    private int Green;
    private int Blue;
    private String ColorData;
    private  int touchRGBDATA;
    public  int alpha;


    public int getRed() {
        return Red;
    }

    public void setRed(int red) {
        Red = red;
    }

    public int getGreen() {
        return Green;
    }

    public void setGreen(int green) {
        Green = green;
    }

    public int getBlue() {
        return Blue;
    }

    public void setBlue(int blue) {
        Blue = blue;
    }

    public String getColorData() {
        return ColorData;
    }

    public void setColorData(String colorData) {
        ColorData = colorData;
    }

    public int getTouchRGBDATA() {
        return touchRGBDATA;
    }

    public void setTouchRGBDATA(int touchRGBDATA) {
        this.touchRGBDATA = touchRGBDATA;
    }

    public int getAlpha() {
        return alpha;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

}
