package com.example.raviprmr21.hub20.Network;

import android.content.Context;

import com.example.raviprmr21.hub20.BuildConfig;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class IpScannerArp extends IpScanner {
    private String my_interface = "wlan0";
    private Thread receive_thr;
    private final String toolname = "libiplist.so";

    public IpScannerArp(Context ctx) {
        super(ctx);
    }

    public void stop_scan() {
        super.stop_scan();
        Shell.startCommand(this.context.getApplicationInfo().nativeLibraryDir + "/libkillall.so -SIGINT " + "libiplist.so" + "\n");
        try {
            this.receive_thr.interrupt();
        } catch (Exception e) {
        }
    }

    public void start_scan(long start, long stop) {
        super.start_scan(start, stop);
        this.conveyor = new ThreadConveyor(this.THREADS);
        this.conveyor.start();
        start();
        try {
            this.receive_thr.join();
        } catch (InterruptedException e) {
        }
        this.conveyor.joinAll();
        this.WAS_STARTED = false;
    }

    public void start() {
        start_thread();
        start_native_app();
    }

    private void start_native_app() {
        this.my_interface = this.context.getSharedPreferences("settings", 0).getString("my_interface", "wlan0");
        new Thread(new Runnable() {
            public void run() {
                if (!Shell.startCommand((IpScannerArp.this.context.getApplicationInfo().nativeLibraryDir + "/" + "libiplist.so") + " -i " + IpScannerArp.this.my_interface)) {
                    try {
                        byte[] message = "_exit_".getBytes();
                        DatagramPacket packet = new DatagramPacket(message, message.length, InetAddress.getByName("127.0.0.1"), 8090);
                        DatagramPacket packet_size = new DatagramPacket(Utils.ip_to_byte((long) message.length), 4, InetAddress.getByName("127.0.0.1"), 8090);
                        DatagramSocket dsocket = new DatagramSocket();
                        dsocket.send(packet_size);
                        dsocket.send(packet);
                        dsocket.close();
                    } catch (IOException e) {
                    }
                }
            }
        }).start();
    }

    private void start_thread() {
        NativeReceiver receiver = new NativeReceiver(8090);
        this.receive_thr = receiver;
        receiver.setReceiveListener(new NativeReceiver.OnReceiveListener() {
            public void setPid(String p) {
            }

            public void onError(String line) {
            }

            public void print(String line) {
                String ip = BuildConfig.FLAVOR;
                String mac = BuildConfig.FLAVOR;
                int len = line.length();
                int i = 1;
                while (i < len && line.charAt(i) != ' ') {
                    ip = ip + line.charAt(i);
                    i++;
                }
                i++;
                while (i < len && line.charAt(i) != '\n') {
                    mac = mac + line.charAt(i);
                    i++;
                }
                IpScannerArp.this.conveyor.add(new ResolveThread(ip, mac, BuildConfig.FLAVOR));
            }

            public void wasStartedBefore() {
                Shell.startCommand(IpScannerArp.this.context.getApplicationInfo().nativeLibraryDir + "/libkillall.so -SIGINT " + "libiplist.so" + "\n");
            }

            public void stop() {
            }
        });
        receiver.start();
    }
}
