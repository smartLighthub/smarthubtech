package com.example.raviprmr21.hub20.Settings;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.example.raviprmr21.hub20.R;
import com.example.raviprmr21.hub20.View.IpDialog;
import com.kizitonwose.colorpreference.ColorPreference;
import com.larswerkman.lobsterpicker.LobsterPicker;
import com.larswerkman.lobsterpicker.sliders.LobsterShadeSlider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;


/**
 * Created by raviprmr21 on 3/6/17.
 */

public class settingsMain extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    String PrefSummary;
    int value;
    String field;
    private int TIMEINSEC = 10000;
    private final String CallAlert_ColorKey = "callAlertColorKey";
    private final String Instagram_ColorKey = "InstagramColorKey";
    private final String SnapChat_ColorKey = "SnapChaColorKey";
    private final String Message_ColorKey = "MessageColorKey";
    private final String Facebook_ColorKey = "FacebookColorKey";
    private final String Hike_ColorKey = "HikeColorKey";
    private final String Whatsapp_ColorKey = "WhatsappColorKey";
    private final String Gmail_ColorKey = "GmailColorKey";
    private final String Messenger_ColorKey = "MessengerColorKey";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {


        //You will need the PreferenceKey for adding OnClick ////
        if (preference.getKey().equals(getResources().getString(R.string.wifi_reset_key))) {
            Toast.makeText(getActivity(), "Wifi reset", Toast.LENGTH_LONG).show();
        }
        if (preference.getKey().equals(getResources().getString(R.string.StoreKey))) {
            Toast.makeText(getActivity(), "store", Toast.LENGTH_LONG).show();
        }
        if (preference.getKey().equals(getResources().getString(R.string.InstructionKey))) {
            Toast.makeText(getActivity(), "ADD instruction", Toast.LENGTH_LONG).show();
        }
        if (preference.getKey().equals(getResources().getString(R.string.app_version_key))) {
            Toast.makeText(getActivity(), "version 1.0", Toast.LENGTH_LONG).show();
        }
//
//        /////////////////////////////on click on all the colors /////////////////////////////////////
        if (preference.getKey().equals(getResources().getString(R.string.pref_callAlertColorKey))) {
            showColorDialog(preference, CallAlert_ColorKey);
        }
        if (preference.getKey().equals(getResources().getString(R.string.pref_InstagramColorKey))) {
            showColorDialog(preference, Instagram_ColorKey);
        }
        if (preference.getKey().equals(getResources().getString(R.string.pref_SnapChatColorKey))) {
            showColorDialog(preference, SnapChat_ColorKey);
        }
        if (preference.getKey().equals(getResources().getString(R.string.pref_MessageColorKey))) {
            showColorDialog(preference,Message_ColorKey);
        }
        if (preference.getKey().equals(getResources().getString(R.string.pref_FacebookColorKey))) {
            showColorDialog(preference, Facebook_ColorKey);
        }
        if (preference.getKey().equals(getResources().getString(R.string.pref_HikeColorKey))) {
            showColorDialog(preference,Hike_ColorKey);
        }
        if (preference.getKey().equals(getResources().getString(R.string.pref_WhatsappColorKey))) {
            showColorDialog(preference,Whatsapp_ColorKey);
        }
        if (preference.getKey().equals(getResources().getString(R.string.pref_GmailColorKey))) {
            showColorDialog(preference, Gmail_ColorKey);
        }
        if (preference.getKey().equals(getResources().getString(R.string.pref_MessengerColorKey))) {
            showColorDialog(preference,Messenger_ColorKey);
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    private void showColorDialog(final Preference preference, String key) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View colorView = inflater.inflate(R.layout.dialog_color, null);

        int color = PreferenceManager.getDefaultSharedPreferences(getActivity()).getInt(key, Color.YELLOW);
        final LobsterPicker lobsterPicker = (LobsterPicker) colorView.findViewById(R.id.lobsterPicker);
        LobsterShadeSlider shadeSlider = (LobsterShadeSlider) colorView.findViewById(R.id.shadeSlider);

        lobsterPicker.addDecorator(shadeSlider);
        lobsterPicker.setColorHistoryEnabled(true);
        lobsterPicker.setHistory(color);
        lobsterPicker.setColor(color);

        new AlertDialog.Builder(getActivity())
                .setView(colorView)
                .setTitle("Choose Color")
                .setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((ColorPreference) preference).setValue(lobsterPicker.getColor());
                    }
                })
                .setNegativeButton("CLOSE", null)
                .show();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        updatePreference(findPreference(key));

    }

    private void updatePreference(Preference preference) {

        if (preference instanceof MultiSelectListPreference) {
            MultiSelectListPreference multiSelectListPreference = (MultiSelectListPreference) preference;
            if (preference.getKey().equals(getResources().getString(R.string.pref_notification_key))) {
                Log.e("multireference", Arrays.toString(new Set[]{multiSelectListPreference.getValues()}));
                //this gets all the selected items from the NOtification
            }
        }

        if (preference instanceof ListPreference) {
            ListPreference editTextPreference = (ListPreference) preference;
            field = preference.getKey();
            value = Integer.valueOf(editTextPreference.getValue());
            PrefSummary = String.valueOf(editTextPreference.getSummary());
            Log.v("Settings : ", "\n " + "FIELD : " + field + "\t" + "Value:" + value);

            if (field.equals(getResources().getString(R.string.pref_sleep_mode_key))) {
                switch (value) {
                    case 0:
                        //none sleep off  send this data right away with the help of json code
                        // disable sleep cancel the existing sleep if any
                        Toast.makeText(getActivity(), "deactivated sleep", Toast.LENGTH_LONG).show();
                        break;
                    case 1:
                        SleepTimer(5 * TIMEINSEC);  //5min
                        break;
                    case 2:
                        SleepTimer(15 * TIMEINSEC); // 15min
                        break;
                    case 3:
                        SleepTimer(30 * TIMEINSEC); // 30 min
                        break;
                    case 4:
                        SleepTimer(45 * TIMEINSEC); // 45 min
                        break;
                    case 5:
                        SleepTimer(60 * TIMEINSEC); //60 min
                        break;
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        for (int i = 0; i < getPreferenceScreen().getPreferenceCount(); ++i) {
            Preference preference = getPreferenceScreen().getPreference(i);
            updatePreference(preference);
        }
    }

    @Override
    public void onPause() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }


    private void SleepTimer(int countTimer) {

        Toast.makeText(getActivity(), "Sleep Activated", Toast.LENGTH_SHORT).show();

        new CountDownTimer(countTimer, 1000) {

            public void onTick(long millisUntilFinished) {
                Log.e("seconds remaining: ", String.valueOf(millisUntilFinished / 1000));
            }

            public void onFinish() {
                Log.e("TImes up", "Sucess");
            }
        }.start();

    }
}


