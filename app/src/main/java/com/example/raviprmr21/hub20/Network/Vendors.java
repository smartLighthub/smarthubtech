package com.example.raviprmr21.hub20.Network;

import android.content.Context;

import com.example.raviprmr21.hub20.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

class Vendors {
    private Context context;
    private ArrayList<String> macs = new ArrayList<String>();
    private ArrayList<String> masks = new ArrayList<>();
    private ArrayList<String> vendor = new ArrayList<>();

    Vendors(Context context) {
        this.context = context;
        vendor_lookup_load_tables();
    }

    String get_vendor_by_mac(String mac) {
        String subMac = mac.substring(0, 8).toUpperCase();
        int num = macs.indexOf(subMac);
        if (num != -1) {
            return get_vendor_by_num(num);
        }
        return "Unknown vendor";
    }

    private void vendor_lookup_load_tables() {
        get_macs_table();
        get_vendors_table();
    }

    private String get_vendor_by_num(int num) {
        return this.vendor.get(num);
    }

    private void get_macs_table() {
        InputStream inputstream = this.context.getResources().openRawResource(R.raw.table_macs);
        this.masks.clear();
        this.macs.clear();
        try {
            InputStreamReader inputStreamReader = new InputStreamReader(inputstream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            while (true) {
                String str = bufferedReader.readLine();
                if (str != null) {
                    this.masks.add(str);
                    this.macs.add(str);
                } else {
                    bufferedReader.close();
                    inputStreamReader.close();
                    inputstream.close();
                    return;
                }
            }
        } catch (IOException e) {
        }
    }

    private void get_vendors_table() {
        InputStream is = this.context.getResources().openRawResource(R.raw.table_vendors);
        this.vendor.clear();
        try {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader reader = new BufferedReader(isr);
            while (true) {
                String str = reader.readLine();
                if (str != null) {
                    this.vendor.add(str);
                } else {
                    reader.close();
                    isr.close();
                    is.close();
                    return;
                }
            }
        } catch (IOException e) {
        }
    }
}
