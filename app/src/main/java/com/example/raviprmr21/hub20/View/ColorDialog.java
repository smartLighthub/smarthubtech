package com.example.raviprmr21.hub20.View;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.example.raviprmr21.hub20.Model.ColorModel;
import com.example.raviprmr21.hub20.R;


/**
 * Created by raviprmr21 on 10/6/17.
 */
public class ColorDialog extends DialogFragment {
    public int touchedRGB = 0 ;
    int alphaValue,redValue,blueValue,greenValue;
    String ColorData;
    private ColorDialogListener mCallback;



    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface ColorDialogListener {
        public void onDialogPositiveClick(ColorModel model);
      //  public void onDialogNegativeClick(ColorModel model);
//        void receiveColorData(int ColorData);
        //public void onColorSelect(ColorModel model);


    }
    public void setCallBack(ColorDialogListener listener) {

        this.mCallback = listener;
    }

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener


    @Override
    public void onAttach(Context context) {
            super.onAttach(context);
        }

    @Override
    public void onStart() {
        super.onStart();
        //this statement is used to dim the background of the app when a dialog box is activated
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = null;
        if (window != null) {
            windowParams = window.getAttributes();
        }
        if (windowParams != null) {
            windowParams.dimAmount = 0.75f;
            windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            window.setAttributes(windowParams);
        }
    }
    @SuppressLint("ResourceAsColor")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.ColoePickerDialogStyle);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layout = inflater.inflate(R.layout.dialogcolor, null);
        ImageView image = layout.findViewById(R.id.dialog_imageview);
        final Button display = layout.findViewById(R.id.displayled1);
        final Button display1 = layout.findViewById(R.id.displayled2);
        final Button display2 = layout.findViewById(R.id.displayled3);
        final Button display3 = layout.findViewById(R.id.displayled4);
        final Button display4 = layout.findViewById(R.id.displayled5);
        final Button display5 = layout.findViewById(R.id.displayled6);

        image.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                float eventX = event.getX();
                float eventY = event.getY();
                float[] eventXY = new float[]{eventX, eventY};

                Matrix invertMatrix = new Matrix();
                ((ImageView) view).getImageMatrix().invert(invertMatrix);

                invertMatrix.mapPoints(eventXY);
                int x = (int) eventXY[0];
                int y = (int) eventXY[1];

                Drawable imgDrawable = ((ImageView) view).getDrawable();
                Bitmap bitmap = ((BitmapDrawable) imgDrawable).getBitmap();

                if (x < 0) {
                    x = 0;
                } else if (x > bitmap.getWidth() - 1) {
                    x = bitmap.getWidth() - 1;
                }

                if (y < 0) {
                    y = 0;
                } else if (y > bitmap.getHeight() - 1) {
                    y = bitmap.getHeight() - 1;
                }

                touchedRGB = bitmap.getPixel(x, y);

                display.setBackgroundColor(touchedRGB);
                display1.setBackgroundColor(touchedRGB);
                display2.setBackgroundColor(touchedRGB);
                display3.setBackgroundColor(touchedRGB);
                display4.setBackgroundColor(touchedRGB);
                display5.setBackgroundColor(touchedRGB);

                alphaValue = Color.alpha(touchedRGB);
                redValue = Color.red(touchedRGB);
                blueValue = Color.blue(touchedRGB);
                greenValue = Color.green(touchedRGB);

                ColorData = alphaValue+","+redValue+","+blueValue+","+greenValue;
                return true;
            }
        });

        builder.setView(layout)
                .setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                ColorDialog.this.getDialog().cancel();

                            }
                        })
                .setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                ColorModel colormodel = new ColorModel();
                                colormodel.setColorData(ColorData);
                                colormodel.setTouchRGBDATA(touchedRGB);
                                mCallback.onDialogPositiveClick(colormodel);
                        }
                        });
        return builder.create();
    }
}