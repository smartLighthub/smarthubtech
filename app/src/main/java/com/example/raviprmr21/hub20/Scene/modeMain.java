package com.example.raviprmr21.hub20.Scene;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.raviprmr21.hub20.R;

/**
 * Created by raviprmr21 on 3/6/17.
 */

public class modeMain extends android.support.v4.app.Fragment implements View.OnClickListener
{
	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.cloneInContext(new ContextThemeWrapper(getActivity(), R.style.Setting_Style))
				.inflate(R.layout.mode_main, container, false);
		Button scenebtn =(Button)v.findViewById(R.id.scene_btn);
		scenebtn.setOnClickListener(this);
		return v;
	}
	@Override
	public void onClick(View v) {
		sceneMain mSceneMain = new sceneMain();
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace(R.id.contentContainer, mSceneMain);
		fragmentTransaction.commit();
	}
}
