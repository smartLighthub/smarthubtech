package com.example.raviprmr21.hub20.Color;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.example.raviprmr21.hub20.R;

/**
 * Created by raviprmr21 on 3/6/17.
 */

public class colorMain extends android.support.v4.app.Fragment implements View.OnClickListener {

    ImageView imgSource1, imgSource2;
    Button  display,display1,display2,display3,display4,display5,happiness,joy ,
            proud ,sadness,fear ,embarrassment,worry ,anger ,upset ;
    private View view;
    private ShapeDrawable shape;
    private int touchedRGB = -1;
    SeekBar seek;
    HorizontalScrollView Emotions;
    private String LastColorData = "00000000";

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.color_main,container,false);

        imgSource1 = (ImageView) v.findViewById(R.id.imageView);
        //imgSource2 = (ImageView)findViewById(R.id.source2);
        display = (Button) v.findViewById(R.id.displayled1);
        display1 = (Button) v.findViewById(R.id.displayled2);
        display2 = (Button) v.findViewById(R.id.displayled3);
        display3 = (Button) v.findViewById(R.id.displayled4);
        display4 = (Button) v.findViewById(R.id.displayled5);
        display5 = (Button) v.findViewById(R.id.displayled6);
        seek = (SeekBar)v.findViewById(R.id.colorseekbar);
        Emotions=(HorizontalScrollView)v.findViewById(R.id.Emotions);

        happiness = (Button) v.findViewById(R.id.happiness);
        joy = (Button) v.findViewById(R.id.joy);
        proud  = (Button) v.findViewById(R.id.proud);
        sadness = (Button) v.findViewById(R.id.sadness);
        fear = (Button) v.findViewById(R.id.fear);
        embarrassment = (Button) v.findViewById(R.id.embarrassment);
        worry = (Button) v.findViewById(R.id.worry);
        anger = (Button) v.findViewById(R.id.angry);
        upset = (Button) v.findViewById(R.id.upset);

        happiness.setOnClickListener(this);
        joy.setOnClickListener(this);
        proud.setOnClickListener(this);
        sadness.setOnClickListener(this);
        fear.setOnClickListener(this);
        embarrassment.setOnClickListener(this);
        worry.setOnClickListener(this);
        anger.setOnClickListener(this);
        upset.setOnClickListener(this);

        Emotions.setHorizontalScrollBarEnabled(false);
        float[] outerR = new float[]{15, 15, 15, 15, 15, 15, 15, 15};
        shape = new ShapeDrawable(new RoundRectShape(outerR, null, null));
        LinearGradient test = new LinearGradient(0.f, 0.f, 500.f, 0.f,
                Color.BLACK, Color.WHITE, Shader.TileMode.CLAMP);
        shape.getPaint().setShader(test);
        seek.setProgressDrawable(shape);
        imgSource1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                float eventX = event.getX();
                float eventY = event.getY();
                float[] eventXY = new float[] {eventX, eventY};

                Matrix invertMatrix = new Matrix();
               ((ImageView)v).getImageMatrix().invert(invertMatrix);

                invertMatrix.mapPoints(eventXY);
                int x = (int) eventXY[0];
                int y = (int) eventXY[1];


                Drawable imgDrawable = ((ImageView)v).getDrawable();
                Bitmap bitmap = ((BitmapDrawable)imgDrawable).getBitmap();

                if(x < 0){
                    x = 0;
                }else if(x > bitmap.getWidth()-1){
                    x = bitmap.getWidth()-1;
                }

                if(y < 0){
                    y = 0;
                }else if(y > bitmap.getHeight()-1){
                    y = bitmap.getHeight()-1;
                }

                touchedRGB = bitmap.getPixel(x, y);
                String s= String.valueOf(touchedRGB);
                if (touchedRGB!= -16777216) {
                    display.setBackgroundColor(touchedRGB);
                    display1.setBackgroundColor(touchedRGB);
                    display2.setBackgroundColor(touchedRGB);
                    display3.setBackgroundColor(touchedRGB);
                    display4.setBackgroundColor(touchedRGB);
                    display5.setBackgroundColor(touchedRGB);

                    float[] outerR = new float[]{15, 15, 15, 15, 15, 15, 15, 15};
                    shape = new ShapeDrawable(new RoundRectShape(outerR, null, null));
                    LinearGradient test = new LinearGradient(0.f, 0.f, 500.f, 0.f,
                            Color.BLACK, touchedRGB, Shader.TileMode.CLAMP);
                    shape.getPaint().setShader(test);
	                seek.setProgressDrawable(shape);
                    int a = Color.alpha(touchedRGB);
                    int r = Color.red(touchedRGB);
                    int g = Color.green(touchedRGB);
                    int b = Color.blue(touchedRGB);

                    ColorData(Integer.toHexString(touchedRGB));

                }
                seek.setProgress(100);

                return true;
            }


        });

	    seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int a,rr,gg,bb;

		    @Override
		    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

			    a = Color.alpha(touchedRGB);
			    int r = Color.red(touchedRGB);
			    int g = Color.green(touchedRGB);
			    int b = Color.blue(touchedRGB);
			    rr = (progress*r)/100;
			    gg = (progress*g)/100;
			    bb = (progress*b)/100;
			    display.setBackgroundColor(Color.argb(a, rr, gg, bb));
			    display1.setBackgroundColor(Color.argb(a, rr, gg, bb));
			    display2.setBackgroundColor(Color.argb(a, rr, gg, bb));
			    display3.setBackgroundColor(Color.argb(a, rr, gg, bb));
			    display4.setBackgroundColor(Color.argb(a, rr, gg, bb));
			    display5.setBackgroundColor(Color.argb(a, rr, gg, bb));

			   // MusicData(Integer.toHexString(a)+Integer.toHexString(rr)+Integer.toHexString(gg)+Integer.toHexString(bb));
            }

		    @Override
		    public void onStartTrackingTouch(SeekBar seekBar) {

		    }

		    @Override
		    public void onStopTrackingTouch(SeekBar seekBar) {
		        ColorData(Integer.toHexString(a)+Integer.toHexString(rr)+Integer.toHexString(gg)+Integer.toHexString(bb));
            }
	    });

	    return v ;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.happiness:
                ColorData("FFFEE700");
                break;
            case R.id.joy:
                ColorData("FFFCB700");
                break;
            case R.id.proud:
                ColorData("FFF98A01");
                break;
            case R.id.sadness:
                ColorData("FF0C82F0");
                break;
            case R.id.fear:
                ColorData("FFB102EA");
                break;
            case R.id.embarrassment:
                ColorData("FFCCFE07");
                break;
            case R.id.worry:
                ColorData("FF5BAEFE");
                break;
            case R.id.angry:
                ColorData("FFFF0000");
                break;
            case R.id.upset:
                ColorData("FF4DD869");
                break;

        }
    }

    public void ColorData(String value){
        if (!LastColorData.equals(value)){
            Log.e("Color ", value);
            LastColorData=value;
        }
    }

}
