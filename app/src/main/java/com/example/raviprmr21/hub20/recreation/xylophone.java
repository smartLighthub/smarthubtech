package com.example.raviprmr21.hub20.recreation;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.example.raviprmr21.hub20.R;


/**
 * Created by raviprmr21 on 12/6/17.
 */
public class xylophone extends AppCompatActivity{
    // Helpful Constants
    final int NR_OF_SIMULTANEOUS_SOUNDS = 7;
    final float LEFT_VOLUME = 1.0f;
    final float RIGHT_VOLUME = 1.0f;
    final int NO_LOOP = 0;
    final int PRIORITY = 0;
    final float NORMAL_PLAY_RATE = 1.0f;

    // Add member variables
    private SoundPool mSoundpool;
    private int mCSoundId;
    private int mDSoundId;
    private int mESoundId;
    private int mFSoundId;
    private int mGSoundId;
    private int mASoundId;
    private int mBSoundId;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recreation_xylophone);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // Create a new SoundPool

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();
            mSoundpool = new SoundPool.Builder().setAudioAttributes(audioAttributes)
                    .setMaxStreams(NR_OF_SIMULTANEOUS_SOUNDS).build();
        }
        else {

            mSoundpool = new SoundPool(NR_OF_SIMULTANEOUS_SOUNDS, AudioManager.STREAM_MUSIC, 0);
        }


        // Load and get the IDs to identify the sounds
        mASoundId = mSoundpool.load(this,R.raw.note6_a,1);
        mBSoundId = mSoundpool.load(this,R.raw.note7_b,1);
        mCSoundId = mSoundpool.load(this,R.raw.note1_c,1);
        mDSoundId = mSoundpool.load(this,R.raw.note2_d,1);
        mESoundId = mSoundpool.load(this,R.raw.note3_e,1);
        mFSoundId = mSoundpool.load(this,R.raw.note4_f,1);
        mGSoundId = mSoundpool.load(this,R.raw.note5_g,1);
    }




    // Add the play methods triggered by the buttons

    public void akey(View view) {

        mSoundpool.play(mASoundId,LEFT_VOLUME,RIGHT_VOLUME,NO_LOOP,PRIORITY,NORMAL_PLAY_RATE);
        if (Build.VERSION.SDK_INT >= 26) {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(68,2));
        } else {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(68);
        }     }
    public  void bkey(View view){
        mSoundpool.play(mBSoundId,LEFT_VOLUME,RIGHT_VOLUME,NO_LOOP,PRIORITY,NORMAL_PLAY_RATE);
        if (Build.VERSION.SDK_INT >= 26) {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(80,2));
        } else {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(80);
        }     }
    public void ckey(View view){
        mSoundpool.play(mCSoundId,LEFT_VOLUME,RIGHT_VOLUME,NO_LOOP,PRIORITY,NORMAL_PLAY_RATE);
        if (Build.VERSION.SDK_INT >= 26) {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(18,2));
        } else {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(18);
        }
    }
    public  void dkey(View view){
        mSoundpool.play(mDSoundId,LEFT_VOLUME,RIGHT_VOLUME,NO_LOOP,PRIORITY,NORMAL_PLAY_RATE);
        Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= 26) {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(28,2));
        } else {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(28);
        }
    }
    public  void ekey(View view){
        mSoundpool.play(mESoundId,LEFT_VOLUME,RIGHT_VOLUME,NO_LOOP,PRIORITY,NORMAL_PLAY_RATE);
        if (Build.VERSION.SDK_INT >= 26) {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(38,2));
        } else {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(38);
        }
    }

    public void gkey(View view){
        mSoundpool.play(mGSoundId,LEFT_VOLUME,RIGHT_VOLUME,NO_LOOP,PRIORITY,NORMAL_PLAY_RATE);
        if (Build.VERSION.SDK_INT >= 26) {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(58,2));
        } else {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(58);
        }
    }

    public void fkey(View view) {
        mSoundpool.play(mFSoundId,LEFT_VOLUME,RIGHT_VOLUME,NO_LOOP,PRIORITY,NORMAL_PLAY_RATE);
        if (Build.VERSION.SDK_INT >= 26) {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(48,2));
        } else {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(48);
        }
    }
}

