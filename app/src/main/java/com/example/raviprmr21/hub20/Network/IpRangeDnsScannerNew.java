package com.example.raviprmr21.hub20.Network;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.example.raviprmr21.hub20.BuildConfig;

import java.net.InetAddress;
import java.util.ArrayList;

public class IpRangeDnsScannerNew extends IpScanner {

    private class ScanPingThread extends Thread {
        private long start_ip;
        private long stop_ip;

        ScanPingThread(long low_hostnum, long high_hostnum) {
            this.start_ip = low_hostnum;
            this.stop_ip = high_hostnum;
        }

        public void run() {
            while (this.start_ip < this.stop_ip && IpRangeDnsScannerNew.this.WAS_STARTED) {
                try {
                    String ip = Utils.ip_to_str(this.start_ip);
                    String name = InetAddress.getByName(ip).getHostName();
                    if (!(name.isEmpty() || ip.equals(name))) {
                        IpRangeDnsScannerNew.this.conveyor.add(new ResolveThread(ip, BuildConfig.FLAVOR, name));
                    }
                } catch (Exception ignored) {
                }
                this.start_ip++;
            }
        }
    }

    public IpRangeDnsScannerNew(Context ctx) {
        super(ctx);
    }

    public void start_scan(long start, long stop) {
        super.start_scan(start, stop);
        this.conveyor = new ThreadConveyor(this.THREADS);
        this.conveyor.start();
        reach_hosts();
        this.conveyor.joinAll();
        this.WAS_STARTED = false;
    }

    private void reach_hosts() {
        long start_ip = this.start_ip;
        long stop_ip = this.stop_ip;
        if (start_ip == Long.MAX_VALUE || stop_ip == Long.MAX_VALUE) {
            try {
                ((Activity) this.context).runOnUiThread(new Runnable() {
                    public void run() {
                        Toast t = Toast.makeText(IpRangeDnsScannerNew.this.context, "You need to enter correct IP", Toast.LENGTH_SHORT);
                        t.setGravity(17, 0, 25);
                        t.show();
                    }
                });
            } catch (NullPointerException ignored) {
            }
        } else if (stop_ip - start_ip >= 0) {
            long p = (stop_ip - start_ip) / ((long) this.THREADS);
            if (p == 0) {
                p = 1;
            }
            ArrayList<ScanPingThread> t = new ArrayList<>();
            while (start_ip < stop_ip) {
                ScanPingThread th = new ScanPingThread(start_ip, start_ip + p);
                th.start();
                t.add(th);
                start_ip += p;
            }
            for (int i = 0; i < t.size(); i++) {
                try {
                    (t.get(i)).join();
                } catch (InterruptedException ignored) {
                }
            }
        }
    }
}
