package com.example.raviprmr21.hub20.Network;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build.VERSION;
import java.io.DataOutputStream;
import java.io.IOException;

public class Shell {
    private static int FULL_PATH = 0;
    private static int SU_MODE = 0;
    private static String SU_PATH = "su";
    private static volatile int exec_err = 0;
    private static DataOutputStream os = null;
    private static Process suProcess = null;

    public static void getShell(Context context, boolean su) {
        SharedPreferences mSettings = context.getSharedPreferences("settings", 0);
        if (!mSettings.contains("su_mode")) {
            if (VERSION.SDK_INT >= 24) {
                mSettings.edit().putInt("su_mode", 1).apply();
            } else {
                mSettings.edit().putInt("su_mode", 0).apply();
            }
        }
        FULL_PATH = mSettings.getInt("su_path", 0);
        SU_MODE = mSettings.getInt("su_mode", 0);
        getShell(su);
    }

    public static boolean getShell(boolean su) {
        if (su) {
            SU_PATH = "su";
        } else {
            SU_PATH = "sh";
        }
        if (FULL_PATH == 1) {
            String path = new ShellSearch().searchBin(SU_PATH);
            if (!path.isEmpty()) {
                SU_PATH = path;
            }
        }
        if (SU_MODE == 1) {
            return true;
        }
        if (su) {
            try {
                suProcess = Runtime.getRuntime().exec(SU_PATH);
            } catch (IOException e) {
                return false;
            }
        }
        try {
            suProcess = Runtime.getRuntime().exec(SU_PATH);
        } catch (IOException e) {
            e.printStackTrace();
        }
        os = new DataOutputStream(suProcess.getOutputStream());
        return true;
    }

    public static void closeShell() {
        if (SU_MODE != 1) {
            try {
                os.close();
                suProcess.destroy();
            } catch (IOException e) {
            } catch (NullPointerException e2) {
            }
        }
    }

    public static boolean startCommand(String command) {
        try {
            if (SU_MODE != 1) {
                if (suProcess == null || os == null) {
                    if (Vals.root == 0) {
                        getShell(true);
                    } else {
                        getShell(false);
                    }
                }
                os.writeBytes(command + "\n");
                exec_err = 0;
                return true;
            } else if (Vals.root == 1) {
                Runtime.getRuntime().exec(command);
                return true;
            } else {
                Runtime.getRuntime().exec(SU_PATH + " -c " + command);
                return true;
            }
        } catch (Exception e) {
            if (SU_MODE == 0) {
                if (Vals.root == 0) {
                    getShell(true);
                } else {
                    getShell(false);
                }
            }
            exec_err++;
            if (exec_err <= 15) {
                return startCommand(command);
            }
            exec_err = 0;
            return false;
        }
    }
}
