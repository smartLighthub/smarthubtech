package com.example.raviprmr21.hub20.Music;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.audiofx.Equalizer;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.raviprmr21.hub20.MainActivity;
import com.example.raviprmr21.hub20.R;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by raviprmr21 on 26/6/17.
 */

public class EquilizerFragment  extends DialogFragment{
	private static final String TAG = "AudioFxDemo";

	private static final float VISUALIZER_HEIGHT_DIP = 50f;

	private MediaPlayer mMediaPlayer;
	private Visualizer mVisualizer;
	private Equalizer mEqualizer;
	public  int positionx;
	private LinearLayout mLinearLayout;
	private TextView mStatusTextView;
	int audiosessionId;
	private Spinner presentSpineer;
	private SeekBar BassBoostSeek,surroundSoundSeek,seekBar1,seekBar2,seekBar3,seekBar4,seekBar5;
	short numberOfFrequencyBands;
	SeekBar[] seekBarFinal = new SeekBar[5];
	float[] points;
	Context ctx;
	onCheckChangedListener mCallback;

	public interface onCheckChangedListener {
		void onCheckChanged(boolean isChecked);
	}

	public EquilizerFragment() {
		// Required empty public constructor
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		ctx = context;
		mCallback = (onCheckChangedListener) context;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_equilizer,container,false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mEqualizer = new Equalizer(0,audiosessionId);
		mEqualizer.setEnabled(true);
		presentSpineer =(Spinner)view.findViewById(R.id.spinner);

		//Custom height for popUp window
		try{
			Field popup =Spinner.class.getDeclaredField("mPopup");
			popup.setAccessible(true);
			android.widget.ListPopupWindow popupWindow =(android.widget.ListPopupWindow)popup.get(presentSpineer);
			popupWindow.setHeight(850);
		}catch (NoClassDefFoundError |ClassCastException |NoSuchFieldException| IllegalAccessException ignored){
		}
		mLinearLayout = (LinearLayout) view.findViewById(R.id.equalizerContainer);
		numberOfFrequencyBands = 5;
		points = new float[numberOfFrequencyBands];
		final short lowerEqualizerBandLevel = musicMain.mEqualizer.getBandLevelRange()[0];
		final short upperEqualizerBandLevel =musicMain.mEqualizer.getBandLevelRange()[1];

		for (short i = 0; i < numberOfFrequencyBands; i++) {
			final short equalizerBandIndex = i;
			final TextView frequencyHeaderTextView = new TextView(getContext());
			frequencyHeaderTextView.setLayoutParams(new ViewGroup.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.WRAP_CONTENT
			));
			frequencyHeaderTextView.setGravity(Gravity.CENTER_HORIZONTAL);
			frequencyHeaderTextView.setTextColor(Color.parseColor("#FFFFFF"));
			frequencyHeaderTextView.setText((musicMain.mEqualizer.getCenterFreq(equalizerBandIndex) / 1000) + "Hz");

			LinearLayout seekBarRowLayout = new LinearLayout(getContext());
			seekBarRowLayout.setOrientation(LinearLayout.VERTICAL);

			TextView lowerEqualizerBandLevelTextView = new TextView(getContext());
			lowerEqualizerBandLevelTextView.setLayoutParams(new ViewGroup.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.MATCH_PARENT
			));
			lowerEqualizerBandLevelTextView.setTextColor(Color.parseColor("#FFFFFF"));
			lowerEqualizerBandLevelTextView.setText((lowerEqualizerBandLevel / 100) + "dB");

			TextView upperEqualizerBandLevelTextView = new TextView(getContext());
			lowerEqualizerBandLevelTextView.setLayoutParams(new ViewGroup.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT
			));
			upperEqualizerBandLevelTextView.setTextColor(Color.parseColor("#FFFFFF"));
			upperEqualizerBandLevelTextView.setText((upperEqualizerBandLevel / 100) + "dB");

			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.WRAP_CONTENT
			);
			layoutParams.weight = 1;

			SeekBar seekBar = new SeekBar(getContext());
			TextView textView = new TextView(getContext());
			switch (i) {
				case 0:
					seekBar = (SeekBar) view.findViewById(R.id.seekBar1);
					textView = (TextView) view.findViewById(R.id.textView1);
					break;
				case 1:
					seekBar = (SeekBar) view.findViewById(R.id.seekBar2);
					textView = (TextView) view.findViewById(R.id.textView2);
					break;
				case 2:
					seekBar = (SeekBar) view.findViewById(R.id.seekBar3);
					textView = (TextView) view.findViewById(R.id.textView3);
					break;
				case 3:
					seekBar = (SeekBar) view.findViewById(R.id.seekBar4);
					textView = (TextView) view.findViewById(R.id.textView4);
					break;
				case 4:
					seekBar = (SeekBar) view.findViewById(R.id.seekBar5);
					textView = (TextView) view.findViewById(R.id.textView5);
					break;
			}
			seekBarFinal[i] = seekBar;
			//   seekBar.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(Color.DKGRAY, PorterDuff.Mode.SRC_IN)); this will change the seekbar color
			seekBar.setId(i);
			seekBar.setMax(upperEqualizerBandLevel - lowerEqualizerBandLevel);

			textView.setText(frequencyHeaderTextView.getText());
			textView.setTextColor(Color.WHITE);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
				textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
			}

			seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					musicMain.mEqualizer.setBandLevel(equalizerBandIndex, (short) (progress + lowerEqualizerBandLevel));
					points[seekBar.getId()] = musicMain.mEqualizer.getBandLevel(equalizerBandIndex) - lowerEqualizerBandLevel; //get values from 0 to 3000
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					presentSpineer.setSelection(0);

				}

				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {

				}
			});

		}

		setupEqualizerFxAndUI();
	}


	private void setupEqualizerFxAndUI()
	{
		ArrayList<String> equalizerPresetNames = new ArrayList<String>();
		ArrayAdapter<String> equalizerPresetSpinnerAdapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_spinner_item, equalizerPresetNames);
		equalizerPresetSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		equalizerPresetNames.add("Custom");
//        get list of the device's equalizer presets
		for (short i = 0; i < musicMain.mEqualizer.getNumberOfPresets(); i++) {
			equalizerPresetNames.add(musicMain.mEqualizer.getPresetName(i));
		}
		presentSpineer.setAdapter(equalizerPresetSpinnerAdapter);
		presentSpineer.setSelection(MainActivity.presetPos);
//        handle the spinner item selections
		presentSpineer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent,
									   View view, int position, long id) {
				((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE); //change the selected text color
				try {
					if (position != 0) {
						//first list item selected by default and sets the preset accordingly
						musicMain.mEqualizer.usePreset((short) (position-1));
						MainActivity.presetPos = position;
						short numberOfFreqBands =5;
						final short lowerEqualizerBandLevel = musicMain.mEqualizer.getBandLevelRange()[0];

						for (short i = 0; i < numberOfFreqBands; i++) {
							seekBarFinal[i].setProgress(musicMain.mEqualizer.getBandLevel(i) - lowerEqualizerBandLevel);
							points[i] = musicMain.mEqualizer.getBandLevel(i) - lowerEqualizerBandLevel;
						}
					}
				}
				catch (Exception e) {
					Log.v("ErrorUpdate","TRUE");
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
//                not used
			}
		});
	}
	@Override
	public void onStart() {
		super.onStart();
		//this statement is used to dim the background of the app when a dialog box is activated
		Window window = getDialog().getWindow();
		WindowManager.LayoutParams windowParams = null;
		if (window != null) {
			windowParams = window.getAttributes();
		}
		if (windowParams != null) {
			windowParams.dimAmount = 0.75f;
			windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
			window.setAttributes(windowParams);
			window.setLayout(1000,950); //setting up dimension can be added to Dimen in res folder
		}
		if (getDialog() == null)
		{
			setShowsDialog(false);
		}

	}
	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = new Dialog(getActivity(),R.style.EqStyle);
		final View view = getActivity().getLayoutInflater().inflate(R.layout.local_musiclist, null);

		final Drawable d = new ColorDrawable(Color.BLACK);
		d.setAlpha(100);

		if(dialog.getWindow()!=null)
			dialog.getWindow().setBackgroundDrawable(d);
			dialog.getWindow().setContentView(view);
		final WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
		params.width = WindowManager.LayoutParams.MATCH_PARENT;
		params.height = WindowManager.LayoutParams.WRAP_CONTENT;
		params.gravity = Gravity.CENTER;
		dialog.setCanceledOnTouchOutside(true);
		return dialog;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

}