package com.example.raviprmr21.hub20.recreation;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.example.raviprmr21.hub20.R;

/**
 * Created by raviprmr21 on 12/6/17.
 */

public class more extends DialogFragment {

    @Override
    public void onStart() {
        super.onStart();
        //this statement is used to dim the background of the app when a dialog box is activated
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = null;
        if (window != null) {
            windowParams = window.getAttributes();
        }
        if (windowParams != null) {
            windowParams.dimAmount = 0.75f;
            windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            window.setAttributes(windowParams);
        }
        if (getDialog() == null)
            return;

    }

    @SuppressLint("ResourceAsColor")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MoreDialogStyle);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layout = inflater.inflate(R.layout.recreation_more, null);

        builder.setView(layout)
                .setTitle("")
                .setMessage(R.string.moreMessage)
                .setCancelable(false)
                .setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });

        return builder.create();
    }
}