package com.example.raviprmr21.hub20;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.annotation.RequiresApi;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import static com.example.raviprmr21.hub20.View.NotificationModeDialog.CURRENT_SAVED_NOTIFICATION_KEY;
import static com.example.raviprmr21.hub20.View.NotificationModeDialog.getArray;
import static com.example.raviprmr21.hub20.View.NotificationModeDialog.notificationListItems;

@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
public class NLService extends NotificationListenerService {

    public ArrayList<String> list = new ArrayList<>();
    private String TAG = this.getClass().getSimpleName();
    public  String notification ,notificationpackage,notificationtitle,notificationtext;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {

        Bundle extras= null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            extras = sbn.getNotification().extras;
        }

        notification= String.valueOf(sbn.getNotification().tickerText);
        notificationpackage=sbn.getPackageName();
        notificationtitle=extras.getString("android.title");
        notificationtext= String.valueOf(extras.getCharSequence("android.text"));

        Log.i(TAG,"**********  onNotificationPosted");

        final boolean[] notificationSelected={false,false,false,false,false,false,false,false,false};
        SharedPreferences share=getApplication().getSharedPreferences(CURRENT_SAVED_NOTIFICATION_KEY, Context.MODE_PRIVATE);
        String mem = share.getString(CURRENT_SAVED_NOTIFICATION_KEY, "");
        String[] notificationArray = getArray(mem); //see below for the getArray() method
        for(int n = 0; n < notificationArray.length; n++){
            notificationSelected[n] = notificationArray[n].equals("true");
        }
        list.clear();
        for(int i=0;i<9;i++)
        {
            if(notificationSelected[i])
            {
                list.add(notificationListItems[i]);
            }
        }

        Log.e("list of array", String.valueOf(list));
        Log.e("Notification Package " , notificationpackage);
        if(notification!=null) {Log.e("Notification full text ", notification);}
        if(notificationtitle!=null) {Log.e("Notification Title ", notificationtitle);}
        if(notificationtext!=null) {Log.e("Notification Text " , notificationtext);}

    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        Log.i(TAG,"********** onNOtificationRemoved");
        Log.e("Notification " , sbn.getPackageName());
    }

    class NLServiceReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
                Log.e("Notification " , "success");
        }
    }

    public void NotificationData(String status,String packagename ){

    }
}


