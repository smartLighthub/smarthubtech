package com.example.raviprmr21.hub20.recreation;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.raviprmr21.hub20.Model.ColorModel;
import com.example.raviprmr21.hub20.R;
import com.example.raviprmr21.hub20.View.ColorDialog;
import com.nhaarman.supertooltips.ToolTip;
import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.nhaarman.supertooltips.ToolTipView;

import java.util.Random;

/**
 * Created by raviprmr21 on 12/6/17.
 */

public class clap extends AppCompatActivity
        implements MicLevelReader.MicLevelReaderValueListener,ColorDialog.ColorDialogListener,
        View.OnClickListener{

    private static final int REQUEST_RECORD_AUDIO = 121;
    private Thread mRecorderThread = null;
    private double mMeterValue = 0;
    private MicLevelReader mMicLevelReader;
    ImageView recordmic;
    Button ColorPick;
    int touchRGB =0;
    public Context ctx;
    private int brightness;
    ColorModel colormodel;
    public int red,green,blue;


    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (mMicLevelReader.isRunning()) {
                updateVuData(mMeterValue);
               // System.out.println(mMeterValue);
            }

        }
    };
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recreation_clap);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        recordmic = (ImageView) findViewById(R.id.recordMic);
        ColorPick =(Button)findViewById(R.id.Mic_pickColor);
        colormodel= new ColorModel();
        recordmic.setImageLevel(0);
        mMicLevelReader = new MicLevelReader(this, LevelMethod.Max);
        startit();
        checkMicrophoneAccess();
        ColorPick.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        ColorDialog dialogFragment = new ColorDialog();
        dialogFragment.setCallBack(this);
        dialogFragment.show(getSupportFragmentManager(), "ColorSelect");

    }
    private boolean checkMicrophoneAccess() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_RECORD_AUDIO);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_RECORD_AUDIO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(this,"Need Microphone to work", Toast.LENGTH_LONG).show();
                }
            }
        }
    }


    @Override
    protected void onPause() {

        if (mMicLevelReader.isRunning()) {
            stopit();
        }
        super.onPause();
    }

    @Override
    public void valueCalculated(double level) {
        mMeterValue = level;
        mHandler.obtainMessage(1).sendToTarget();

    }



    void updateVuData(double micData)
    {
        if(micData>20000 && micData <32000) {
               Log.e("Clap ","success");
            final ColorModel colorModel =new ColorModel();
            Random Redrnd = new Random();
            Random Greenrnd = new Random();
            Random Bluernd = new Random();
            red= Redrnd.nextInt(255);
            green= Greenrnd.nextInt(255);
            blue= Bluernd.nextInt(255);
            colorModel.setRed(red);
            colorModel.setBlue(blue);
            colorModel.setGreen(green);
            Log.v("Color:", String.valueOf(red)+","+String.valueOf(green)+","+String.valueOf(blue));
        }
    }

    public boolean startit() {
        if (checkMicrophoneAccess()) {
            mRecorderThread = new Thread(mMicLevelReader, "AudioListener Thread");
            mRecorderThread.start();
            Window w = this.getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            return true;
        }
        return false;
    }


    public void stopit() {
        if (mMicLevelReader.isRunning()) {
            mMicLevelReader.stop();
            Window w = this.getWindow();
            w.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            try {
                mRecorderThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        recordmic.setImageLevel(0);

    }

    @Override
    protected void onStop() {
        stopit();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        stopit();
        super.onDestroy();
    }

    @Override
    public void onDialogPositiveClick(ColorModel model) {
        touchRGB = model.getTouchRGBDATA();
        Log.v("touchrgbcolor:", String.valueOf(touchRGB));
        ColorPick.setBackgroundColor(touchRGB);
        brightness = Color.alpha(touchRGB);
        Log.v("brightness", String.valueOf(brightness));
    }
}

