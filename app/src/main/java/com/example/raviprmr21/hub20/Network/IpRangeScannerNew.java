package com.example.raviprmr21.hub20.Network;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.example.raviprmr21.hub20.BuildConfig;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class IpRangeScannerNew extends IpScanner {

    private class ScanPingThread extends Thread {
        private long start_ip;
        private long stop_ip;

        public ScanPingThread(long low_hostnum, long high_hostnum) {
            this.start_ip = low_hostnum;
            this.stop_ip = high_hostnum;
        }

        public void run() {
            while (this.start_ip < this.stop_ip && IpRangeScannerNew.this.WAS_STARTED) {
                Process suProcess = null;
                try {
                    suProcess = Runtime.getRuntime().exec(IpRangeScannerNew.this.PING_PATH + " -c 1 -w 1 -W 1 " + Utils.ip_to_str(this.start_ip));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                DataOutputStream os = new DataOutputStream(suProcess.getOutputStream());
                InputStreamReader reader = new InputStreamReader(suProcess.getInputStream());
                BufferedReader buffer = new BufferedReader(reader);
                long timeS = System.nanoTime();
                while (IpRangeScannerNew.this.WAS_STARTED && (System.nanoTime() - timeS) / 1000000 < 2000) {
                    String line = null;
                    try {
                        if (buffer.ready()) {
                            try {
                                line = buffer.readLine();
                            } catch (IOException e) {
                                line = null;
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (line == null) {
                        try {
                            Thread.currentThread();
                            Thread.sleep(20);
                        } catch (InterruptedException e2) {
                        }
                    }
                    if (line != null) {
                        line = IpRangeScannerNew.this.parse_line(line);
                        if (line == null) {
                            continue;
                        } else if (line.equals("_exit_")) {
                            break;
                        } else {
                            IpRangeScannerNew.this.conveyor.add(new ResolveThread(line, BuildConfig.FLAVOR, BuildConfig.FLAVOR));
                        }
                    }
                }
                try {
                    buffer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                suProcess.destroy();
                this.start_ip++;
            }
        }
    }

    public IpRangeScannerNew(Context ctx) {
        super(ctx);
    }

    public void start_scan(long start, long stop) {
        super.start_scan(start, stop);
        this.conveyor = new ThreadConveyor(this.THREADS);
        this.conveyor.start();
        reach_hosts();
        this.conveyor.joinAll();
        this.WAS_STARTED = false;
    }

    private void reach_hosts() {
        long start_ip = this.start_ip;
        long stop_ip = this.stop_ip;
        if (start_ip == Long.MAX_VALUE || stop_ip == Long.MAX_VALUE) {
            try {
                ((Activity) this.context).runOnUiThread(new Runnable() {
                    public void run() {
                        Toast t = Toast.makeText(IpRangeScannerNew.this.context, "You need to enter correct IP", Toast.LENGTH_SHORT);
                        t.setGravity(17, 0, 25);
                        t.show();
                    }
                });
            } catch (NullPointerException e) {
            }
        } else if (stop_ip - start_ip >= 0) {
            long p = (stop_ip - start_ip) / ((long) this.THREADS);
            if (p == 0) {
                p = 1;
            }
            ArrayList<ScanPingThread> t = new ArrayList<>();
            while (start_ip < stop_ip) {
                ScanPingThread th = new ScanPingThread(start_ip, start_ip + p);
                th.start();
                t.add(th);
                start_ip += p;
            }
            for (int i = 0; i < t.size(); i++) {
                try {
                    (t.get(i)).join();
                } catch (InterruptedException e2) {
                }
            }
        }
    }

    private String parse_line(String line) {
        try {
            int len = line.length();
            if (len == 0) {
                return null;
            }
            if (line.contains("packets transmitted")) {
                return null;
            }
            if (line.charAt(0) == '-') {
                return "_exit_";
            }
            if (line.contains("Do you want to ping broadcast")) {
                return "_exit_";
            }
            if (line.contains("unknown host")) {
                return "_exit_";
            }
            if (line.charAt(0) >= '0' && line.charAt(0) <= '9') {
                StringBuilder fromip = new StringBuilder(BuildConfig.FLAVOR);
                String fromname = BuildConfig.FLAVOR;
                int i = 0;
                while (i < len && line.charAt(i) != ' ') {
                    i++;
                }
                while (i < len && line.charAt(i) == ' ') {
                    i++;
                }
                while (i < len && line.charAt(i) != ' ') {
                    i++;
                }
                while (i < len && line.charAt(i) == ' ') {
                    i++;
                }
                while (i < len && line.charAt(i) != ' ') {
                    i++;
                }
                while (i < len && line.charAt(i) == ' ') {
                    i++;
                }
                while (i < len && line.charAt(i) != ' ') {
                    if (line.charAt(i) != ':') {
                        fromname = fromname + line.charAt(i);
                    }
                    i++;
                }
                while (i < len && line.charAt(i) == ' ') {
                    i++;
                }
                if (line.charAt(i) != '(') {
                    return fromname;
                }
                while (i < len && line.charAt(i) != ')' && line.charAt(i) != ':') {
                    if (!(line.charAt(i) == '(' || line.charAt(i) == ')')) {
                        fromip.append(line.charAt(i));
                    }
                    i++;
                }
                return fromip.toString();
            }
            return null;
        } catch (Exception ignored) {
        }
        return line;
    }
}
