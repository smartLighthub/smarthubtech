package com.example.raviprmr21.hub20.View;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.NumberPicker;

import com.example.raviprmr21.hub20.R;

import java.util.ArrayList;

/**
 * Created by raviprmr21 on 10/6/17.
 */
public class IpDialog extends DialogFragment {

    ArrayList<NumberPicker> mNpList = new ArrayList<>(5);

    @Override
    public void onStart() {
        super.onStart();
	    //this statement is used to dim the background of the app when a dialog box is activated
	    Window window = getDialog().getWindow();
	    WindowManager.LayoutParams windowParams = null;
	    if (window != null) {
		    windowParams = window.getAttributes();
	    }
	    if (windowParams != null) {
		    windowParams.dimAmount = 0.75f;
		    windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		    window.setAttributes(windowParams);
	    }
	    if (getDialog() == null) {
        }

    }
    @SuppressLint("ResourceAsColor")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),R.style.AlertDialogStyle);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layout = inflater.inflate(R.layout.ipdialog, null);
        NumberPicker np1 = (NumberPicker) layout.findViewById(R.id.numberPicker1);
        NumberPicker np2 = (NumberPicker) layout.findViewById(R.id.numberPicker2);
        NumberPicker np3 = (NumberPicker) layout.findViewById(R.id.numberPicker3);
        NumberPicker np4 = (NumberPicker) layout.findViewById(R.id.numberPicker4);
        NumberPicker np5 = (NumberPicker) layout.findViewById(R.id.numberPicker5);

        setDividerColor(np1, Color.WHITE);
        setDividerColor(np2, Color.WHITE);
        setDividerColor(np3, Color.WHITE);
        setDividerColor(np4, Color.WHITE);
        setDividerColor(np5, Color.WHITE);

        np1.setMinValue(0);
        np2.setMinValue(0);
        np3.setMinValue(0);
        np4.setMinValue(0);
        np5.setMinValue(1);
        np1.setMaxValue(255);
        np2.setMaxValue(255);
        np3.setMaxValue(255);
        np4.setMaxValue(255);
        np5.setMaxValue(65535);

        mNpList.add(np1);
        mNpList.add(np2);
        mNpList.add(np3);
        mNpList.add(np4);
        mNpList.add(np5);

        builder.setView(layout)
                .setTitle("Choose Host IP address and Port number")
                .setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                IpDialog.this.getDialog().cancel();
                            }
                        })
                .setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                              //  Log.d("ip selected", String.valueOf(np5.getValue()));
                            }
                        });

        return builder.create();
    }
    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException | Resources.NotFoundException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }
}