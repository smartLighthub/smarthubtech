package com.example.raviprmr21.hub20.Scene;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.raviprmr21.hub20.Model.ColorModel;
import com.example.raviprmr21.hub20.R;
import com.example.raviprmr21.hub20.View.ColorDialog;

/**
 * Created by raviprmr21 on 17/7/17.
 */

public class addMain extends DialogFragment implements View.OnClickListener,ColorDialog.ColorDialogListener {

	Button  color1,color2,color3,color4,color5,color6;
	public int touchedRGB = 0;
	Integer[] coloranimchange={0,0,0,0,0,0},customvisibility={0,0,0,0,0,0};
	EditText custtomname;
	String theme;
	int colorid = 0;
	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.ColoePickerDialogStyle);
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View layout = inflater.inflate(R.layout.dialogadd, null);
		color1 = (Button) layout.findViewById(R.id.addcolor1);
		color2 = (Button) layout.findViewById(R.id.addcolor2);
		color3 = (Button) layout.findViewById(R.id.addcolor3);
		color4 = (Button) layout.findViewById(R.id.addcolor4);
		color5 = (Button) layout.findViewById(R.id.addcolor5);
		color6 = (Button) layout.findViewById(R.id.addcolor6);
		custtomname = (EditText) layout.findViewById(R.id.themename);
		color1.setOnClickListener(this);
		color2.setOnClickListener(this);
		color3.setOnClickListener(this);
		color4.setOnClickListener(this);
		color5.setOnClickListener(this);
		color6.setOnClickListener(this);
		builder.setView(layout)
				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
                                addMain.this.getDialog().cancel();
                            }
						})
				.setPositiveButton(R.string.ok,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								theme = custtomname.getText().toString();
								SharedPreferences share = getActivity().getSharedPreferences("customvisibility", Context.MODE_PRIVATE);
								String mem = share.getString("customvisibility", "");
								String[] color = getArray(mem); //see below for the getArray() method
								if (mem.equals("")) {
									customvisibility[0] = 1;

								} else {
									for (int n = 0; n < color.length; n++) {
										customvisibility[0] = 1;
										if (Integer.valueOf(color[n]) == 1) {
											customvisibility[n + 1] = 1;
											Toast.makeText(getActivity(), color[n], Toast.LENGTH_SHORT).show();
										}
									}
								}
								String save = "";
								SharedPreferences.Editor editor = share.edit();
                                for (Integer aCustomvisibility : customvisibility) {
                                    save = save + aCustomvisibility + "|$|SEPARATOR|$|";
                                }
								editor.putString("customvisibility", save);
								editor.apply();
								Toast.makeText(getActivity(), save, Toast.LENGTH_SHORT).show();


								share = getActivity().getSharedPreferences("customvisibility", Context.MODE_PRIVATE);
								mem = share.getString("customvisibility", "");
								color = getArray(mem);

								for (int n = color.length - 1; n >= 0; n--) {
									if (Integer.valueOf(color[n]) == 1) {
										if (n == 0) {
											save = "";
											share = getActivity().getSharedPreferences("custom1modecolors", Context.MODE_PRIVATE);
											editor = share.edit();
											for (int j = 0; j < coloranimchange.length; j++) {

												save = save + coloranimchange[j] + "|$|SEPARATOR|$|";
												coloranimchange[j] = 0;
											}
											editor.putString("custom1modecolors", save);
											editor.apply();
											Toast.makeText(getActivity(), save, Toast.LENGTH_SHORT).show();

											share = getActivity().getSharedPreferences("custom1themename", Context.MODE_PRIVATE);
											editor = share.edit();
											editor.putString("custom1themename", theme);
											editor.apply();
											Toast.makeText(getActivity(), theme, Toast.LENGTH_SHORT).show();

											break;
										}

										if (n == 1) {
											save = "";
											share = getActivity().getSharedPreferences("custom2modecolors", Context.MODE_PRIVATE);
											editor = share.edit();
											for (int j = 0; j < coloranimchange.length; j++) {
												save = save + coloranimchange[j] + "|$|SEPARATOR|$|";
												coloranimchange[j] = 0;
											}
											editor.putString("custom2modecolors", save);
											editor.apply();
											Toast.makeText(getActivity(), save, Toast.LENGTH_SHORT).show();

											share = getActivity().getSharedPreferences("custom2themename", Context.MODE_PRIVATE);
											editor = share.edit();
											editor.putString("custom2themename", theme);
											editor.apply();
											Toast.makeText(getActivity(), theme, Toast.LENGTH_SHORT).show();
											break;
										}

										if (n == 2) {
											save = "";
											share = getActivity().getSharedPreferences("custom3modecolors", Context.MODE_PRIVATE);
											editor = share.edit();
											for (int j = 0; j < coloranimchange.length; j++) {
												save = save + coloranimchange[j] + "|$|SEPARATOR|$|";
												coloranimchange[j] = 0;
											}
											editor.putString("custom3modecolors", save);
											editor.apply();
											Toast.makeText(getActivity(), save, Toast.LENGTH_SHORT).show();

											share = getActivity().getSharedPreferences("custom3themename", Context.MODE_PRIVATE);
											editor = share.edit();
											editor.putString("custom3themename", theme);
											editor.apply();
											Toast.makeText(getActivity(), theme, Toast.LENGTH_SHORT).show();
											break;
										}

										if (n == 3) {
											save = "";
											share = getActivity().getSharedPreferences("custom4modecolors", Context.MODE_PRIVATE);
											editor = share.edit();
											for (int j = 0; j < coloranimchange.length; j++) {
												save = save + coloranimchange[j] + "|$|SEPARATOR|$|";
												coloranimchange[j] = 0;
											}
											editor.putString("custom4modecolors", save);
											editor.apply();
											Toast.makeText(getActivity(), save, Toast.LENGTH_SHORT).show();

											share = getActivity().getSharedPreferences("custom4themename", Context.MODE_PRIVATE);
											editor = share.edit();
											editor.putString("custom4themename", theme);
											editor.apply();
											Toast.makeText(getActivity(), theme, Toast.LENGTH_SHORT).show();

											break;
										}

										if (n == 4) {
											save = "";
											share = getActivity().getSharedPreferences("custom5modecolors", Context.MODE_PRIVATE);
											editor = share.edit();
											for (int j = 0; j < coloranimchange.length; j++) {
												save = save + coloranimchange[j] + "|$|SEPARATOR|$|";
												coloranimchange[j] = 0;
											}
											editor.putString("custom5modecolors", save);
											editor.apply();
											Toast.makeText(getActivity(), save, Toast.LENGTH_SHORT).show();

											share = getActivity().getSharedPreferences("custom5themename", Context.MODE_PRIVATE);
											editor = share.edit();
											editor.putString("custom5themename", theme);
											editor.apply();
											Toast.makeText(getActivity(), theme, Toast.LENGTH_SHORT).show();
											break;
										}

										if (n == 5) {
											save = "";
											share = getActivity().getSharedPreferences("custom6modecolors", Context.MODE_PRIVATE);
											editor = share.edit();
											for (int j = 0; j < coloranimchange.length; j++) {
												save = save + coloranimchange[j] + "|$|SEPARATOR|$|";
												coloranimchange[j] = 0;
											}
											editor.putString("custom6modecolors", save);
											editor.apply();
											Toast.makeText(getActivity(), save, Toast.LENGTH_SHORT).show();

											share = getActivity().getSharedPreferences("custom6themename", Context.MODE_PRIVATE);
											editor = share.edit();
											editor.putString("custom6themename", theme);
											editor.apply();
											Toast.makeText(getActivity(), theme, Toast.LENGTH_SHORT).show();
											break;
										}
									}
								}
							}
						});
		return builder.create();
	}
	@Override
	public void onClick(View view) {
		ColorDialog dialogFragment = new ColorDialog();
		switch (view.getId())
		{
			case R.id.addcolor1:
				dialogFragment.setCallBack(this);
				dialogFragment.show(getChildFragmentManager(), "ColorSelect");
				colorid=1;
				break;
			case R.id.addcolor2:
				dialogFragment.setCallBack(this);
				dialogFragment.show(getChildFragmentManager(), "ColorSelect");
				colorid=2;
				break;
			case R.id.addcolor3:
				dialogFragment.setCallBack(this);
				dialogFragment.show(getChildFragmentManager(), "ColorSelect");
				colorid=3;
				break;
			case R.id.addcolor4:
				dialogFragment.setCallBack(this);
				dialogFragment.show(getChildFragmentManager(), "ColorSelect");
				colorid=4;
				break;
			case R.id.addcolor5:
				dialogFragment.setCallBack(this);
				dialogFragment.show(getChildFragmentManager(), "ColorSelect");
				colorid=5;
				break;
			case R.id.addcolor6:
				dialogFragment.setCallBack(this);
				dialogFragment.show(getChildFragmentManager(), "ColorSelect");
				colorid=6;
				break;
		}
	}
	@Override
	public void onDialogPositiveClick(ColorModel model) {
	touchedRGB= model.getTouchRGBDATA();

		if(touchedRGB!=0) {
			if (colorid == 1) {
				color1.setBackgroundColor(touchedRGB);
				coloranimchange[0]=touchedRGB;
			} else if (colorid == 2) {
				color2.setBackgroundColor(touchedRGB);
				coloranimchange[1]=touchedRGB;
			} else if (colorid == 3) {
				color3.setBackgroundColor(touchedRGB);
				coloranimchange[2]=touchedRGB;
			} else if (colorid == 4) {
				color4.setBackgroundColor(touchedRGB);
				coloranimchange[3]=touchedRGB;
			} else if (colorid == 5) {
				color5.setBackgroundColor(touchedRGB);
				coloranimchange[4]=touchedRGB;
			} else {
				color6.setBackgroundColor(touchedRGB);
				coloranimchange[5]=touchedRGB;
			}
		}
	}
	public static String[] getArray(String input) {
		return input.split("\\|\\$\\|SEPARATOR\\|\\$\\|");
	}
}