package com.example.raviprmr21.hub20.Music;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.example.raviprmr21.hub20.MainActivity;
import com.example.raviprmr21.hub20.R;

import java.util.ArrayList;


/**
 * Created by raviprmr21 on 13/6/17.
 */

public class localMusicMain extends DialogFragment implements SongAdapter.ClickListener{

    private ArrayList<LocalTrackModel> _songs = new ArrayList<>();
    RecyclerView recyclerView;
    SongAdapter songAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.local_musiclist, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.localMusicList);
        songAdapter = new SongAdapter(this.getActivity(), _songs);
        songAdapter.setClickListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL)); //adding a divider line between list items
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(songAdapter);
        LoadMediaList();
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        //this statement is used to dim the background of the app when a dialog box is activated
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = null;
        if (window != null) {
            windowParams = window.getAttributes();
        }
        if (windowParams != null) {
            windowParams.dimAmount = 0.75f;
            windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            window.setAttributes(windowParams);
            window.setLayout(1000,950); //setting up dimension can be added to Dimen in res folder
        }
        if (getDialog() == null)
        {
            setShowsDialog(false);
        }
    }


    public ArrayList<LocalTrackModel> LoadMediaList() {
        _songs.clear();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, MediaStore.MediaColumns.TITLE );
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    String name = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                    String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                    String url = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                    String album =cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                    long duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION));
                    if(duration == 0.0 || duration >10000) {
                        LocalTrackModel s = new LocalTrackModel(name, artist, url,album);
                        _songs.add(s);
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
            songAdapter = new SongAdapter(getActivity(),_songs);
        }
        return _songs;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(),android.R.style.Theme_Translucent_NoTitleBar);
        final View view = getActivity().getLayoutInflater().inflate(R.layout.local_musiclist,null);

        final Drawable d = new ColorDrawable(Color.BLACK);
        d.setAlpha(130);

        if(dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(d);
        }
        dialog.getWindow().setContentView(view);

        final WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.CENTER;
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    @Override
    public void itemClicked(View v, int position) {
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        Fragment fragment = new musicMain();
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = ((MainActivity) getContext()).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.contentContainer, fragment, "Music").commit();
        dismiss();
    }
}