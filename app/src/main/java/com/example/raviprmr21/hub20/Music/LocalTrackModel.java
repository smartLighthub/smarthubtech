package com.example.raviprmr21.hub20.Music;

import java.io.Serializable;

/**
 * Created by raviprmr21 on 13/6/17.
 */

class LocalTrackModel {
    private long id;
    private String title;
    private String artist;
    private String path;
    private String album;

    public LocalTrackModel(String title, String artist, String path ,String album) {
        //this.id = id;
        this.title = title;
        this.artist = artist;
        this.path = path;
        this.album =album;
        // this.duration = duration;

    }



    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }
    public void setArtist(String artist) {

        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }
}
