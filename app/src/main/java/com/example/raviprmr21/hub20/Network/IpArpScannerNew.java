package com.example.raviprmr21.hub20.Network;

import android.content.Context;

import com.example.raviprmr21.hub20.BuildConfig;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;

public class IpArpScannerNew extends IpScanner {
    private ArrayList<String> IPS = new ArrayList<String>();

    private class ScanThread extends Thread {
        private long start_ip;
        private long stop_ip;

        ScanThread(long start_ip, long stop_ip) {
            this.start_ip = start_ip;
            this.stop_ip = stop_ip;
        }

        public void run() {
            for (int n = 0; n < IpArpScannerNew.this.LOOPS; n++) {
                while (this.start_ip < this.stop_ip) {
                    byte[] addr = Utils.ip_to_byte(this.start_ip);
                    if (!Arrays.equals(IpArpScannerNew.this.my_ipByte, addr)) {
                        try {
                            InetAddress.getByAddress(addr).isReachable(IpArpScannerNew.this.REACHABLE_TIMEOUT);
                        } catch (UnknownHostException ignored) {
                        } catch (IOException ignored) {
                        }
                    }
                    this.start_ip++;
                }
            }
        }
    }

    public IpArpScannerNew(Context ctx) {
        super(ctx);
    }

    public void start_scan(long start, long stop) {
        super.start_scan(start, stop);
        this.conveyor = new ThreadConveyor(this.THREADS);
        this.conveyor.start();
        this.IPS.clear();
        this.conveyor.add(new ResolveThread(this.my_ip, this.mymac, BuildConfig.FLAVOR));
        show_arp();
        if (this.WAS_STARTED) {
            run_reach_threads();
            if (this.WAS_STARTED) {
                show_arp();
                if (this.WAS_STARTED) {
                    try {
                        Thread.currentThread();
                        Thread.sleep((long) this.SCAN_TIMEOUT);
                    } catch (InterruptedException ignored) {
                    }
                    if (this.WAS_STARTED) {
                        show_arp();
                        this.conveyor.joinAll();
                    }
                }
            }
        }
    }

    private void run_reach_threads() {
        long start_ip = this.start_ip;
        long stop_ip = this.stop_ip;
        long p = (stop_ip - start_ip) / ((long) this.THREADS);
        if (p == 0) {
            p = 1;
        }
        ArrayList<ScanThread> t = new ArrayList<>();
        while (start_ip < stop_ip) {
            ScanThread th = new ScanThread(start_ip, start_ip + p);
            th.start();
            t.add(th);
            start_ip += p;
        }
        for (int i = 0; i < t.size(); i++) {
            try {
                ((ScanThread) t.get(i)).join();
            } catch (InterruptedException ignored) {
            }
        }
    }

    private void show_arp() {
        try {
            FileInputStream inputstream = new FileInputStream("/proc/net/arp");
            if (inputstream != null) {
                InputStreamReader isr = new InputStreamReader(inputstream);
                BufferedReader reader = new BufferedReader(isr);
                reader.readLine();
                while (this.WAS_STARTED) {
                    String line = reader.readLine();
                    if (line == null) {
                        break;
                    }
                    String mac = BuildConfig.FLAVOR;
                    StringBuilder ip = new StringBuilder(BuildConfig.FLAVOR);
                    int llen = line.length();
                    int l = 0;
                    while (l < llen && line.charAt(l) != ' ') {
                        ip.append(line.charAt(l));
                        l++;
                    }
                    if (!this.IPS.contains(ip.toString())) {
                        while (l < llen && line.charAt(l) == ' ') {
                            l++;
                        }
                        while (l < llen && line.charAt(l) != ' ') {
                            l++;
                        }
                        while (l < llen && line.charAt(l) == ' ') {
                            l++;
                        }
                        while (l < llen && line.charAt(l) != ' ') {
                            l++;
                        }
                        while (l < llen && line.charAt(l) == ' ') {
                            l++;
                        }
                        while (l < llen && line.charAt(l) != '\n' && line.charAt(l) != ' ') {
                            mac = mac + line.charAt(l);
                            l++;
                        }
                        if (!(mac.equals("00:00:00:00:00:00") || mac.isEmpty())) {
                            this.conveyor.add(new ResolveThread(ip.toString(), mac, BuildConfig.FLAVOR));
                            this.IPS.add(ip.toString());
                        }
                    }
                }
                reader.close();
                isr.close();
                inputstream.close();
            }
        } catch (Throwable ignored) {
        }
    }
}
