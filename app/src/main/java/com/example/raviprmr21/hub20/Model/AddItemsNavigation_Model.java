package com.example.raviprmr21.hub20.Model;

/**
 * Created by raviprmr21 on 21/7/17.
 */

public class AddItemsNavigation_Model {
	private String LightName;
	private String IpAddress;
	private String IconSelected;

	public String getLightName() {
		return LightName;
	}

	public void setLightName(String lightName) {
		LightName = lightName;
	}

	public String getIpAddress() {
		return IpAddress;
	}

	public void setIpAddress(String ipAddress) {
		IpAddress = ipAddress;
	}

	public String getIconSelected() {
		return IconSelected;
	}

	public void setIconSelected(String iconSelected) {
		IconSelected = iconSelected;
	}
}
