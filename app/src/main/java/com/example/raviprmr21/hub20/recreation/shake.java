package com.example.raviprmr21.hub20.recreation;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.raviprmr21.hub20.Model.ColorModel;
import com.example.raviprmr21.hub20.R;

import java.util.Random;

/**
 * Created by raviprmr21 on 12/6/17.
 */

public class shake extends AppCompatActivity {

    // The following are used for the shake detection
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private shakeListener mShakeDetector;
    ImageView shakeImg;
    public int red,green,blue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recreation_shake);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        shakeImg = (ImageView)findViewById(R.id.shakeImgage);
        //ShakeDetector initialization
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        final ColorModel colorModel =new ColorModel();
        mShakeDetector = new shakeListener();
        mShakeDetector.setOnShakeListener(new shakeListener.OnShakeListener() {

            @Override
            public void onShake(int count) {
                shake();
                Random Redrnd = new Random();
                Random Greenrnd = new Random();
                Random Bluernd = new Random();
                red= Redrnd.nextInt(255);
                green= Greenrnd.nextInt(255);
                blue= Bluernd.nextInt(255);
                colorModel.setRed(red);
                colorModel.setBlue(blue);
                colorModel.setGreen(green);
                Log.v("Color:", String.valueOf(red)+","+String.valueOf(green)+","+String.valueOf(blue));
            }
        });

    }

    void shake(){

        if (Build.VERSION.SDK_INT >= 26) {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(1000,10));
        } else {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(1000);
        }
        Animation rotate = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.shake);
        shakeImg.startAnimation(rotate);
    }

    @Override
    public void onResume() {
        super.onResume();
        // Add the following line to register the Session Manager Listener onResume
        mSensorManager.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onPause() {
        // Add the following line to unregister the Sensor Manager onPause
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
    }

}


