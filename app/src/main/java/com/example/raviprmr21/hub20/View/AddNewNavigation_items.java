package com.example.raviprmr21.hub20.View;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.raviprmr21.hub20.MainActivity;
import com.example.raviprmr21.hub20.R;

/**
 * Created by raviprmr21 on 21/7/17.
 */

public class AddNewNavigation_items extends Dialog {
	private  Button SaveButton;
	private  EditText HostName;




	public AddNewNavigation_items(){
		super(null);
	}
	public AddNewNavigation_items(@NonNull Context context) {
		super(context);

		final Dialog dialog = new Dialog(context);
		dialog.setContentView(R.layout.add_new_items);

		SaveButton=(Button) dialog.findViewById(R.id.saveLed_btn);
		HostName=(EditText)dialog.findViewById(R.id.Edit_name_light);

		SaveButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				Log.e("HostEdited :" , String.valueOf(HostName.getText()));
			dialog.dismiss();
			}
		});

		dialog.show();
	}

}
