package com.example.raviprmr21.hub20.Database;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by ${RaviParmar} on ${18/08/17}.
 */

public class hubContract {

    public hubContract() {
    }

    public static final class SHubEntry implements BaseColumns {

        /** Name of database table for smartHub find esp8266 */
        public final static String TABLE_NAME = "smartHub";

        /**
         * Unique ID number for the shub (only for use in the database table).
         *
         * Type: INTEGER
         */
        public final static String _ID = BaseColumns._ID;

        /**
         * Name of the mac id.
         *
         * Type: TEXT
         */
        public final static String COLUMN_MAC_ID ="mac";

        /**
         *  Name of the vendor
         *
         * Type: TEXT
         */
        public final static String COLUMN_VENDOR_NAME = "vendor";


        /**
         *  Name of the ip
         *
         * Type: TEXT
         */
        public final static String COLUMN_IP_NAME = "ipDevice";


        /**
         *  Name of the deviceName
         *
         * Type: TEXT
         */
        public final static String COLUMN_Device_NAME = "deviceName";

    }

}

