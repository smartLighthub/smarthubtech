package com.example.raviprmr21.hub20.Database;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import  com.example.raviprmr21.hub20.Database.hubContract.SHubEntry;
import com.example.raviprmr21.hub20.R;

/**
 * Created by ${RaviParmar} on ${24/09/17}.
 */

public class hubDbHelper extends SQLiteOpenHelper {

    public static final String LOG_TAG = hubDbHelper.class.getSimpleName();

    /** Name of the database file */
    private static final String DATABASE_NAME = "SmartHub.db";

    /**
     * Database version. If you change the database schema, you must increment the database version.
     */
    private static final int DATABASE_VERSION = 1;

    /**
     * Constructs a new instance of {@link hubDbHelper}.
     *
     * @param context of the app
     */
    public hubDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * This is called when the database is created for the first time.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create a String that contains the SQL statement to create the shub table
        String SQL_CREATE_SHub_TABLE =  "CREATE TABLE " + SHubEntry.TABLE_NAME + " ("
                + SHubEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + SHubEntry.COLUMN_MAC_ID + " TEXT NOT NULL, "
                + SHubEntry.COLUMN_IP_NAME +"TEXT NOT NULL,"
                + SHubEntry.COLUMN_VENDOR_NAME + " TEXT,"
                + SHubEntry.COLUMN_Device_NAME +"TEXT NOT NULL" + ")";

        // Execute the SQL statement
        db.execSQL(SQL_CREATE_SHub_TABLE);
    }

    /**
     * This is called when the database needs to be upgraded.
     */
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // The database is still at version 1, so there's nothing to do be done here.
    }
}
