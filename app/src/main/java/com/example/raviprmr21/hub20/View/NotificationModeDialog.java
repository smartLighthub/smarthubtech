package com.example.raviprmr21.hub20.View;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.Window;
import android.view.WindowManager;

import com.example.raviprmr21.hub20.R;

import java.util.ArrayList;

/**
 * Created by ${RaviParmar} on ${18/08/17}.
 */
public class NotificationModeDialog extends DialogFragment {
    public ArrayList<String>  list = new ArrayList<>();
    public  int i;
    public static final String  CURRENT_SAVED_NOTIFICATION_KEY = "2486";
    public static final String notificationListItems[]={
            "CallAlert",
            "Instagram",
            "Snapchat",
            "Message",
            "Facebook",
            "Hike",
            "Whatsapp",
            "Gmail",
            "Messenger"};

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final boolean[] notificationSelected={false,false,false,false,false,false,false,false,false};
        SharedPreferences share=getActivity().getSharedPreferences(CURRENT_SAVED_NOTIFICATION_KEY, Context.MODE_PRIVATE);
        String mem = share.getString(CURRENT_SAVED_NOTIFICATION_KEY, "");
        String[] notificationArray = getArray(mem); //see below for the getArray() method
        for(int n = 0; n < notificationArray.length; n++){
            notificationSelected[n] = notificationArray[n].equals("true");
        }

        for(i=0;i<9;i++)
        {
            if(notificationSelected[i])
            {
                list.add(notificationListItems[i]);
            }
        }
        AlertDialog dialog = new AlertDialog.Builder(getActivity(), R.style.AlertDialogStyle)
                .setTitle("Notification Mode")
                .setMultiChoiceItems(notificationListItems, notificationSelected ,new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int j, boolean isChecked) {
                        if(isChecked)
                        {
                            list.add(notificationListItems[j]);
                        }
                        else if(list.contains(notificationListItems[j]))
                        {
                            list.remove(notificationListItems[j]);
                        }

                    }
                })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String selections = "";
                        for (String ms : list) {
                            selections = selections + "\n" + ms;
                        }
                        //Uncomment the below line for displaying the message on screen if needed.
                        // Toast.makeText(getActivity(),selections,Toast.LENGTH_SHORT).show();
                        String save = "";
                        SharedPreferences share = getActivity()
                                .getSharedPreferences(CURRENT_SAVED_NOTIFICATION_KEY, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = share.edit();
                        for (boolean aNotificationSelected : notificationSelected) {

                            if (aNotificationSelected) {
                                save = save + "true" + "|$|SEPARATOR|$|";
                            } else {
                                save = save + "false" + "|$|SEPARATOR|$|";
                            }

                        }
                        editor.putString(CURRENT_SAVED_NOTIFICATION_KEY, save);
                        editor.apply();
                    }
                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).create();
        dialog.show();
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        //this statement is used to dim the background of the app when a dialog box is activated
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = null;
        if (window != null) {
            windowParams = window.getAttributes();
        }
        if (windowParams != null) {
            windowParams.dimAmount = 0.75f;
            windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            window.setAttributes(windowParams);
        }
        if (getDialog() == null)
        {
            setShowsDialog(false);
        }
    }
    public static String[] getArray(String input) {
        return input.split("\\|\\$\\|SEPARATOR\\|\\$\\|");
    }
}