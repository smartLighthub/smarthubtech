package com.example.raviprmr21.hub20.Network;

import com.example.raviprmr21.hub20.BuildConfig;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ShellSearch {
    private volatile String SEARCH_PATH = BuildConfig.FLAVOR;
    private volatile boolean SEARCH_STARTED = false;
    private Object lock = new Object();
    private String[] places = new String[]{BuildConfig.FLAVOR, "/sbin/", "/bin/", "/xbin/", "/system/bin/", "/system/xbin/", "/system/sbin/", "/su/bin/", "/su/xbin/", "/su/sbin/", "/system/sd/xbin/", "/system/sd/bin/", "/system/sd/sbin/", "/system/bin/failsafe/", "/data/local/xbin/", "/data/local/sbin/", "/data/local/bin/", "/data/local/"};
    private Thread[] search_thrs = new Thread[this.places.length];

    private class Search extends Thread {
        private String cmd;

        public Search(String cmd) {
            this.cmd = cmd;
        }

        public void run() {
            try {
                Process suProcess = Runtime.getRuntime().exec(this.cmd);
                InputStreamReader reader = new InputStreamReader(suProcess.getInputStream());
                BufferedReader buffer = new BufferedReader(reader);
                long timeS = System.nanoTime();
                while ((System.nanoTime() - timeS) / 1000000 < 2000 && ShellSearch.this.SEARCH_STARTED) {
                    String line = null;
                    if (buffer.ready()) {
                        try {
                            line = buffer.readLine();
                        } catch (IOException e) {
                            line = null;
                        }
                    }
                    if (line != null) {
                        if (!(line.isEmpty() || line.contains("files") || line.contains("lib"))) {
                            synchronized (ShellSearch.this.lock) {
                                ShellSearch.this.SEARCH_PATH = line;
                                ShellSearch.this.SEARCH_STARTED = false;
                            }
                            break;
                        }
                    }
                }
                buffer.close();
                reader.close();
                suProcess.destroy();
            } catch (IOException e2) {
            }
        }
    }

    public String searchBin(String bin) {
        int i;
        this.SEARCH_STARTED = true;
        this.SEARCH_PATH = BuildConfig.FLAVOR;
        for (i = 0; i < this.places.length; i++) {
            this.search_thrs[i] = new Search(this.places[i] + "which " + bin);
            this.search_thrs[i].start();
        }
        for (i = 0; i < this.places.length; i++) {
            try {
                this.search_thrs[i].join();
            } catch (InterruptedException e) {
            }
        }
        return this.SEARCH_PATH;
    }
}
