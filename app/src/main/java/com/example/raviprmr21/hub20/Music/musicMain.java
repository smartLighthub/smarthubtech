package com.example.raviprmr21.hub20.Music;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.audiofx.Equalizer;
import android.media.audiofx.Visualizer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.raviprmr21.hub20.MainActivity;
import com.example.raviprmr21.hub20.Music.imageLoader.ImageLoader;
import com.example.raviprmr21.hub20.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import static android.R.attr.colorBackgroundFloating;
import static android.R.attr.defaultValue;
import static android.content.Context.NOTIFICATION_SERVICE;
import static android.support.v4.app.NotificationCompat.PRIORITY_MAX;
import static com.example.raviprmr21.hub20.Music.SongAdapter._songs;
import static com.example.raviprmr21.hub20.Music.SongAdapter.position;

/**
 * Created by raviprmr21 on 3/6/17.
 */

public class musicMain extends android.support.v4.app.Fragment
        implements View.OnClickListener {

    private final static int READ_MULTIPLE_PERMISSION_REQUEST = 123;
    public static Equalizer mEqualizer;
    String TAG = "MediaPlayer";
    String mTitle, mUrl, mArtist;
    public MediaPlayer mMediaPlayer;
    TextView title;
    TextView artist;
    ImageView PlayerController, NextPlay, PreviousPlay, ShufflePlay, RepeatPlay, Equalizer, localMusicFolder, BluetoothController;
    ImageView albumPic;
    BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();
    SeekBar MusicProgress;
    private double finalTime = 0;
    private Handler myHandler = new Handler();
    TextView startTimeTV, finishTimeTV;
    long length;
    boolean isShuffle = false, isRepeat = false;
    Bundle bundle;
    public static int oneTimeOnly = 0;
    ;
    public Context ctx;
    MainActivity mainActivity;
    int index;
    boolean serviceBound = false;
    public static final String Broadcast_PLAY_NEW_AUDIO = "com.example.raviprmr21.hub20.smarthub.PlayNewAudio";
    ArrayList<localMusicMain> audioList;
    private NotificationCompat.Builder builder;
    private NotificationManager notificationManager;
    private int notification_id;
    private RemoteViews remoteViews;
    private Context context;
    private BroadcastReceiver onNotice;
    private final static String TAG1 = "AudioFocus";
    private boolean mLossTransient, mLossTransientCanDuck;
    private static Visualizer mVisualizera;
    private static final float SEGMENT_SIZE = 100.f;
    private static final float BASS_SEGMENT_SIZE = 10.f / SEGMENT_SIZE;
    private static final float MID_SEGMENT_SIZE = 30.f / SEGMENT_SIZE;
    private static final float TREBLE_SEGMENT_SIZE = 60.f / SEGMENT_SIZE;
    private byte[] mBytes;
    private float bass;
    private float mid;
    private float treble;
    private NotificationManager notifManager;
    private final String CHANNEL_ONE_ID = "com.SmartHub";
    private final String CHANNEL_ONE_NAME = "SmartHub";


    private Runnable UpdateSongTime = new Runnable() {
        public void run() {
            if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
                int startTimenew = mMediaPlayer.getCurrentPosition();

                long StartminuteV = TimeUnit.MILLISECONDS.toMinutes((long) startTimenew);
                long StartSecondsV = TimeUnit.MILLISECONDS.toSeconds((long) startTimenew) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) startTimenew));
                startTimeTV.setText(StartminuteV + ":" + StartSecondsV);

                MusicProgress.setProgress(startTimenew); //update seekBar with current time
                myHandler.postDelayed(this, 100);
            }
        }
    };

    public musicMain() {

    }

    public void setupVisualizerFxAndUI() {
        try {
            mEqualizer = new Equalizer(0, mMediaPlayer.getAudioSessionId());
            mEqualizer.setEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            int pos = MainActivity.presetPos;
            if (pos != 0) {
                mEqualizer.usePreset((short) (pos - 1));
            } else {
                for (short i = 0; i < 5; i++) {
                    mEqualizer.setBandLevel(i, (short) MainActivity.seekbarpos[i]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Logic for FFT here
        mVisualizera = new Visualizer(mMediaPlayer.getAudioSessionId());
        mVisualizera.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);

        mVisualizera.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
            public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes,
                                              int samplingRate) {

            }

            public void onFftDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
                updateFFT(bytes);
            }
        }, Visualizer.getMaxCaptureRate() / 2, true, true);

        mVisualizera.setEnabled(true);
    }


    public void updateFFT(byte[] bytes) {
        mBytes = bytes;

        // Calculate average for bass segment
        float bassTotal = 0;
        for (int i = 0; i < bytes.length * BASS_SEGMENT_SIZE; i++) {
            bassTotal += Math.abs(bytes[i]);
        }
        bass = bassTotal / (bytes.length * BASS_SEGMENT_SIZE);
        bass = map((long) bass, 0, 20, 0, 255);

        // Calculate average for mid segment
        float midTotal = 0;
        for (int i = (int) (bytes.length * BASS_SEGMENT_SIZE); i < bytes.length * MID_SEGMENT_SIZE; i++) {
            midTotal += Math.abs(bytes[i]);
        }
        mid = midTotal / (bytes.length * MID_SEGMENT_SIZE);
        mid = map((long) mid, 0, 5, 0, 255);

        // Calculate average for terble segment
        float trebleTotal = 0;
        for (int i = (int) (bytes.length * MID_SEGMENT_SIZE); i < bytes.length; i++) {
            trebleTotal += Math.abs(bytes[i]);
        }
        treble = trebleTotal / (bytes.length * TREBLE_SEGMENT_SIZE);
        treble = map((long) treble, 0, 2, 0, 255);

        Log.e("fft string", "ff" + (bass <= 15 ? "0" : "") +
                Integer.toHexString((int) bass) + (mid <= 15 ? "0" : "") + Integer.toHexString((int) mid)
                + (treble <= 15 ? "0" : "") + Integer.toHexString((int) treble));
        // SEND DATA TO json

    }

    long map(long x, long in_min, long in_max, long out_min, long out_max) {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity) context;
        ctx = context;
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.music_main, container, false);
        localMusicFolder = (ImageView) rootView.findViewById(R.id.LocalMusic);
        Equalizer = (ImageView) rootView.findViewById(R.id.equalizer_icon);
        title = (TextView) rootView.findViewById(R.id.selected_track_title_sp_AB);
        artist = (TextView) rootView.findViewById(R.id.selected_track_artist_sp_AB);
        PlayerController = (ImageView) rootView.findViewById(R.id.PlayerController);
        albumPic = (ImageView) rootView.findViewById(R.id.AlbumArt);
        MusicProgress = (SeekBar) rootView.findViewById(R.id.progressBar);
        startTimeTV = (TextView) rootView.findViewById(R.id.currTime);
        finishTimeTV = (TextView) rootView.findViewById(R.id.totalTime);
        NextPlay = (ImageView) rootView.findViewById(R.id.nextTrack);
        PreviousPlay = (ImageView) rootView.findViewById(R.id.previousTrack);
        ShufflePlay = (ImageView) rootView.findViewById(R.id.shuffle_controller);
        RepeatPlay = (ImageView) rootView.findViewById(R.id.repeat_controller);
        BluetoothController = (ImageView) rootView.findViewById(R.id.BluetoothAudio_Controller);
        localMusicFolder.setOnClickListener(this);
        BluetoothController.setOnClickListener(this);
        if (mBtAdapter != null) {
            if (!mBtAdapter.isEnabled()) {
                BluetoothController.setImageResource(R.drawable.ic_bluetooth_black_24dp);
            } else {
                BluetoothController.setImageResource(R.drawable.ic_bluetooth_audio_black_24dp);
            }
        }
        //set visibility low
        Equalizer.setAlpha(0.2f);
        NextPlay.setAlpha(0.2f);
        PreviousPlay.setAlpha(0.2f);
        PlayerController.setAlpha(0.2f);

        ShufflePlay.setOnClickListener(this);
        RepeatPlay.setOnClickListener(this);

        bundle = this.getArguments();

        broadcastInitialization();
        context = getActivity();
        notificationManager = (NotificationManager) getActivity().getSystemService(NOTIFICATION_SERVICE);
        builder = new NotificationCompat.Builder(getActivity());
        remoteViews = new RemoteViews(getActivity().getPackageName(), R.layout.music_notification);

        if (oneTimeOnly == 0) {
            localMusicFolder.performClick();
            oneTimeOnly = 1;
        }
        updateUi();
        return rootView;
    }

    private AudioManager.OnAudioFocusChangeListener mOnAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
            switch (focusChange) {
                case AudioManager.AUDIOFOCUS_GAIN:
                    Log.i(TAG1, "AUDIOFOCUS_GAIN");
                    //restart/resume your sound
                    if (mLossTransient) {
                        Play();
                        mLossTransient = false;
                    }
                    if (mLossTransientCanDuck) {
                        mMediaPlayer.setVolume(100, 100);
                        mLossTransientCanDuck = false;
                    }
                    break;
                case AudioManager.AUDIOFOCUS_LOSS:
                    Log.e(TAG1, "AUDIOFOCUS_LOSS");
                    //Loss of audio focus for a long time
                    //Stop playing the sound
                    if (mMediaPlayer != null) {
                        if (mMediaPlayer.isPlaying()) {
                            Pause();
                        }
                    }
                    break;
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                    Log.e(TAG1, "AUDIOFOCUS_LOSS_TRANSIENT");
                    //Loss of audio focus for a short time
                    //Pause playing the sound
                    if (mMediaPlayer.isPlaying()) {
                        mLossTransient = true;
                        Pause();
                    }
                    break;
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                    Log.e(TAG1, "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK");
                    //Loss of audio focus for a short time.
                    //But one can duck. Lower the volume of playing the sound
                    if (mMediaPlayer.isPlaying()) {
                        mMediaPlayer.setVolume(36, 36);
                        mLossTransientCanDuck = true;
                    }
                    break;
                default:
                    //
            }
        }
    };


    private boolean requestAudioFocusForMyApp(final Context context) {
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        // Request audio focus for playback
        int result = am.requestAudioFocus(mOnAudioFocusChangeListener,
                // Use the music stream.
                AudioManager.STREAM_MUSIC,
                // Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN);

        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            Log.d(TAG, "Audio focus received");
            return true;
        } else {
            Log.d(TAG, "Audio focus NOT received");
            return false;
        }
    }

    private void broadcastInitialization() {

        IntentFilter filter = new IntentFilter();
        filter.addAction("button_click");
        filter.addAction("button_click1");
        filter.addAction("button_click2");

        onNotice = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                String action = (String) intent.getExtras().get("id");
                Log.v("action", action);
                String Action = "Pause/Play";

                if (action.equals("play")) {
                    Toast.makeText(context, "Pause/Play", Toast.LENGTH_SHORT).show();
                    playPause();
                }
                if (action.equals("previous")) {
                    Toast.makeText(context, "Previous Song", Toast.LENGTH_SHORT).show();
                    previousSong();
                }
                if (action.equals("next")) {
                    Toast.makeText(context, "Next Song", Toast.LENGTH_SHORT).show();
                    nextSong();
                }
            }
        };
        getActivity().registerReceiver(onNotice, filter);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.LocalMusic:
                if (checkPermission()) {
                    localMusicMain mlocalMusicMain = new localMusicMain();
                    mlocalMusicMain.show(getFragmentManager(), "LocalMusicList");
                }
                break;
            case R.id.equalizer_icon:
                EquilizerFragment mEquilizerFragment = new EquilizerFragment();
                mEquilizerFragment.show(getFragmentManager(), "Equilizer");
                break;
            case R.id.nextTrack:
                nextSong();
                break;
            case R.id.previousTrack:
                previousSong();
                break;
            case R.id.repeat_controller:
                repeat();
                break;
            case R.id.shuffle_controller:
                shuffle();
                break;
            case R.id.PlayerController:
                playPause();
                break;
            case R.id.BluetoothAudio_Controller:
                if (!mBtAdapter.isEnabled()) {
                    mBtAdapter.enable();
                    //new BluetoothSetting(getActivity());
                    BluetoothController.setImageResource(R.drawable.ic_bluetooth_audio_black_24dp);

                } else {
                    mBtAdapter.disable();
                    BluetoothController.setImageResource(R.drawable.ic_bluetooth_black_24dp);
                }
                break;
        }

    }

    public void updateUi() {
        if (bundle != null) {
            boolean gotFocus = requestAudioFocusForMyApp(getActivity());
            if (gotFocus) {
                index = bundle.getInt("position", defaultValue);
                SongAdapter.position = index;
                LocalTrackModel c = _songs.get(index);
                mTitle = c.getTitle();
                mArtist = c.getArtist();
                mUrl = c.getPath();
                title.setText(mTitle);
                artist.setText(mArtist);
                //This will set image on the screen
                ImageLoader imageLoader = new ImageLoader(getActivity());
                albumPic.setAlpha(0.65f);
                albumPic.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageLoader.DisplayImage(mUrl, albumPic); //needs two arguments first is url of image and second is ImageView itself.
                PlayerController.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_pause_white_48dp));
                MusicProgress.setProgress(0);
                musicNotificationUpdate(mTitle, mArtist, mUrl, null);
            }
        }

        if (mUrl != null) {
            MediaPlayer(mUrl);
        }
    }

    public void MediaPlayer(final String Url) {
        //media Player
        mMediaPlayer = new MediaPlayer();
        try {
            mMediaPlayer.setDataSource(Url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mMediaPlayer.prepareAsync();
        //when media player is running set equalizer true
        Equalizer.setAlpha(1.0f);
        Equalizer.setOnClickListener(this);
        //when media player is running set previous true
        NextPlay.setAlpha(1.0f);
        NextPlay.setOnClickListener(this);
        //when media player is running set next true
        PreviousPlay.setAlpha(1.0f);
        PreviousPlay.setOnClickListener(this);

        PlayerController.setAlpha(1.0f);
        PlayerController.setOnClickListener(this);
        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                play(Url);
            }
        });
        MusicProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mMediaPlayer.seekTo(progress);
                    myHandler.postDelayed(UpdateSongTime, 100);
                    length = mMediaPlayer.getCurrentPosition();
                    mMediaPlayer.seekTo((int) length);//update music according to seekBar position
                }
            }
        });

    }

    public void play(String Url) {
        boolean gotFocus = requestAudioFocusForMyApp(getActivity());
        if (gotFocus) {
            mMediaPlayer.start();
            Log.v("Play", "runninig");
            try {
                if (!mMediaPlayer.isPlaying()) {
                    mMediaPlayer = new MediaPlayer();
                    mMediaPlayer.setDataSource(Url);
                } else {
                    length = mMediaPlayer.getCurrentPosition();
                    finalTime = mMediaPlayer.getDuration();
                    mMediaPlayer.seekTo((int) length);
                    myHandler.postDelayed(UpdateSongTime, 100);
                }
                MusicProgress.setMax((int) finalTime); //sync song end time with seekBar
                double startTime = 0;
                long StartSecondsV = TimeUnit.MILLISECONDS.toSeconds(length) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) startTime));
                long StartminuteV = TimeUnit.MILLISECONDS.toMinutes(length);

                long FinishSecondsV = TimeUnit.MILLISECONDS.toSeconds((long) finalTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) finalTime));
                long FinishminuteV = TimeUnit.MILLISECONDS.toMinutes((long) finalTime);
                setupVisualizerFxAndUI();
                startTimeTV.setText(StartminuteV + ":" + StartSecondsV);
                finishTimeTV.setText(FinishminuteV + ":" + FinishSecondsV);
                mMediaPlayer.prepareAsync();
                mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.seekTo((int) length);
                        mp.start();
                        myHandler.postDelayed(UpdateSongTime, 100);//update time in ui
                    }
                });
            } catch (Throwable t) {
                Log.d(TAG, t.toString());
            }
            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    Log.d(TAG, "Completed Playing");
                    if (isShuffle) {
                        Random r = new Random();
                        int Low = 0;
                        int High = _songs.size();
                        SongAdapter.position = r.nextInt(High - Low) + Low;
                        //String s= String.valueOf(Result);
                        // Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
                        layoutupdate();
                    } else if (isRepeat) {
                        layoutupdate();
                    } else {
                        SongAdapter.position = SongAdapter.position + 1;
                        layoutupdate();
                    }

                }
            });
        }
    }

    private void playPause() {
        if (mMediaPlayer.isPlaying()) {
            Pause();
        } else {
            Play();
        }
    }


    private void Play() {
        PlayerController.setImageResource(R.drawable.ic_pause_white_48dp);
        musicNotificationUpdate(null, null, null, "pause");
        play(mUrl);
        Log.v("playerController", "play");
    }

    private void Pause() {
        PlayerController.setImageResource(R.drawable.ic_play_arrow_white_48dp);
        if (mMediaPlayer != null) {
            mMediaPlayer.pause();
            length = mMediaPlayer.getCurrentPosition();
            mMediaPlayer.seekTo((int) length);
            mVisualizera.setEnabled(false);
            mVisualizera.release();
        }
        musicNotificationUpdate(null, null, null, "play");
        Log.v("playerController", "pause");
    }


    private void repeat() {
        if (isRepeat) {
            isRepeat = false;
            RepeatPlay.setColorFilter(ContextCompat.getColor(getActivity(), R.color.white));
        } else {
            isRepeat = true;    // make repeat to true
            isShuffle = false; // make shuffle to false
            RepeatPlay.setColorFilter(ContextCompat.getColor(getActivity(), R.color.blue));
            ShufflePlay.setColorFilter(ContextCompat.getColor(getActivity(), R.color.white));
        }
    }

    private void shuffle() {
        if (isShuffle) {
            isShuffle = false;
            ShufflePlay.setColorFilter(ContextCompat.getColor(getActivity(), R.color.white));
        } else {
            isShuffle = true;// make repeat to true
            isRepeat = false;// make shuffle to false
            RepeatPlay.setColorFilter(ContextCompat.getColor(getActivity(), R.color.white));
            ShufflePlay.setColorFilter(ContextCompat.getColor(getActivity(), R.color.blue));
        }
    }

    public void nextSong() {
        if (_songs != null) {

            if (SongAdapter.position >= 0 && SongAdapter.position < _songs.size() - 1) {
                SongAdapter.position = SongAdapter.position + 1;
            }

            if (SongAdapter.position >= _songs.size() - 1) {
                SongAdapter.position = 0;
            }
            layoutupdate();
        }
    }

    public void previousSong() {
        if (_songs != null) {
            if (SongAdapter.position > 0 && SongAdapter.position <= _songs.size() - 1) {
                SongAdapter.position = SongAdapter.position - 1;
            }

            if (SongAdapter.position == 0) {
                SongAdapter.position = _songs.size() - 1;
            }
            layoutupdate();
        }
    }

    public void layoutupdate() {
        LocalTrackModel c = _songs.get(position);
        mMediaPlayer.stop();
        mMediaPlayer.release();
        mTitle = c.getTitle();
        mArtist = c.getArtist();
        mUrl = c.getPath();
        title.setText(mTitle);
        artist.setText(mArtist);
        //This will set image on the screen
        ImageLoader imageLoader = new ImageLoader(getActivity());
        albumPic.setAlpha(0.65f);
        albumPic.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageLoader.DisplayImage(mUrl, albumPic); //needs two arguments first is url of image and second is ImageView itself.
        PlayerController.setImageResource(R.drawable.ic_pause_white_48dp);
        MusicProgress.setProgress(0);
        mVisualizera.release();
        MediaPlayer(mUrl);
        musicNotificationUpdate(mTitle, mArtist, mUrl, null);
    }


    public void NotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ONE_ID,
                    CHANNEL_ONE_NAME, notifManager.IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.CYAN);
            notificationChannel.setShowBadge(false);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            getManager().createNotificationChannel(notificationChannel);
        }
    }


    public void musicNotificationUpdate(String ntitle, String nartist, String nurl, String nstatus) {
        NotificationChannel();
        notification_id = 122165723;
        remoteViews.setImageViewResource(R.id.nPlayerController, R.drawable.ic_pause_white_48dp);
        builder.setOngoing(true);


        if (nstatus != null) {
            if (nstatus.equals("play")) {
                remoteViews.setImageViewResource(R.id.nPlayerController, R.drawable.ic_play_arrow_white_48dp);
                builder.setOngoing(false);

            } else if (nstatus.equals("pause")) {
                remoteViews.setImageViewResource(R.id.nPlayerController, R.drawable.ic_pause_white_48dp);
                builder.setOngoing(true);
            }
        }

        if (ntitle != null) {
            remoteViews.setTextViewText(R.id.nSongName, ntitle);
        }
        if (nartist != null) {
            remoteViews.setTextViewText(R.id.nArtistName, nartist);
        }

        Bitmap remote_picture = null;
        if (nurl != null) {
            ImageLoader imageLoader = new ImageLoader(getActivity());
            remote_picture = imageLoader.getBitmap(nurl);

            if (remote_picture == null) {
                remoteViews.setImageViewResource(R.id.nAlbumPic, R.drawable.musicwall);
            } else {
                remoteViews.setImageViewBitmap(R.id.nAlbumPic, remote_picture);
            }

            Intent button_intent = new Intent("button_click");
            button_intent.putExtra("id", "play");
            PendingIntent button_pending_event = PendingIntent.getBroadcast(context, 1,
                    button_intent, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.nPlayerController, button_pending_event);

            Intent button_intent1 = new Intent("button_click1");
            button_intent1.putExtra("id", "next");
            PendingIntent button_pending_event1 = PendingIntent.getBroadcast(context, 2,
                    button_intent1, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.nnextTrack, button_pending_event1);

            Intent button_intent2 = new Intent("button_click2");
            button_intent2.putExtra("id", "previous");
            PendingIntent button_pending_event2 = PendingIntent.getBroadcast(context, 3,
                    button_intent2, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.npreviousTrack, button_pending_event2);

        }

        Intent notification_intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notification_intent, 0);

        builder.setSmallIcon(R.drawable.ic_notification_round)
                .setColor(0xfb00b44f)
                .setAutoCancel(false)
                .setCustomBigContentView(remoteViews)
                .setLargeIcon(remote_picture)
                .setPriority(PRIORITY_MAX)
                .setContentTitle(mTitle)
                .setContentText(mArtist)
                .setContentIntent(pendingIntent)
                .setChannelId(CHANNEL_ONE_ID)
                .setOnlyAlertOnce(true);

        notificationManager.notify(notification_id, builder.build());
    }


    private NotificationManager getManager() {
        if (notifManager == null) {
            notifManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notifManager;
    }

    public boolean checkPermission() {
        int permissionreadStorage = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            permissionreadStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        int RecordPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO);
        Log.d("RecordPermission", String.valueOf(RecordPermission)); //returns -1 for Deny and 0 for allow
        Log.d("StoragePermission", String.valueOf(permissionreadStorage));
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionreadStorage != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
        }
        if (RecordPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO);
        }
        if (!listPermissionsNeeded.isEmpty()) {

            for (String s : listPermissionsNeeded) {
                Log.d("My array list content: ", s);
            }
            requestPermissions(listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]), READ_MULTIPLE_PERMISSION_REQUEST);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        Log.d("Mulitiple Permissions", "Permission callback called-------");
        switch (requestCode) {
            case READ_MULTIPLE_PERMISSION_REQUEST: {
                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                }
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Multiple Permissions", "audio & storage services permission granted");

                    } else {
                        Log.d("SmartHub:", "Some permissions are not granted ask again ");
                        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.RECORD_AUDIO) ||
                                ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Record Audio and External Storage Services Permission required for this Feature",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkPermission();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    Toast.makeText(getActivity(), "Music sync feature will not work unless all permissions are enabled", Toast.LENGTH_LONG).show();
                                                    break;
                                            }
                                        }
                                    });
                        } else {

                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle("Music sync");
                            builder.setMessage("You will not be able to access the Media Player.Please Enable All the Permissions required in settings");
                            builder.setCancelable(true);
                            final AlertDialog closedialog = builder.create();
                            closedialog.show();
                            final Timer timer2 = new Timer();
                            timer2.schedule(new TimerTask() {
                                public void run() {
                                    closedialog.dismiss();
                                    timer2.cancel(); //this will cancel the timer of the system
                                }
                            }, 5000); // the timer will count 5 seconds....

                        }

                    }
                }
            }
        }
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("onCreateSave", "Called");
    }

    @Override
    public void onDetach() {
        Log.v("Detach", "the activity is Stopped");
        super.onDetach();
    }

    @Override
    public void onStop() {
        Log.v("stop", "the activity is Stopped");
        super.onStop();
    }

    @Override
    public void onPause() {
        Log.v("Pause", "the activity is paused");
        super.onPause();
    }

    @Override
    public void onStart() {
        Log.v("Start", "the activity is Start");
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        super.onDestroy();
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(122165723);


        if (mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.stop();
                mVisualizera.release();
            }
            getActivity().unregisterReceiver(onNotice);
            onNotice = null;
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

}