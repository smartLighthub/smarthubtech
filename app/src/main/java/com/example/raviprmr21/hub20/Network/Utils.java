package com.example.raviprmr21.hub20.Network;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.support.v7.app.AlertDialog;
import android.text.ClipboardManager;
import android.text.format.Formatter;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.example.raviprmr21.hub20.BuildConfig;
import com.example.raviprmr21.hub20.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.channels.FileChannel;
import java.util.Collections;
import java.util.Iterator;

public class Utils {
    private static OnActionListener listener;
    public  static AlertDialog.Builder builder;
    private static String[] reslove_mascs = new String[]{"0.0.0.0", "128.0.0.0", "192.0.0.0", "224.0.0.0", "240.0.0.0", "248.0.0.0", "252.0.0.0", "254.0.0.0", "255.0.0.0", "255.128.0.0", "255.192.0.0", "255.224.0.0", "255.240.0.0", "255.248.0.0", "255.252.0.0", "255.254.0.0", "255.255.0.0", "255.255.128.0", "255.255.192.0", "255.255.224.0", "255.255.240.0", "255.255.248.0", "255.255.252.0", "255.255.254.0", "255.255.255.0", "255.255.255.128", "255.255.255.192", "255.255.255.224", "255.255.255.240", "255.255.255.248", "255.255.255.252", "255.255.255.254", "255.255.255.255"};
    public static Drawable stop;

    public interface OnActionListener {
        void BHide();

        void BShow();
    }
    public static String[] to_str_arr(String[] arr, String line) {
        String[] arr2 = new String[(arr.length + 1)];
        for (int l = 0; l < arr.length; l++) {
            arr2[l] = arr[l];
        }
        arr2[arr.length] = line;
        return (String[]) arr2.clone();
    }

    public static String[] get_interfaces_names() {
        String[] res = new String[0];
        try {
            Iterator it = Collections.list(NetworkInterface.getNetworkInterfaces()).iterator();
            while (it.hasNext()) {
                String dev = ((NetworkInterface) it.next()).getName();
                if ((dev.charAt(0) == 'w' && dev.charAt(1) == 'l' && dev.charAt(2) == 'a' && dev.charAt(3) == 'n') || (dev.charAt(0) == 'e' && dev.charAt(1) == 't' && dev.charAt(2) == 'h')) {
                    res = to_str_arr(res, dev);
                }
            }
        } catch (SocketException e) {
        }
        return res;
    }


    public static byte[] int_to_byte(long ch) {
        return new byte[]{(byte) ((int) ((ch >> 24) & 255)), (byte) ((int) ((ch >> 16) & 255)), (byte) ((int) ((ch >> 8) & 255)), (byte) ((int) (ch & 255))};
    }

    public static int byte_to_int(byte[] b) {
        return ((((b[3] & 255) << 24) | ((b[2] & 255) << 16)) | ((b[1] & 255) << 8)) | (b[0] & 255);
    }

    public static void copy_file(File from, File to) throws IOException {
        if (!to.exists()) {
            to.createNewFile();
        }
        FileChannel in = new FileInputStream(from).getChannel();
        new FileOutputStream(to).getChannel().transferFrom(in, 0, in.size());
    }

    public static String read_file(String filename) {
        if (!new File(filename).exists()) {
            return BuildConfig.FLAVOR;
        }
        try {
            FileInputStream inputstream = new FileInputStream(filename);
            if (inputstream != null) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputstream));
                StringBuffer buffer = new StringBuffer();
                while (true) {
                    String str = reader.readLine();
                    if (str != null) {
                        buffer.append(str + "\n");
                    } else {
                        inputstream.close();
                        return buffer.toString();
                    }
                }
            }
            inputstream.close();
            return BuildConfig.FLAVOR;
        } catch (Throwable th) {
        }
        return filename;
    }

    public static void write_file(String path, String str) {
        try {
            File file = new File(path);
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(file));
            osw.write(str);
            osw.close();
        } catch (Throwable th) {
        }
    }

    public static void write_bytes(String path, byte[] str) {
        try {
            File file = new File(path);
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            FileOutputStream outputstream = new FileOutputStream(file);
            outputstream.write(str);
            outputstream.close();
        } catch (Throwable th) {
        }
    }

    public static void setOnActionListener(OnActionListener l) {
        listener = l;
    }

    public static void resolver(String[] res, Vendors vendorResolve, String mac, String ip, String my_ip, String mymac, String gateway, boolean in_lan, boolean RESOLVE_MAC, boolean RESOLVE_NAME, boolean RESOLVE_VENDOR, boolean USE_NETBIOS, int NS_TIMEOUT) {
        char icon = 'o';
        String vendor = "Vendor";
        String name = ip;
        String linkstr = BuildConfig.FLAVOR;
        if (RESOLVE_MAC && in_lan && mac.isEmpty()) {
            if (ip.equals(my_ip)) {
                mac = mymac;
            } else {
                mac = get_arp_mac(ip);
            }
        }
        if (in_lan) {
            if (ip.equals(gateway)) {
                icon = '1';
            } else if (ip.equals(my_ip)) {
                icon = '0';
            } else {
                icon = '2';
            }
        }
        if (RESOLVE_NAME) {
            name = get_hostname(ip, USE_NETBIOS, NS_TIMEOUT);
            if (ip.equals(my_ip)) {
                name = name + " (This device)";
            } else if (ip.equals(gateway)) {
                name = name + " (Gateway)";
            }
        }
        if (RESOLVE_VENDOR && !mac.isEmpty()) {
            vendor = !mac.equals(mymac) ? vendorResolve.get_vendor_by_mac(mac) : get_my_vendor();
        }
        res[0] = BuildConfig.FLAVOR + icon;
        res[1] = name;
        res[2] = vendor;
        res[3] = ip;
        res[4] = mac;
    }

    public static String get_arp_ip(String mac, String non_ip) {
        try {
            FileInputStream inputstream = new FileInputStream("/proc/net/arp");
            if (inputstream != null) {
                InputStreamReader isr = new InputStreamReader(inputstream);
                BufferedReader reader = new BufferedReader(isr);
                while (true) {
                    String line = reader.readLine();
                    if (line == null) {
                        break;
                    } else if (line.contains(mac)) {
                        String ip = BuildConfig.FLAVOR;
                        String tst_mac = BuildConfig.FLAVOR;
                        int llen = line.length();
                        int l = 0;
                        while (l < llen && line.charAt(l) != ' ') {
                            ip = ip + line.charAt(l);
                            l++;
                        }
                        if (ip.equals(non_ip)) {
                            continue;
                        } else {
                            while (l < llen && line.charAt(l) == ' ') {
                                l++;
                            }
                            while (l < llen && line.charAt(l) != ' ') {
                                l++;
                            }
                            while (l < llen && line.charAt(l) == ' ') {
                                l++;
                            }
                            while (l < llen && line.charAt(l) != ' ') {
                                l++;
                            }
                            while (l < llen && line.charAt(l) == ' ') {
                                l++;
                            }
                            while (l < llen && line.charAt(l) != '\n' && line.charAt(l) != ' ') {
                                tst_mac = tst_mac + line.charAt(l);
                                l++;
                            }
                            if (mac.equals(tst_mac)) {
                                reader.close();
                                isr.close();
                                inputstream.close();
                                return ip;
                            }
                        }
                    }
                }
                reader.close();
                isr.close();
                inputstream.close();
            }
        } catch (Throwable ignored) {
        }
        return BuildConfig.FLAVOR;
    }

    public static String get_arp_mac(String ip) {
        try {
            FileInputStream inputstream = new FileInputStream("/proc/net/arp");
            if (inputstream != null) {
                InputStreamReader isr = new InputStreamReader(inputstream);
                BufferedReader reader = new BufferedReader(isr);
                while (true) {
                    String line = reader.readLine();
                    if (line == null) {
                        break;
                    } else if (line.contains(ip)) {
                        String mac = BuildConfig.FLAVOR;
                        String tst_ip = BuildConfig.FLAVOR;
                        int llen = line.length();
                        int l = 0;
                        while (l < llen && line.charAt(l) != ' ') {
                            tst_ip = tst_ip + line.charAt(l);
                            l++;
                        }
                        if (ip.equals(tst_ip)) {
                            while (l < llen && line.charAt(l) == ' ') {
                                l++;
                            }
                            while (l < llen && line.charAt(l) != ' ') {
                                l++;
                            }
                            while (l < llen && line.charAt(l) == ' ') {
                                l++;
                            }
                            while (l < llen && line.charAt(l) != ' ') {
                                l++;
                            }
                            while (l < llen && line.charAt(l) == ' ') {
                                l++;
                            }
                            while (l < llen && line.charAt(l) != '\n' && line.charAt(l) != ' ') {
                                mac = mac + line.charAt(l);
                                l++;
                            }
                            if (!(mac.equals("00:00:00:00:00:00") || mac.isEmpty())) {
                                reader.close();
                                isr.close();
                                inputstream.close();
                                return mac;
                            }
                        }
                        continue;
                    }
                }
                reader.close();
                isr.close();
                inputstream.close();
            }
        } catch (Throwable th) {
        }
        return BuildConfig.FLAVOR;
    }

    public static String get_host_name(String hostip, int NS_TIMEOUT) {
        String line2 = BuildConfig.FLAVOR;
        String line = BuildConfig.FLAVOR;
        try {
            int i;
            DatagramSocket clientSocket = new DatagramSocket();
            clientSocket.setSoTimeout(NS_TIMEOUT);
            InetAddress IPAddress = InetAddress.getByName(hostip);
            byte[] sendData = new byte[1024];
            sendData[0] = (byte) -104;
            sendData[1] = (byte) 1;
            sendData[2] = (byte) 0;
            sendData[3] = (byte) 16;
            sendData[4] = (byte) 0;
            sendData[5] = (byte) 1;
            for (i = 0; i < 6; i++) {
                sendData[i + 6] = (byte) 0;
            }
            sendData[12] = (byte) 32;
            sendData[13] = (byte) 67;
            sendData[14] = (byte) 75;
            for (i = 0; i < 30; i++) {
                sendData[i + 15] = (byte) 65;
            }
            sendData[45] = (byte) 0;
            sendData[46] = (byte) 0;
            sendData[47] = (byte) 33;
            sendData[48] = (byte) 0;
            sendData[49] = (byte) 1;
            byte[] receiveData = new byte[1024];
            clientSocket.send(new DatagramPacket(sendData, 50, IPAddress, 137));
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            clientSocket.receive(receivePacket);
            clientSocket.close();
            receiveData = receivePacket.getData();
            byte[] result = new byte[1024];
            int j = 93;
            i = 0;
            while (receiveData[j] != (byte) 0 && receiveData[j] != (byte) 32 && j < receiveData.length) {
                result[i] = receiveData[j];
                i++;
                j++;
            }
            result[i] = (byte) 0;
            String line3 = new String(result);
            i = 0;
            while (line3.charAt(i) != '\u0000') {
                line2 = line2 + line3.charAt(i);
                i++;
            }
            return line2;
        } catch (UnknownHostException e4) {
            return BuildConfig.FLAVOR;
        } catch (SocketException e5) {
            return BuildConfig.FLAVOR;
        } catch (IOException e6) {
            return BuildConfig.FLAVOR;
        }
    }

    public static String get_hostname(String ip, boolean USE_NETBIOS, int NS_TIMEOUT) {
        String line = BuildConfig.FLAVOR;
        if (USE_NETBIOS) {
            line = get_host_name(ip, NS_TIMEOUT);
        }
        if (!line.isEmpty()) {
            return line;
        }
        String dns = BuildConfig.FLAVOR;
        try {
            dns = InetAddress.getByName(ip).getHostName();
        } catch (UnknownHostException e) {
        }
        if (dns.equals(BuildConfig.FLAVOR)) {
            return line + "Unknown host";
        }
        return line + dns;
    }

    public static long[] get_lan_range(Context context) {
        int netmask_pref;
        WifiManager wm = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        String my_ip = null;
        if (wm != null) {
            my_ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        }
        String netmask = BuildConfig.FLAVOR;
        try {
            NetworkInterface networkInterface = NetworkInterface.getByInetAddress(InetAddress.getByName(my_ip));
            if (networkInterface.getInterfaceAddresses().size() < 2) {
                netmask_pref = ((InterfaceAddress) networkInterface.getInterfaceAddresses().get(0)).getNetworkPrefixLength();
            } else {
                netmask_pref = ((InterfaceAddress) networkInterface.getInterfaceAddresses().get(1)).getNetworkPrefixLength();
            }
            if (netmask_pref > 32) {
                netmask_pref = 0;
            }
            netmask = get_masc_by_pref(netmask_pref);
        } catch (SocketException e) {
            netmask_pref = 0;
        } catch (UnknownHostException e2) {
            netmask_pref = 0;
        } catch (NullPointerException e3) {
            netmask_pref = 0;
        }
        if (netmask_pref == 0) {
            try {
                int n = wm.getDhcpInfo().netmask;
                if (n != 0) {
                    netmask = Formatter.formatIpAddress(n);
                    for (netmask_pref = 0; (n >> netmask_pref) != 0; netmask_pref++) {
                    }
                } else {
                    netmask = "255.255.255.0";
                }
            } catch (Exception e4) {
                netmask = "255.255.255.0";
            }
        }
        long masc = str_to_ip(netmask);
        long start_ip = str_to_ip(my_ip) & masc;
        long range_ip = masc ^ -1;
        return new long[]{start_ip, start_ip | range_ip};
    }

    public static String ip_to_str(long ip) {
        byte[] b = ip_to_byte(ip);
        return Integer.toString(b[0] & 255) + "." + Integer.toString(b[1] & 255) + "." + Integer.toString(b[2] & 255) + "." + Integer.toString(b[3] & 255);
    }

    public static byte[] ip_to_byte(long ch) {
        return new byte[]{(byte) ((int) (ch >> 24)), (byte) ((int) ((ch >> 16) & 255)), (byte) ((int) ((ch >> 8) & 255)), (byte) ((int) (ch & 255))};
    }

    public static long str_to_mac(String addr) {
        int i = 0;
        long ip_addr = 0;
        long num = 0;
        try {
            long len = (long) addr.length();
            for (int sdvig = 40; sdvig >= 0; sdvig -= 8) {
                String buff = BuildConfig.FLAVOR;
                while (((long) i) < len && addr.charAt(i) != ':') {
                    buff = buff + addr.charAt(i);
                    i++;
                }
                i++;
                long a = Long.parseLong(buff, 16);
                if (a > 255) {
                    return Long.MAX_VALUE;
                }
                ip_addr |= a << sdvig;
                num++;
            }
            return num != 6 ? Long.MAX_VALUE : ip_addr;
        } catch (Exception e) {
            return Long.MAX_VALUE;
        }
    }

    public static long str_to_ip(String addr) {
        int i = 0;
        int ip_addr = 0;
        try {
            int len = addr.length();
            for (int sdvig = 24; sdvig >= 0; sdvig -= 8) {
                String buff = BuildConfig.FLAVOR;
                while (i < len && addr.charAt(i) != '.') {
                    buff = buff + addr.charAt(i);
                    i++;
                }
                i++;
                ip_addr |= Integer.parseInt(buff) << sdvig;
            }
            return (long) ip_addr;
        } catch (Exception e) {
            return -1;
        }
    }

    public static String get_masc_by_pref(int pref) {
        return reslove_mascs[pref];
    }

    public static String get_my_mac(Context context) {
        SharedPreferences mSettings = context.getSharedPreferences("settings", 0);
        String mac = BuildConfig.FLAVOR;
        String res = BuildConfig.FLAVOR;
        if (mSettings.contains("mymacaddr")) {
            mac = mSettings.getString("mymacaddr", BuildConfig.FLAVOR);
            if (mac != null && !mac.isEmpty()) {
                return mac;
            }
            mSettings.edit().remove("mymacaddr").apply();
        }
        if (VERSION.SDK_INT < 23 || VERSION.SDK_INT >= 24) {
            mac = ((WifiManager) context.getSystemService(Context.WIFI_SERVICE)).getConnectionInfo().getMacAddress();
            if (mac == null) {
                mac = BuildConfig.FLAVOR;
            }
            if (VERSION.SDK_INT < 24 && !mac.isEmpty()) {
                Editor editor = mSettings.edit();
                editor.putString("mymacaddr", mac);
                editor.apply();
            }
            return mac;
        }
        mac = read_file("/sys/class/net/wlan0/address");
        if (mac.isEmpty()) {
            mac = ((WifiManager) context.getSystemService(Context.WIFI_SERVICE)).getConnectionInfo().getMacAddress();
            if (mac == null) {
                mac = BuildConfig.FLAVOR;
            }
            return mac;
        }
        int len = mac.length();
        for (int i = 0; i < len; i++) {
            if (mac.charAt(i) != '\n') {
                res = res + mac.charAt(i);
            }
        }
//        editor = mSettings.edit();
//        editor.putString("mymacaddr", res);
//        editor.apply();
        return res;
    }

    private static String to_big(String str) {
        String res = BuildConfig.FLAVOR;
        int i = 0;
        int len = str.length();
        while (i < len) {
            if (str.charAt(i) < 'a' || str.charAt(i) > 'z') {
                res = res + str.charAt(i);
            } else {
                res = res + ((char) (str.charAt(i) - 32));
            }
            i++;
        }
        return res;
    }

    public static String get_my_vendor() {
        String model = Build.MODEL;
        String manuf = Build.MANUFACTURER;
        return model.startsWith(manuf) ? model : to_big(manuf) + " " + model;
    }
}
