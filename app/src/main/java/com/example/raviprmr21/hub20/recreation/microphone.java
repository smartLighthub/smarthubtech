package com.example.raviprmr21.hub20.recreation;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.raviprmr21.hub20.Model.ColorModel;
import com.example.raviprmr21.hub20.R;
import com.example.raviprmr21.hub20.View.ColorDialog;
import com.nhaarman.supertooltips.ToolTip;
import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.nhaarman.supertooltips.ToolTipView;

/**
 * Created by raviprmr21 on 12/6/17.
 */

public class microphone extends AppCompatActivity
        implements MicLevelReader.MicLevelReaderValueListener,ColorDialog.ColorDialogListener,
        View.OnClickListener{

    private static final int REQUEST_RECORD_AUDIO = 121;
    private Thread mRecorderThread = null;
    private double mMeterValue = 0;
    private MicLevelReader mMicLevelReader;
    ImageView recordmic;
    Button ColorPick;
    int touchRGB = -13369414;
    public Context ctx;
    private int brightness;
    ColorModel colormodel;
    public String lastsenddata;
    private int miclevel;
    private String finaltouchRGB;

    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (mMicLevelReader.isRunning()) {
                updateVuData(mMeterValue);
                //    System.out.println(mMeterValue);
            }

        }
    };



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recreation_microphone);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        recordmic = (ImageView) findViewById(R.id.recordMic);
        ColorPick =(Button)findViewById(R.id.Mic_pickColor);
        colormodel= new ColorModel();
        recordmic.setImageLevel(0);
        mMicLevelReader = new MicLevelReader(this, LevelMethod.Max);
        startit();
        checkMicrophoneAccess();
        ColorPick.setOnClickListener(this);
        ColorPick.setBackgroundColor(touchRGB);

    }
    @Override
    public void onClick(View view) {
        ColorDialog dialogFragment = new ColorDialog();
        dialogFragment.setCallBack(this);
        dialogFragment.show(getSupportFragmentManager(), "ColorSelect");

    }
    private boolean checkMicrophoneAccess() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_RECORD_AUDIO);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_RECORD_AUDIO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(this,"Need Microphone to work", Toast.LENGTH_LONG).show();
                }
            }
        }
    }


    @Override
    protected void onPause() {

        if (mMicLevelReader.isRunning()) {
            stopit();
        }
        super.onPause();
    }

    @Override
    public void valueCalculated(double level) {
        mMeterValue = level;
        mHandler.obtainMessage(1).sendToTarget();

    }



    void updateVuData(double micData)
    {
        if(micData<2000)
        {
            recordmic.setImageLevel(0);
            miclevel=0;
        }
        else if(micData>2000 && micData <4000)
        {
            recordmic.setImageLevel(1);
            miclevel=1;

        }
        else if(micData>4000 && micData <6000)
        {
            recordmic.setImageLevel(2);
            miclevel=2;

        }
        else if(micData>6000 && micData <8000)
        {
            recordmic.setImageLevel(3);
            miclevel=3;

        }
        else if(micData>8000 && micData <10000)
        {
            recordmic.setImageLevel(4);
            miclevel=4;

        }
        else if(micData>10000 && micData <12000)
        {
            recordmic.setImageLevel(5);
            miclevel=5;

        }
        else if(micData>12000 && micData <14000)
        {
            recordmic.setImageLevel(6);
            miclevel=6;

        }
        else if(micData>14000 && micData <16000)
        {
            recordmic.setImageLevel(7);
            miclevel=7;

        }
        else if(micData>16000 && micData <18000)
        {
            recordmic.setImageLevel(8);
            miclevel=8;
        }
        else if(micData>18000 && micData <20000)
        {
            recordmic.setImageLevel(9);
            miclevel=9;
        }
        else if(micData>20000 && micData <22000)
        {
            recordmic.setImageLevel(10);
            miclevel=10;
        }
        else if(micData>22000 && micData <24000)
        {
            recordmic.setImageLevel(11);
            miclevel=11;
        }
        else if(micData>24000 && micData <26000)
        {
            recordmic.setImageLevel(12);
            miclevel=12;
        }
        else if(micData>26000 && micData <28000)
        {
            recordmic.setImageLevel(13);
            miclevel=13;
        }
        else if(micData>28000 && micData <30000)
        {
            recordmic.setImageLevel(14);
            miclevel=14;
        }
        else if(micData>30000 && micData <32000)
        {
            recordmic.setImageLevel(15);
            miclevel=15;
        }
        else if (micData>32000 )
        {
            recordmic.setImageLevel(16);
            miclevel=16;
        }
        int a = Color.alpha(touchRGB);
        int r = Color.red(touchRGB);
        int g = Color.green(touchRGB);
        int b = Color.blue(touchRGB);
        a = map(miclevel,0,16,0,255);// makemultipal digit of map value

        finaltouchRGB =(a <= 15 ? "0" : "") + Integer.toHexString(a)+(r <= 15 ? "0" : "")+
                Integer.toHexString(r)+(g <= 15 ? "0" : "")+Integer.toHexString(g)
                +(b <= 15 ? "0" : "")+Integer.toHexString(b);

        microphonedata(finaltouchRGB);
    }




    public boolean startit() {
        if (checkMicrophoneAccess()) {
            mRecorderThread = new Thread(mMicLevelReader, "AudioListener Thread");
            mRecorderThread.start();
            Window w = this.getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            return true;
        }
        return false;
    }


    public void stopit() {
        if (mMicLevelReader.isRunning()) {
            mMicLevelReader.stop();
            Window w = this.getWindow();
            w.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            try {
                mRecorderThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        recordmic.setImageLevel(0);

    }

    @Override
    protected void onStop() {
        stopit();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        stopit();
        super.onDestroy();
    }

    @Override
    public void onDialogPositiveClick(ColorModel model) {
    touchRGB = model.getTouchRGBDATA();
     Log.v("touchrgbcolor:", String.valueOf(touchRGB));
        ColorPick.setBackgroundColor(touchRGB);
        brightness = Color.alpha(touchRGB);
        Log.v("brightness", String.valueOf(brightness));
    }

    int map(int x, int in_min, int in_max, int out_min, int out_max)
    {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

    public  void microphonedata (String value){

        if(!value.equals(lastsenddata)) {
            Log.e("microphonedata", value);
            lastsenddata=value;
        }
    }
}

