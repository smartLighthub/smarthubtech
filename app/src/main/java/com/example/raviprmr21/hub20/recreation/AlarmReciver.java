package com.example.raviprmr21.hub20.recreation;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Locale;

import static com.example.raviprmr21.hub20.recreation.alarm.Fri;
import static com.example.raviprmr21.hub20.recreation.alarm.Mon;
import static com.example.raviprmr21.hub20.recreation.alarm.Sat;
import static com.example.raviprmr21.hub20.recreation.alarm.Sun;
import static com.example.raviprmr21.hub20.recreation.alarm.Thur;
import static com.example.raviprmr21.hub20.recreation.alarm.Tue;
import static com.example.raviprmr21.hub20.recreation.alarm.Wed;
import static com.example.raviprmr21.hub20.recreation.alarm.alarmManager;
import static com.example.raviprmr21.hub20.recreation.alarm.pendingIntent;
import static com.example.raviprmr21.hub20.recreation.alarm.time;

public class AlarmReciver extends BroadcastReceiver
{
    private long currenttime;
    private Boolean check;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Toast.makeText(context, "Alarm! Wake up! Wake up!", Toast.LENGTH_LONG).show();
        Calendar calendar = Calendar.getInstance();
        String dayLongName = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
        Log.e("alarm","Day on time : " +dayLongName);
        if(dayLongName.equals("Monday")){
            check=Mon;
            Log.e("Mon", String.valueOf(check));
        }
        if(dayLongName.equals("Tuesday")){
            check=Tue;
        }
        if(dayLongName.equals("Wednesday")){
            check=Wed;
        }
        if(dayLongName.equals("Thursday")){
            check=Thur;
        }
        if(dayLongName.equals("Friday")){
            check=Fri;
        }
        if(dayLongName.equals("Saturday")){
            check=Sat;
        }
        if(dayLongName.equals("Sunday")){
            check=Sun;
        }

        currenttime=(calendar.getTimeInMillis()-(calendar.getTimeInMillis()%60000));
        if(currenttime==time && check){
            Log.e("alarm","is on time" +currenttime);
            alarmManager.cancel(pendingIntent);
        }
        else if(currenttime==time){
            Log.e("alarm","is on time but day is off" );
            alarmManager.cancel(pendingIntent);
        }
    }
}