package com.example.raviprmr21.hub20.recreation;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;


class MicLevelReader implements Runnable {
    private static final int RECORDER_SAMPLERATE = 16000;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private static final int RECORDER_BUFFER_SIZE = RECORDER_SAMPLERATE/40;

    private boolean mIsRunning = false;
    private LevelMethod mLevelMethod = LevelMethod.RMS;

    private final MicLevelReaderValueListener mValueListener;

    MicLevelReader(MicLevelReaderValueListener valueListener, LevelMethod levelMethod) {
        mValueListener = valueListener;
        mLevelMethod = levelMethod;
    }

    boolean isRunning() {
        return mIsRunning;
    }

    void stop() {
        mIsRunning = false;
    }

    @Override
    public void run() {

        int bufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE,
                RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);
        AudioRecord recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                RECORDER_SAMPLERATE, RECORDER_CHANNELS,
                RECORDER_AUDIO_ENCODING, bufferSize);
        try {

            recorder.startRecording();
            mIsRunning = true;
            short sData[] = new short[RECORDER_BUFFER_SIZE];
            while (mIsRunning && recorder.getState()== AudioRecord.STATE_INITIALIZED) {

                int read = recorder.read(sData, 0, RECORDER_BUFFER_SIZE);
                if (read==0) {
                    continue;
                }
                double scale = 1.0;
                double resultval = mLevelMethod.calculate(sData, read, scale);
                mValueListener.valueCalculated(resultval);
            }
        } finally {
            recorder.stop();
            recorder.release();
        }
    }

    interface MicLevelReaderValueListener {

        void valueCalculated(double level);

    }
}
