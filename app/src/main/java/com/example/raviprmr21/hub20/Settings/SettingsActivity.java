package com.example.raviprmr21.hub20.Settings;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toolbar;

import com.example.raviprmr21.hub20.R;
import com.mikepenz.materialdrawer.Drawer;

import static com.example.raviprmr21.hub20.MainActivity.lastSelectedPosition;

/**
 * Created by ${RaviParmar} on ${18/08/17}.
 */

public class SettingsActivity extends AppCompatActivity {
    private Drawer result = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        ActionBar actionBar = this.getSupportActionBar();
        if (actionBar != null) {
            // Set the action bar back button to look like an up button
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.red)));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home ) {
            NavUtils.navigateUpFromSameTask(this);
        }
        return super.onOptionsItemSelected(item);
    }
    @Override

    public void onBackPressed() {
        //handle the back press :D close the drawer first and if the drawer is closed close the activity
        Log.e("backpress", String.valueOf(lastSelectedPosition));
        if (result != null && result.isDrawerOpen()) {
            result.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }
}
