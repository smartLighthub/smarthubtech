package com.example.raviprmr21.hub20.recreation;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.raviprmr21.hub20.MainActivity;
import com.example.raviprmr21.hub20.R;

import java.util.Calendar;

/**
 * Created by raviprmr21 on 12/6/17.
 */

public class alarm extends AppCompatActivity
{

    TimePicker alarmTimePicker;
    public static PendingIntent pendingIntent;
    public static AlarmManager alarmManager;
    public static long time;

    Button Monbutton,Tuebutton,Wedbutton,Thurbutton,Fributton,Satbutton,Sunbutton;
    public static Boolean Mon=false,Tue=false,Wed=false,Thur=false,Fri=false,Sat=false,Sun=false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recreation_alarm);
        alarmTimePicker = (TimePicker) findViewById(R.id.timePicker);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Monbutton=(Button) findViewById(R.id.monalarm);
        Tuebutton=(Button) findViewById(R.id.tuealarm);
        Wedbutton=(Button) findViewById(R.id.wedalarm);
        Thurbutton=(Button) findViewById(R.id.thuralarm);
        Fributton=(Button) findViewById(R.id.frialarm);
        Satbutton=(Button) findViewById(R.id.satalarm);
        Sunbutton=(Button) findViewById(R.id.sunalarm);

        if(Mon){Monbutton.setBackgroundResource(R.drawable.weekon);}
        else{Monbutton.setBackgroundResource(R.drawable.weekoff);}
        if(Tue){Tuebutton.setBackgroundResource(R.drawable.weekon);}
        else {Tuebutton.setBackgroundResource(R.drawable.weekoff);}
        if(Wed){Wedbutton.setBackgroundResource(R.drawable.weekon);}
        else{Wedbutton.setBackgroundResource(R.drawable.weekoff);}
        if(Thur){Thurbutton.setBackgroundResource(R.drawable.weekon);}
        else{Thurbutton.setBackgroundResource(R.drawable.weekoff);}
        if(Fri){Fributton.setBackgroundResource(R.drawable.weekon);}
        else{Fributton.setBackgroundResource(R.drawable.weekoff);}
        if(Sat){Satbutton.setBackgroundResource(R.drawable.weekon);}
        else{ Satbutton.setBackgroundResource(R.drawable.weekoff);}
        if(Sun){Sunbutton.setBackgroundResource(R.drawable.weekon);}
        else{Sunbutton.setBackgroundResource(R.drawable.weekoff);}


        Monbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Mon=changeboolen(Mon);
                Log.e("Mon", String.valueOf(Mon));
                if(Mon){Monbutton.setBackgroundResource(R.drawable.weekon);}
                else{Monbutton.setBackgroundResource(R.drawable.weekoff);}
            }
        });

        Tuebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Tue=changeboolen(Tue);
                Log.e("Tue", String.valueOf(Tue));
                if(Tue){Tuebutton.setBackgroundResource(R.drawable.weekon);}
                else {Tuebutton.setBackgroundResource(R.drawable.weekoff);}
            }
        });
        Wedbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Wed=changeboolen(Wed);
                Log.e("Wed", String.valueOf(Wed));
                if(Wed){Wedbutton.setBackgroundResource(R.drawable.weekon);}
                else{Wedbutton.setBackgroundResource(R.drawable.weekoff);}
            }
        });
        Thurbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thur=changeboolen(Thur);
                Log.e("Thur", String.valueOf(Thur));
                if(Thur){Thurbutton.setBackgroundResource(R.drawable.weekon);}
                else{Thurbutton.setBackgroundResource(R.drawable.weekoff);}
            }
        });
        Fributton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fri=changeboolen(Fri);
                Log.e("Fri", String.valueOf(Fri));
                if(Fri){Fributton.setBackgroundResource(R.drawable.weekon);}
                else{Fributton.setBackgroundResource(R.drawable.weekoff);}

            }
        });

        Satbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Sat=changeboolen(Sat);
                Log.e("Sat", String.valueOf(Sat));
                if(Sat){Satbutton.setBackgroundResource(R.drawable.weekon);}
                else{ Satbutton.setBackgroundResource(R.drawable.weekoff);}

            }
        });
        Sunbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Sun=changeboolen(Sun);
                Log.e("Sun", String.valueOf(Sun));
                if(Sun){Sunbutton.setBackgroundResource(R.drawable.weekon);}
                else{Sunbutton.setBackgroundResource(R.drawable.weekoff);}
            }
        });

    }
    public void OnToggleClicked(View view)
    {

        if (((ToggleButton) view).isChecked())
        {
            Toast.makeText(this, "ALARM ON", Toast.LENGTH_SHORT).show();
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, alarmTimePicker.getCurrentHour());
            calendar.set(Calendar.MINUTE, alarmTimePicker.getCurrentMinute());
            time=calendar.getTimeInMillis();
            setAlarm(calendar.getTimeInMillis());
        }
        else
        {
            alarmManager.cancel(pendingIntent);
            Toast.makeText(this, "ALARM OFF", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean changeboolen (Boolean item){
        if(item){return false;}
        else {return true;}
    }
    private void setAlarm(long time) {
        //getting the alarm manager
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        //creating a new intent specifying the broadcast receiver
        Intent i = new Intent(this, AlarmReciver.class);

        //creating a pending intent using the intent
        pendingIntent = PendingIntent.getBroadcast(this, 0, i, 0);

        //setting the repeating alarm that will be fired every day
        alarmManager.setRepeating(AlarmManager.RTC, time, AlarmManager.INTERVAL_DAY, pendingIntent);
        Toast.makeText(this, "Alarm is set", Toast.LENGTH_SHORT).show();
    }



}
