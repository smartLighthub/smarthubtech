package com.example.raviprmr21.hub20.Network;

import java.util.ArrayList;

public class ThreadConveyor extends Thread {
    private final int MAX_THREADS;
    private Object lock = new Object();
    private boolean stoped = false;
    private volatile ArrayList<Thread> thrs = new ArrayList();
    private volatile ArrayList<Thread> thrs_added = new ArrayList();

    public ThreadConveyor(int max) {
        this.MAX_THREADS = max;
    }

    public void add(Thread t) {
        synchronized (this.lock) {
            this.thrs_added.add(t);
        }
    }

    public void joinAll() {
        while (true) {
            if (this.thrs.size() > 0 || this.thrs_added.size() > 0) {
                try {
                    Thread.currentThread();
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                }
            } else {
                this.stoped = true;
                interrupt();
                return;
            }
        }
    }

    public void run() {
        while (!this.stoped) {
            for (int i = 0; i < this.thrs.size(); i++) {
                if (!((Thread) this.thrs.get(i)).isAlive()) {
                    this.thrs.remove(i);
                }
            }
            synchronized (this.lock) {
                while (this.thrs.size() < this.MAX_THREADS && this.thrs_added.size() > 0) {
                    Thread t = (Thread) this.thrs_added.get(0);
                    t.start();
                    this.thrs.add(t);
                    this.thrs_added.remove(0);
                }
            }
            try {
                Thread.currentThread();
                Thread.sleep(10);
            } catch (InterruptedException e) {
            }
        }
    }
}
