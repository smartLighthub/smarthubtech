package com.example.raviprmr21.hub20;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;
import com.example.raviprmr21.hub20.Color.colorMain;
import com.example.raviprmr21.hub20.Music.EqualizerModel;
import com.example.raviprmr21.hub20.Music.EquilizerFragment;
import com.example.raviprmr21.hub20.Music.musicMain;
import com.example.raviprmr21.hub20.Network.AplicationData;
import com.example.raviprmr21.hub20.Network.IpArpScannerNew;
import com.example.raviprmr21.hub20.Network.IpRangeDnsScannerNew;
import com.example.raviprmr21.hub20.Network.IpRangeScannerNew;
import com.example.raviprmr21.hub20.Network.IpScanner;
import com.example.raviprmr21.hub20.Network.IpScannerArp;
import com.example.raviprmr21.hub20.Network.Utils;
import com.example.raviprmr21.hub20.Network.Vals;
import com.example.raviprmr21.hub20.Scene.sceneMain;
import com.example.raviprmr21.hub20.Settings.SettingsActivity;
import com.example.raviprmr21.hub20.Settings.settingsMain;
import com.example.raviprmr21.hub20.View.AddNewNavigation_items;
import com.example.raviprmr21.hub20.View.CustomMiniDrawer;
import com.example.raviprmr21.hub20.recreation.recreationMain;
import com.mikepenz.crossfadedrawerlayout.view.CrossfadeDrawerLayout;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.interfaces.ICrossfader;
import com.mikepenz.materialdrawer.interfaces.OnCheckedChangeListener;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SwitchDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;
import com.mikepenz.materialdrawer.util.DrawerUIUtils;

import java.util.ArrayList;
import java.util.Objects;


public class MainActivity extends AppCompatActivity
        implements BottomNavigationBar.OnTabSelectedListener,
        EquilizerFragment.onCheckChangedListener {

    public static int lastSelectedPosition = 0;
    Toolbar toolbar;
    TextView titletextView;
    BottomNavigationBar bottomNavigationBar;
    private AccountHeader headerResult = null;
    private Drawer result = null;
    SecondaryDrawerItem about, itemAdd;
    CrossfadeDrawerLayout crossfadeDrawerLayout;
    public static EqualizerModel equalizerModel;
    public static boolean isEqualizerEnabled = false;
    public static String wifiState = null;
    public static int[] seekbarpos = new int[5];
    public static int presetPos;
    private int mSelectedItem;
    private static final String SELECTED_ITEM = "selected_item";
    private BroadcastReceiver onWifi;
    public Context ctx;
    public WifiManager wm;
    public static AlertDialog.Builder builder;
    AlertDialog alert;
    private ArrayList ips = new ArrayList();
    private ArrayList macs = new ArrayList();
    private int LOOPS = 1;
    private int MODE = 0;
    private int NAME_TIMEOUT = 750;
    private int REACHABLE_TIMEOUT = 750;
    private boolean USE_NETBIOS = true;
    private Activity activity_context;
    private AplicationData data;
    CustomMiniDrawer miniResult;
    String wifiScanData[][] = new String[10][10];
    int countHost = 0;
    public Thread wifithread;
    NetworkInfo mWifi;
    Handler mHandler;
    private static final int RESULT_SETTINGS = 1;
    //Toggle switch in navigation bar here!!
    private OnCheckedChangeListener onCheckedChangeListener = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(IDrawerItem drawerItem, CompoundButton buttonView, boolean isChecked) {
            if (drawerItem instanceof Nameable) {
                Log.i("material-drawer", "DrawerItem: " + ((Nameable) drawerItem).getName() + " - toggleChecked: " + isChecked);
                Toast.makeText(MainActivity.this, ((Nameable) drawerItem)
                        .getName().getText(MainActivity.this) + "ToggleChecked: " + isChecked, Toast.LENGTH_SHORT).show();
            } else {
                Log.i("material-drawer", "toggleChecked: " + isChecked);
                Toast.makeText(MainActivity.this, ((Nameable) drawerItem)
                        .getName().getText(MainActivity.this) + "ToggleChecked: " + isChecked, Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        bottomNavigationBar = (BottomNavigationBar) findViewById(R.id.bottomBar);
        bottomNavigationBar.setTabSelectedListener(this);
        titletextView = (TextView) findViewById(R.id.toolbar_title);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        titletextView.setText("Color");
        toolbar.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.grey));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (isTaskRoot()) {
            // This Activity is the only Activity, so the app wasn't running. So start the app from the beginning (redirect to MainActivity)
        } else {
            // App was already running, so just finish, which will drop the user in to the activity that was at the top of the task stack
            Intent intent = getIntent();
            Uri data = intent.getData();
            finish();
        }
        sideNavigation(savedInstanceState);
        refreshBottomNavigation();
        broadcastwifiInitialization();
        start_scan();
        wifidialog();

        mHandler = new Handler(Looper.getMainLooper()) {
            @Override

            public void handleMessage(Message message) {
                if (countHost > 0) {
                    sideNavigation(savedInstanceState);
//                   for(int i=0;i<countHost;i++) {
//                    Log.v("Host Saved", "Host ip = " + wifiScanData[i][0] + " Host vendor = " + wifiScanData[i][1]);
                }
            }
        };

    }

    private void broadcastwifiInitialization() {

        IntentFilter filter = new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION);
        onWifi = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                int extraWifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE,
                        WifiManager.WIFI_STATE_UNKNOWN);


                switch (extraWifiState) {

                    case WifiManager.WIFI_STATE_DISABLED:
                        Log.d("Wifi:", "WIFI STATE DISABLED");
                        wifiState = "DISABLED";
                        alert.show();
                        break;

                    case WifiManager.WIFI_STATE_ENABLED:
                        Log.d("Wifi:", "WIFI STATE ENABLED");
                        if (alert.isShowing()) {
                            alert.dismiss();
                        }
                        wifiState = "ENABLED";
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Do something after 5s = 5000ms
                                Log.d("Wifi ", "Wifi scan has been start");
                                start_scan();
                            }
                        }, 5000);

                        break;
                }
            }
        };
        getApplication().registerReceiver(onWifi, filter);
    }

    private void start_scan() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        mWifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        countHost = 0;

        if (mWifi == null || !mWifi.isConnected()) {

            wifidialog();
            stopScan();
            return;
        }
        if (Vals.root == 1 && this.MODE == 3) {
            this.MODE = 0;
        }
        wifithread = new Thread(new Runnable() {
            public void run() {


                ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                wm = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                long my_ip = Utils.str_to_ip(Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress()));
                long gateway_ip = Utils.str_to_ip(Formatter.formatIpAddress(wm.getDhcpInfo().gateway));
                if (MainActivity.this.MODE == 0) {
                    IpArpScannerNew scanner = new IpArpScannerNew(getApplicationContext());
                    scanner.configureArp(MainActivity.this.LOOPS, MainActivity.this.REACHABLE_TIMEOUT, 2000);
                    scanner.configureResolver(MainActivity.this.NAME_TIMEOUT, MainActivity.this.USE_NETBIOS);
                    scanner.setOnUpdateListener(new IpScanner.onUpdateListener() {
                        public void onUpdate(String ip, String mac, String name, String vendor) {
                            MainActivity.this.print_line(ip, mac, name, vendor);
                            wifiScanData[countHost][0] = ip;
                            wifiScanData[countHost][1] = vendor;
                            countHost += 1;

                        }
                    });
                    scanner.start_scan();
                    MainActivity.this.stopScan();
                } else if (MainActivity.this.MODE == 1) {
                    IpRangeScannerNew scanner2 = new IpRangeScannerNew(getApplicationContext());
                    scanner2.configureResolver(MainActivity.this.NAME_TIMEOUT, MainActivity.this.USE_NETBIOS);
                    scanner2.setOnUpdateListener(new IpScanner.onUpdateListener() {
                        public void onUpdate(String ip, String mac, String name, String vendor) {
                            MainActivity.this.print_line(ip, mac, name, vendor);
                        }
                    });
                    scanner2.start_scan();
                    MainActivity.this.stopScan();
                } else if (MainActivity.this.MODE == 2) {
                    IpRangeDnsScannerNew scanner3 = new IpRangeDnsScannerNew(getApplicationContext());
                    scanner3.configureResolver(MainActivity.this.NAME_TIMEOUT, MainActivity.this.USE_NETBIOS);
                    scanner3.setOnUpdateListener(new IpScanner.onUpdateListener() {
                        public void onUpdate(String ip, String mac, String name, String vendor) {
                            MainActivity.this.print_line(ip, mac, name, vendor);
                        }
                    });
                    scanner3.start_scan();
                    MainActivity.this.stopScan();
                } else if (MainActivity.this.MODE == 3) {
                    IpScannerArp scanner4 = new IpScannerArp(getApplicationContext());
                    scanner4.configureArp(MainActivity.this.LOOPS, MainActivity.this.REACHABLE_TIMEOUT, 2000);
                    scanner4.configureResolver(MainActivity.this.NAME_TIMEOUT, MainActivity.this.USE_NETBIOS);
                    scanner4.setOnUpdateListener(new IpScanner.onUpdateListener() {
                        public void onUpdate(String ip, String mac, String name, String vendor) {
                            MainActivity.this.print_line(ip, mac, name, vendor);
                        }
                    });
                    scanner4.start_scan();
                    MainActivity.this.stopScan();
                }
                Log.v("Counter:", String.valueOf(countHost));
                Message message = mHandler.obtainMessage(1, countHost);
                message.sendToTarget();
            }
        });
        wifithread.start();
    }

    private void stopScan() {
        try {
            this.activity_context.runOnUiThread(new Runnable() {
                public void run() {
                    MainActivity.this.data.adresses_to_global(MainActivity.this.ips, MainActivity.this.macs);
                }
            });
        } catch (NullPointerException e) {
        }
    }

    public void wifidialog() {
        builder = new AlertDialog.Builder(
                this);
        builder.setTitle("Wifi Alert");
        builder.setCancelable(false);
        builder.setMessage("Make sure that Only wifi is turned ON and all the devices are connected to a SINGLE ROUTER \nWould you like to Switch on your Wifi");
        builder.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {

                    }
                });
        builder.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        WifiManager wifi = (WifiManager) getApplication().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                        wifi.setWifiEnabled(true);

                    }
                });
        alert = builder.create();
    }


    private void print_line(String ip, String mac, String name, final String vendor) {
        try {
            final String str = ip;
            final String str2 = mac;
            final String str3 = name;
            final String str4 = vendor;
            Log.v("data:", "ip:" + str + "mac:" + str2 + "name" + str3 + "vendor:" + str4);

            this.runOnUiThread(new Runnable() {
                public void run() {
                    //add the address in sq lite table
                    //   Log.v("data:","ip"+ str + "mac:" + str2 +"name" + str3 +"vendor:"+ str4);
//                    sideNavigation(savedInstanceState);
//                    addItem(str, str4); //add item in navigation bar
                    //  if(vendor.equals("Espressif Inc.")){
                    //  wifiScanData[countHost][0]=str;
                    //   wifiScanData[countHost][1]=str4;
                    //     countHost+=1;
                    // }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sideNavigation(Bundle savedInstanceState) {
        Log.v("Side navigation", "Called");
        headerResult = new AccountHeaderBuilder().withActivity(this)
                .withHeaderBackground(R.drawable.header2)
                .withSavedInstance(savedInstanceState).build();

        about = new SecondaryDrawerItem()
                .withName("About")
                .withIcon(FontAwesome.Icon.faw_info);
        itemAdd = new SecondaryDrawerItem()
                .withName("Add")
                .withIcon(FontAwesome.Icon.faw_plus);

        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withHasStableIds(true)
                .withDrawerLayout(R.layout.crossfade_layout)
                .withScrollToTopAfterClick(true)
                .withDrawerWidthDp(72)
                .withGenerateMiniDrawer(true)
                .withAccountHeader(headerResult)
                .addStickyDrawerItems(about, itemAdd)
                .withShowDrawerOnFirstLaunch(true)
                .withSliderBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.customcolornav))
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() //this will enabled only when the item is clicked in navigation bar
                {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

                        if (drawerItem instanceof Nameable) {
                            Toast.makeText(MainActivity.this, ((Nameable) drawerItem)
                                    .getName().getText(MainActivity.this), Toast.LENGTH_SHORT).show();
                            Log.e("Item on Navigation", ((Nameable) drawerItem)
                                    .getName().getText(MainActivity.this));
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            if (Objects.equals(((Nameable) drawerItem)
                                    .getName().getText(MainActivity.this), "Add")) {
                                if (mWifi == null || !mWifi.isConnected()) {
                                    Toast.makeText(getApplication(), "Make sure your device are turned on and connected to the router", Toast.LENGTH_SHORT).show();
                                    Log.e("Wifi", "Is not connected");
                                } else {
                                    if (wifithread.isAlive()) {
                                        Toast.makeText(getApplication(), "Please wait for some time and try again later", Toast.LENGTH_SHORT).show();
                                        Log.e("Thread", "is alive");
                                    } else {
                                        start_scan();
                                    }
                                }
                            }
                        }
                        return true;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .withShowDrawerOnFirstLaunch(true)
                .build();


        for (int i = 0; i < countHost; i++) {
            Log.v("Host Saved", "Host ip = " + wifiScanData[i][0] + " Host vendor = " + wifiScanData[i][1]);
            result.addItems(new SwitchDrawerItem()
                    .withName(wifiScanData[i][0])
                    .withDescription(wifiScanData[i][1])
                    .withIcon(FontAwesome.Icon.faw_lightbulb_o)
                    .withOnCheckedChangeListener(onCheckedChangeListener));
        }

        result.setOnDrawerItemLongClickListener(new Drawer.OnDrawerItemLongClickListener() {
            @Override
            public boolean onItemLongClick(View view, int position, IDrawerItem drawerItem) {
                Toast.makeText(getApplicationContext(), "Long Press Edit", Toast.LENGTH_SHORT).show();
                new AddNewNavigation_items(MainActivity.this);
                return false;
            }
        });
        crossfadeDrawerLayout = (CrossfadeDrawerLayout) result.getDrawerLayout();
        crossfadeDrawerLayout.setMaxWidthPx(DrawerUIUtils.getOptimalDrawerWidth(this));
        CustomMiniDrawer miniResult = new CustomMiniDrawer();//adding a custom layout for enabling icons for switch drawer
        miniResult.withDrawer(result);                          //layout is added with drawer data
        View view = miniResult.build(this);
        view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparentGrey));//setting color to sidebar
        crossfadeDrawerLayout.getSmallView().addView(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        miniResult.withCrossFader(new ICrossfader() {
            @Override
            public void crossfade() {
                boolean isFaded = isCrossfaded();
                crossfadeDrawerLayout.crossfade(400);
                if (isFaded) {
                    result.getDrawerLayout().closeDrawer(GravityCompat.START);
                }
            }

            @Override
            public boolean isCrossfaded() {
                return crossfadeDrawerLayout.isCrossfaded();
            }
        });
    }

    private void ToastMSG(String message) {
        final Toast tst = Toast.makeText(getApplication(), message, Toast.LENGTH_SHORT);
        tst.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tst.cancel();
            }
        }, 1000);
    }

    @Override
    public void onCheckChanged(boolean isChecked) {
        if (isChecked) {
            try {
                isEqualizerEnabled = true;
                int pos = presetPos;
                if (pos != 0) {
                    com.example.raviprmr21.hub20.Music.musicMain.mEqualizer.usePreset((short) (pos - 1));
                } else {
                    for (short i = 0; i < 5; i++) {
                        com.example.raviprmr21.hub20.Music.musicMain.mEqualizer.setBandLevel(i, (short) seekbarpos[i]);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        equalizerModel.isEqualizerEnabled = isChecked;
    }

    private void refreshBottomNavigation() {
        bottomNavigationBar.clearAll();
        setNavigationFragment(lastSelectedPosition);
        bottomNavigationBar
                .addItem(new BottomNavigationItem(R.drawable.ic_color_pallet, "Color").setActiveColorResource(R.color.grey))
                .addItem(new BottomNavigationItem(R.drawable.ic_music_bar, "Music").setActiveColorResource(R.color.purple))
                .addItem(new BottomNavigationItem(R.drawable.ic_scene_bar, "Scene").setActiveColorResource(R.color.green))
                .addItem(new BottomNavigationItem(R.drawable.ic_recreation_bar, "Recreation").setActiveColorResource(R.color.blue))
                .addItem(new BottomNavigationItem(R.drawable.ic_setting_bar, "Settings").setActiveColorResource(R.color.red))
                .setFirstSelectedPosition(lastSelectedPosition)
                .initialise();
    }

    @Override
    public void onTabSelected(int position) {
        lastSelectedPosition = position;
        setNavigationFragment(position);
    }

    private void setNavigationFragment(int position) {
        Fragment frag = null;
        Log.d("position", String.valueOf(position));
        switch (position) {
            case 0:
                titletextView.setText("Color");
                toolbar.setBackgroundColor(0xff343a3f);
                frag = new colorMain();//calling fragment class
                break;
            case 1:
                titletextView.setText("Music");
                toolbar.setBackgroundColor(0xFF4E0086);
                frag = new musicMain();
                break;
            case 2:
                titletextView.setText("Scene");
                toolbar.setBackgroundColor(0xfb00b44f);
                frag = new sceneMain();
                break;
            case 3:
                titletextView.setText("Recreation");
                toolbar.setBackgroundColor(0xff00a6ff);
                frag = new recreationMain();
                break;
            case 4:
                Intent SettingsActivity = new Intent(this, SettingsActivity.class);
                startActivityForResult(SettingsActivity, RESULT_SETTINGS);
                break;
        }
        mSelectedItem = position;
        if (frag != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.contentContainer, frag, frag.getTag()).commit();
        }
    }


    @Override
    public void onTabUnselected(int i) {

    }

    @Override
    public void onTabReselected(int i) {

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //add the values which need to be saved from the drawer to the bundle
        outState = result.saveInstanceState(outState);
        //add the values which need to be saved from the accountHeader to the bundle
        outState = headerResult.saveInstanceState(outState);
        outState.putInt(SELECTED_ITEM, mSelectedItem);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        //handle the back press :D close the drawer first and if the drawer is closed close the activity
        super.onPause();
        Log.v("MainPause", "the activity is paused");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_SETTINGS:
                bottomNavigationBar.clearAll();
                setNavigationFragment(0);
                bottomNavigationBar
                        .addItem(new BottomNavigationItem(R.drawable.ic_color_pallet, "Color").setActiveColorResource(R.color.grey))
                        .addItem(new BottomNavigationItem(R.drawable.ic_music_bar, "Music").setActiveColorResource(R.color.purple))
                        .addItem(new BottomNavigationItem(R.drawable.ic_scene_bar, "Scene").setActiveColorResource(R.color.green))
                        .addItem(new BottomNavigationItem(R.drawable.ic_recreation_bar, "Recreation").setActiveColorResource(R.color.blue))
                        .addItem(new BottomNavigationItem(R.drawable.ic_setting_bar, "Settings").setActiveColorResource(R.color.red))
                        .setFirstSelectedPosition(0)
                        .initialise();
                break;
        }
    }
    @Override

    public void onBackPressed() {
        //handle the back press :D close the drawer first and if the drawer is closed close the activity
        if (result != null && result.isDrawerOpen()) {
            result.closeDrawer();
        } else {
            super.onBackPressed();
        }
        // if onWifi is not register then it can return null to avoid this we need to init onWifi!=null
        if (onWifi != null) {
            getApplication().unregisterReceiver(onWifi);
            onWifi = null;
        }
    }

}
