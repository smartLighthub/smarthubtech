package com.example.raviprmr21.hub20.Scene;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.raviprmr21.hub20.Model.ColorModel;
import com.example.raviprmr21.hub20.R;
import com.example.raviprmr21.hub20.View.ColorDialog;
import com.transitionseverywhere.Slide;
import com.transitionseverywhere.TransitionManager;

import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by raviprmr21 on 3/6/17.
 */

public class sceneMain extends android.support.v4.app.Fragment
        implements View.OnClickListener, View.OnLongClickListener,ColorDialog.ColorDialogListener {
    private LinearLayout horizontalOuterLayout ,horizontalOuterLayout1 ,horizontalOuterLayout2 , horizontalOuterLayout3 ,horizontalOuterLayout4 ,horizontalOuterLayout5 ,horizontalOuterLayout6 , horizontalOuterLayout7 ,horizontalOuterLayout8 ,horizontalOuterLayout9 ,horizontalOuterLayout10 , horizontalOuterLayout11 ,horizontalOuterLayout12;
    public HorizontalScrollView horizontalScrollview, horizontalScrollview1, horizontalScrollview2, horizontalScrollview3, horizontalScrollview4, horizontalScrollview5, horizontalScrollview6, horizontalScrollview7, horizontalScrollview8, horizontalScrollview9,horizontalScrollview10,horizontalScrollview11, horizontalScrollview12;
    Button party,meditation, reading ,romantic, cinema ,emergency,color1, color2,color3,color4,color5,color6,animpositive ,animnegativive,custom1,custom2 ,custom3 ,custom4 ,custom5 ,custom6,customimage1,customimage2,customimage3, customimage4,customimage5,customimage6 ;
    public Integer[] imageNameArray,scrollPos={0,0,0,0,0,0,0,0,0,0,0,0,0}, scrollMax={0,0,0,0,0,0,0,0,0,0,0,0,0} ,coloranimchange, transitiontimeseek={0,0,0,0,0,0,0,0,0,0,0,0};
    public String[] scenesselection={"Party mode activated", "Meditation mode activated", "Reading mode activated", "Romantic mode activated" , "Cinema mode activated", "Emergency mode activated", "Custom1 mode activated","Custom2 mode activated","Custom3 mode activated", "Custom4 mode activated","Custom5 mode activated","Custom6 mode activated"},
            sceneskey={"partymodecolors","meditationmodecolors","readingmodecolors","romanticmodecolors", "cinemamodecolors","emergencymodecolors","custom1modecolors","custom2modecolors", "custom3modecolors","custom4modecolors","custom5modecolors","custom6modecolors"},
            scenetext={"Party Mode","Meditation Mode","Reading Mode","Romantic Mode","Cinema Mode", "Emergency Mode","custom1themename","custom2themename","custom3themename", "custom4themename","custom5themename","custom6themename"};
    public TextView  modeselect,customtext1,customtext2,customtext3,customtext4,customtext5,customtext6;
    private TimerTask scrollerSchedule;
    public Timer scrollTimer=null;
    public SeekBar transitionstime ;
    public int touchedRGB = 0;
    public int timedefault;
    public ViewGroup transitionsContainer;
    public LinearLayout text;
    int Animode=0;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return getActivity().getLayoutInflater()
                .cloneInContext(new ContextThemeWrapper(getActivity(), R.style.Setting_Style))
                .inflate(R.layout.scene_main, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        horizontalScrollview = (HorizontalScrollView) v.findViewById(R.id.horiztonal_scrollview_id);
        horizontalOuterLayout = (LinearLayout) v.findViewById(R.id.horiztonal_outer_layout_id);
        horizontalScrollview1 = (HorizontalScrollView) v.findViewById(R.id.horiztonal_scrollview_id1);
        horizontalOuterLayout1 = (LinearLayout) v.findViewById(R.id.horiztonal_outer_layout_id1);
        horizontalScrollview2 = (HorizontalScrollView) v.findViewById(R.id.horiztonal_scrollview_id2);
        horizontalOuterLayout2 = (LinearLayout) v.findViewById(R.id.horiztonal_outer_layout_id2);
        horizontalScrollview3 = (HorizontalScrollView) v.findViewById(R.id.horiztonal_scrollview_id3);
        horizontalOuterLayout3 = (LinearLayout) v.findViewById(R.id.horiztonal_outer_layout_id3);
        horizontalScrollview4 = (HorizontalScrollView) v.findViewById(R.id.horiztonal_scrollview_id4);
        horizontalOuterLayout4 = (LinearLayout) v.findViewById(R.id.horiztonal_outer_layout_id4);
        horizontalScrollview5 = (HorizontalScrollView) v.findViewById(R.id.horiztonal_scrollview_id5);
        horizontalOuterLayout5 = (LinearLayout) v.findViewById(R.id.horiztonal_outer_layout_id5);
        horizontalScrollview6 = (HorizontalScrollView) v.findViewById(R.id.horiztonal_scrollview_id6);
        horizontalOuterLayout6 = (LinearLayout) v.findViewById(R.id.horiztonal_outer_layout_id6);
        horizontalScrollview7 = (HorizontalScrollView) v.findViewById(R.id.horiztonal_scrollview_id7);
        horizontalOuterLayout7 = (LinearLayout) v.findViewById(R.id.horiztonal_outer_layout_id7);
        horizontalScrollview8 = (HorizontalScrollView) v.findViewById(R.id.horiztonal_scrollview_id8);
        horizontalOuterLayout8 = (LinearLayout) v.findViewById(R.id.horiztonal_outer_layout_id8);
        horizontalScrollview9 = (HorizontalScrollView) v.findViewById(R.id.horiztonal_scrollview_id9);
        horizontalOuterLayout9 = (LinearLayout) v.findViewById(R.id.horiztonal_outer_layout_id9);
        horizontalScrollview10 = (HorizontalScrollView) v.findViewById(R.id.horiztonal_scrollview_id10);
        horizontalOuterLayout10 = (LinearLayout) v.findViewById(R.id.horiztonal_outer_layout_id10);
        horizontalScrollview11 = (HorizontalScrollView) v.findViewById(R.id.horiztonal_scrollview_id11);
        horizontalOuterLayout11 = (LinearLayout) v.findViewById(R.id.horiztonal_outer_layout_id11);
        horizontalScrollview12 = (HorizontalScrollView) v.findViewById(R.id.horiztonal_scrollview_id12);
        horizontalOuterLayout12 = (LinearLayout) v.findViewById(R.id.horiztonal_outer_layout_id12);


        party = (Button) v.findViewById(R.id.BtnParty);
        meditation = (Button) v.findViewById(R.id.BtnMeditation);
        reading = (Button) v.findViewById(R.id.BtnReading);
        romantic = (Button) v.findViewById(R.id.BtnRomantic);
        cinema = (Button) v.findViewById(R.id.BtnCinema);
        emergency = (Button) v.findViewById(R.id.BtnEmergency);
        color1 = (Button) v.findViewById(R.id.animationcolor1);
        color2 = (Button) v.findViewById(R.id.animationcolor2);
        color3 = (Button) v.findViewById(R.id.animationcolor3);
        color4 = (Button) v.findViewById(R.id.animationcolor4);
        color5 = (Button) v.findViewById(R.id.animationcolor5);
        color6 = (Button) v.findViewById(R.id.animationcolor6);
        animpositive = (Button) v.findViewById(R.id.okbutton);
        animnegativive = (Button) v.findViewById(R.id.cancelbutton);
        transitionstime = (SeekBar) v.findViewById(R.id.seektransitiontime);
        modeselect = (TextView) v.findViewById(R.id.modeselected);
        custom1 = (Button) v.findViewById(R.id.BtnCustom1);
        custom2 = (Button) v.findViewById(R.id.BtnCustom2);
        custom3 = (Button) v.findViewById(R.id.BtnCustom3);
        custom4 = (Button) v.findViewById(R.id.BtnCustom4);
        custom5 = (Button) v.findViewById(R.id.BtnCustom5);
        custom6 = (Button) v.findViewById(R.id.BtnCustom6);
        customtext1 = (TextView) v.findViewById(R.id.textCustom1);
        customtext2 = (TextView) v.findViewById(R.id.textCustom2);
        customtext3 = (TextView) v.findViewById(R.id.textCustom3);
        customtext4 = (TextView) v.findViewById(R.id.textCustom4);
        customtext5 = (TextView) v.findViewById(R.id.textCustom5);
        customtext6 = (TextView) v.findViewById(R.id.textCustom6);
        customimage1 = (Button) v.findViewById(R.id.Imagecustom1);
        customimage2 = (Button) v.findViewById(R.id.Imagecustom2);
        customimage3 = (Button) v.findViewById(R.id.Imagecustom3);
        customimage4 = (Button) v.findViewById(R.id.Imagecustom4);
        customimage5 = (Button) v.findViewById(R.id.Imagecustom5);
        customimage6 = (Button) v.findViewById(R.id.Imagecustom6);
        transitionsContainer = (ViewGroup) v.findViewById(R.id.Scene);
        text = (LinearLayout) transitionsContainer.findViewById(R.id.text);

        Button modebtn = (Button) v.findViewById(R.id.mode_btn);
        Button SceneBtn =(Button)v.findViewById(R.id.ADD_Scene);

        modebtn.setOnClickListener(this);
        SceneBtn.setOnClickListener(this);

        party.setOnClickListener(this);
        meditation.setOnClickListener(this);
        reading.setOnClickListener(this);
        romantic.setOnClickListener(this);
        cinema.setOnClickListener(this);
        emergency.setOnClickListener(this);

        party.setOnLongClickListener(this);
        meditation.setOnLongClickListener(this);
        reading.setOnLongClickListener(this);
        romantic.setOnLongClickListener(this);
        cinema.setOnLongClickListener(this);
        emergency.setOnLongClickListener(this);

        color1.setOnClickListener(this);
        color2.setOnClickListener(this);
        color3.setOnClickListener(this);
        color4.setOnClickListener(this);
        color5.setOnClickListener(this);
        color6.setOnClickListener(this);

        custom1.setOnClickListener(this);
        custom2.setOnClickListener(this);
        custom3.setOnClickListener(this);
        custom4.setOnClickListener(this);
        custom5.setOnClickListener(this);
        custom6.setOnClickListener(this);

        custom1.setOnLongClickListener(this);
        custom2.setOnLongClickListener(this);
        custom3.setOnLongClickListener(this);
        custom4.setOnLongClickListener(this);
        custom5.setOnLongClickListener(this);
        custom6.setOnLongClickListener(this);


        animpositive.setOnClickListener(this);
        animnegativive.setOnClickListener(this);

        transitionstime.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String mode = String.valueOf(modeselect.getText());

                if(mode.equals("Party Mode")) {
                    transitiontimeseek[0]=progress;
                }

                if(mode.equals("Meditation Mode")) {
                    transitiontimeseek[1]=progress;
                }

                if(mode.equals("Reading Mode")) {
                    transitiontimeseek[2]=progress;
                }

                if(mode.equals("Romantic Mode")) {
                    transitiontimeseek[3]=progress;
                }

                if(mode.equals("Cinema Mode")) {
                    transitiontimeseek[4]=progress;
                }

                if(mode.equals("Emergency Mode")) {
                    transitiontimeseek[5]=progress;
                }
            }


            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        SharedPreferences share = getActivity().getSharedPreferences("customvisibility", Context.MODE_PRIVATE);
        String mem = share.getString("customvisibility", "");
        String[] color = getArray(mem);
        if (mem.equals("")) {

        } else
        {
            for (int n = 0; n < color.length; n++) {
                if (Integer.valueOf(color[n]) == 1) {
                    if (n == 0) {
                        custom1.setVisibility(View.VISIBLE);
                        customimage1.setVisibility(View.VISIBLE);
                        customtext1.setVisibility(View.VISIBLE);
                        share = getActivity().getSharedPreferences(scenetext[6], Context.MODE_PRIVATE);
                        mem = share.getString(scenetext[6], "");
                        customtext1.setText(mem);

                    }
                    if (n == 1) {
                        custom2.setVisibility(View.VISIBLE);
                        customtext2.setVisibility(View.VISIBLE);
                        customimage2.setVisibility(View.VISIBLE);
                        share = getActivity().getSharedPreferences(scenetext[7], Context.MODE_PRIVATE);
                        mem = share.getString(scenetext[7], "");
                        customtext2.setText(mem);
                    }
                    if (n == 2) {
                        custom3.setVisibility(View.VISIBLE);
                        customtext3.setVisibility(View.VISIBLE);
                        customimage3.setVisibility(View.VISIBLE);
                        share = getActivity().getSharedPreferences(scenetext[8], Context.MODE_PRIVATE);
                        mem = share.getString(scenetext[8], "");
                        customtext3.setText(mem);
                    }
                    if (n == 3) {
                        custom4.setVisibility(View.VISIBLE);
                        customtext4.setVisibility(View.VISIBLE);
                        customimage4.setVisibility(View.VISIBLE);
                        share = getActivity().getSharedPreferences(scenetext[9], Context.MODE_PRIVATE);
                        mem = share.getString(scenetext[9], "");
                        customtext4.setText(mem);
                    }
                    if (n == 4) {
                        custom5.setVisibility(View.VISIBLE);
                        customtext5.setVisibility(View.VISIBLE);
                        customimage5.setVisibility(View.VISIBLE);
                        share = getActivity().getSharedPreferences(scenetext[10], Context.MODE_PRIVATE);
                        mem = share.getString(scenetext[10], "");
                        customtext5.setText(mem);
                    }
                    if (n == 5) {
                        custom6.setVisibility(View.VISIBLE);
                        customimage6.setVisibility(View.VISIBLE);
                        customtext6.setVisibility(View.VISIBLE);
                        share = getActivity().getSharedPreferences(scenetext[11], Context.MODE_PRIVATE);
                        mem = share.getString(scenetext[11], "");
                        customtext6.setText(mem);
                    }
                }
            }
        }
    }

    public void onClick(View v) {
        ColorDialog colorFragment = new ColorDialog();
        switch (v.getId()){
            case R.id.ADD_Scene:
                addMain dialogFragment = new addMain();
                dialogFragment.show(getFragmentManager(), "AddTheme");
                break;
            case R.id.mode_btn:
                modeMain mModeMain = new modeMain();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
               fragmentTransaction.replace(R.id.contentContainer, mModeMain);
                fragmentTransaction.commit();
                break;
            case R.id.animationcolor1:Animode = 1;
                colorFragment.setCallBack(this);
                colorFragment.show(getChildFragmentManager(), "ColorSelect");
                break;
            case R.id.animationcolor2:Animode = 2;
                colorFragment.setCallBack(this);
                colorFragment.show(getChildFragmentManager(), "ColorSelect");
                break;
            case R.id.animationcolor3:Animode = 3;
                colorFragment.setCallBack(this);
                colorFragment.show(getChildFragmentManager(), "ColorSelect");
                break;
            case R.id.animationcolor4:Animode = 4;
                colorFragment.setCallBack(this);
                colorFragment.show(getChildFragmentManager(), "ColorSelect");
                break;
            case R.id.animationcolor5:Animode = 5;
                colorFragment.setCallBack(this);
                colorFragment.show(getChildFragmentManager(), "ColorSelect");
                break;
            case R.id.animationcolor6:Animode = 6;
                colorFragment.setCallBack(this);
                colorFragment.show(getChildFragmentManager(), "ColorSelect");
                break;

            case R.id.BtnParty:SceneOnClick(0);
                break;
            case R.id.BtnMeditation:SceneOnClick(1);
                break;
            case R.id.BtnReading:SceneOnClick(2);
                break;
            case R.id.BtnRomantic:SceneOnClick(3);
                break;
            case R.id.BtnCinema:SceneOnClick(4);
                break;
            case R.id.BtnEmergency:SceneOnClick(5);
                break;

            case R.id.BtnCustom1:SceneOnClick(6);
                break;
            case R.id.BtnCustom2:SceneOnClick(7);
                break;
            case R.id.BtnCustom3:SceneOnClick(8);
                break;
            case R.id.BtnCustom4:SceneOnClick(9);
                break;
            case R.id.BtnCustom5:SceneOnClick(10);
                break;
            case R.id.BtnCustom6:SceneOnClick(11);
                break;
            case R.id.okbutton:
                String mode = String.valueOf(modeselect.getText());

                if(mode.equals(scenetext[0])) {Save(0,mode);}
                if(mode.equals(scenetext[1])) {Save(1,mode);}
                if(mode.equals(scenetext[2])) {Save(2,mode);}
                if(mode.equals(scenetext[3])) {Save(3,mode);}
                if(mode.equals(scenetext[4])) {Save(4,mode);}
                if(mode.equals(scenetext[5])) {Save(5,mode);}

                TransitionManager.beginDelayedTransition(transitionsContainer, new Slide(Gravity.BOTTOM));
                text.setVisibility(View.GONE );
                break;
            case R.id.cancelbutton:
                TransitionManager.beginDelayedTransition(transitionsContainer, new Slide(Gravity.BOTTOM));
                text.setVisibility(View.GONE );
                break;

        }
    }
    @Override
    public boolean onLongClick(View view) {

        switch (view.getId()){

            case R.id.BtnParty:ScenesOnLongClick(0);
                break;
            case R.id.BtnMeditation:ScenesOnLongClick(1);
                break;
            case R.id.BtnReading:ScenesOnLongClick(2);
                break;
            case R.id.BtnRomantic:ScenesOnLongClick(3);
                break;
            case R.id.BtnCinema:ScenesOnLongClick(4);
                break;
            case R.id.BtnEmergency:ScenesOnLongClick(5);
                break;

            case R.id.BtnCustom1:ScenesOnLongClick(6);
                break;
            case R.id.BtnCustom2:ScenesOnLongClick(7);
                break;
            case R.id.BtnCustom3:ScenesOnLongClick(8);
                break;
            case R.id.BtnCustom4:ScenesOnLongClick(9);
                break;
            case R.id.BtnCustom5:ScenesOnLongClick(10);
                break;
            case R.id.BtnCustom6:ScenesOnLongClick(11);
                break;
        }
        return true;
    }

    public void  intialization(){
        imageNameArray = new Integer[]{0xffffffff};
        horizontalScrollview.setHorizontalScrollBarEnabled(false);
        addImagesToView(0);

        ViewTreeObserver vto = horizontalOuterLayout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                horizontalOuterLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                getScrollMaxAmount(0);
                startAutoScrolling(0);
                horizontalScrollview.setVisibility(View.INVISIBLE);
            }
        });

    }
    @Override
    public void onResume() {
        super.onResume();
        intialization();
    }

    public void getScrollMaxAmount(int select) {

        Integer[] actualWidth = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        actualWidth[select] = (horizontalOuterLayout.getMeasuredWidth() - 512);
        scrollMax[select] = actualWidth[select];

    }

    public void startAutoScrolling(final int select) {

        if (scrollTimer == null) {
            scrollTimer = new Timer();
            final Runnable Timer_Tick = new Runnable() {
                public void run() {
                    moveScrollView(select);
                }
            };

            if (scrollerSchedule != null) {
                scrollerSchedule.cancel();
                scrollerSchedule = null;
            }
            scrollerSchedule = new TimerTask() {
                @Override
                public void run() {
                    if(getActivity() != null){
                        getActivity().runOnUiThread(Timer_Tick);
                    }
                }
            };
            scrollTimer.schedule(scrollerSchedule, 5, 5);
        }
    }

    public void moveScrollView(int select) {
        if (select == 0) {
            scrollPos[select] = (int) (horizontalScrollview.getScrollX() + 1.0);
            if (scrollPos[select] >= scrollMax[select]) {
                scrollPos[select] = 0;
            }
            horizontalScrollview.scrollTo(scrollPos[select], 0);
        }

        if (select == 1) {
            scrollPos[select] = (int) (horizontalScrollview1.getScrollX() + 1.0);
            if (scrollPos[select] >= scrollMax[select]) {
                scrollPos[select] = 0;
            }
            horizontalScrollview1.scrollTo(scrollPos[select], 0);
        }

        if (select == 2) {
            scrollPos[select] = (int) (horizontalScrollview2.getScrollX() + 1.0);
            if (scrollPos[select] >= scrollMax[select]) {
                scrollPos[select] = 0;
            }
            horizontalScrollview2.scrollTo(scrollPos[select], 0);
        }

        if (select == 3) {
            scrollPos[select] = (int) (horizontalScrollview3.getScrollX() + 1.0);
            if (scrollPos[select] >= scrollMax[select]) {
                scrollPos[select] = 0;
            }
            horizontalScrollview3.scrollTo(scrollPos[select], 0);
        }

        if (select == 4) {

            scrollPos[select] = (int) (horizontalScrollview4.getScrollX() + 1.0);
            if (scrollPos[select] >= scrollMax[select]) {
                scrollPos[select] = 0;
            }
            horizontalScrollview4.scrollTo(scrollPos[select], 0);
        }

        if (select == 5) {
            scrollPos[select] = (int) (horizontalScrollview5.getScrollX() + 1.0);
            if (scrollPos[select] >= scrollMax[select]) {
                scrollPos[select] = 0;
            }
            horizontalScrollview5.scrollTo(scrollPos[select], 0);
        }
        if (select == 6) {
            scrollPos[select] = (int) (horizontalScrollview6.getScrollX() + 1.0);
            if (scrollPos[select] >= scrollMax[select]) {
                scrollPos[select] = 0;
            }
            horizontalScrollview6.scrollTo(scrollPos[select], 0);
        }
        if (select == 7) {
            scrollPos[select] = (int) (horizontalScrollview7.getScrollX() + 1.0);
            if (scrollPos[select] >= scrollMax[select]) {
                scrollPos[select] = 0;
            }
            horizontalScrollview7.scrollTo(scrollPos[select], 0);
        }

        if (select == 8) {
            scrollPos[select] = (int) (horizontalScrollview8.getScrollX() + 1.0);
            if (scrollPos[select] >= scrollMax[select]) {
                scrollPos[select] = 0;
            }
            horizontalScrollview8.scrollTo(scrollPos[select], 0);
        }

        if (select == 9) {
            scrollPos[select] = (int) (horizontalScrollview9.getScrollX() + 1.0);
            if (scrollPos[select] >= scrollMax[select]) {
                scrollPos[select] = 0;
            }
            horizontalScrollview9.scrollTo(scrollPos[select], 0);
        }
        if (select == 10) {
            scrollPos[select] = (int) (horizontalScrollview10.getScrollX() + 1.0);
            if (scrollPos[select] >= scrollMax[select]) {
                scrollPos[select] = 0;
            }
            horizontalScrollview10.scrollTo(scrollPos[select], 0);
        }
        if (select == 11) {
            scrollPos[select] = (int) (horizontalScrollview11.getScrollX() + 1.0);
            if (scrollPos[select] >= scrollMax[select]) {
                scrollPos[select] = 0;
            }
            horizontalScrollview11.scrollTo(scrollPos[select], 0);
        }
        if (select == 12) {
            scrollPos[select] = (int) (horizontalScrollview12.getScrollX() + 1.0);
            if (scrollPos[select] >= scrollMax[select]) {
                scrollPos[select] = 0;
            }
            horizontalScrollview12.scrollTo(scrollPos[select], 0);
        }
    }

    public void addImagesToView(int select) {

        for (int i = 0; i < 50; i++) {
            for (int j = 0; j < imageNameArray.length; j++) {
                final Button imageButton = new Button(getActivity());


                imageButton.setBackgroundColor(imageNameArray[j]);
                imageButton.setTag(j);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(256, 256);
                params.setMargins(0, 25, 0, 25);
                imageButton.setLayoutParams(params);
                if (select == 0) {
                    horizontalOuterLayout.addView(imageButton);
                }
                if (select == 1) {
                    horizontalOuterLayout1.addView(imageButton);
                }
                if (select == 2) {
                    horizontalOuterLayout2.addView(imageButton);
                }
                if (select == 3) {
                    horizontalOuterLayout3.addView(imageButton);
                }
                if (select == 4) {
                    horizontalOuterLayout4.addView(imageButton);
                }
                if (select == 5) {
                    horizontalOuterLayout5.addView(imageButton);
                }
                if (select == 6) {
                    horizontalOuterLayout6.addView(imageButton);
                }
                if (select == 7) {
                    horizontalOuterLayout7.addView(imageButton);
                }
                if (select == 8) {
                    horizontalOuterLayout8.addView(imageButton);
                }
                if (select == 9) {
                    horizontalOuterLayout9.addView(imageButton);
                }
                if (select == 10) {
                    horizontalOuterLayout10.addView(imageButton);
                }
                if (select == 11) {
                    horizontalOuterLayout11.addView(imageButton);
                }
                if (select == 12) {
                    horizontalOuterLayout12.addView(imageButton);
                }


            }
        }
    }

    public void SceneOnClick(int mode) {
        Toast.makeText(getActivity(), scenesselection[mode], Toast.LENGTH_SHORT).show();
        imageNameArray = new Integer[]{0,0,0,0,0,0};
        scrollTimer = null;
        horizontalScrollview.setVisibility(View.INVISIBLE);
        horizontalScrollview1.setVisibility(View.INVISIBLE);
        horizontalScrollview2.setVisibility(View.INVISIBLE);
        horizontalScrollview3.setVisibility(View.INVISIBLE);
        horizontalScrollview4.setVisibility(View.INVISIBLE);
        horizontalScrollview5.setVisibility(View.INVISIBLE);
        horizontalScrollview6.setVisibility(View.INVISIBLE);
        horizontalScrollview7.setVisibility(View.INVISIBLE);
        horizontalScrollview8.setVisibility(View.INVISIBLE);
        horizontalScrollview9.setVisibility(View.INVISIBLE);
        horizontalScrollview10.setVisibility(View.INVISIBLE);
        horizontalScrollview11.setVisibility(View.INVISIBLE);
        horizontalScrollview12.setVisibility(View.INVISIBLE);

        if(mode==0) {
            horizontalScrollview1.setVisibility(View.VISIBLE);
            horizontalScrollview1.scrollTo(0, 0);
        }else if (mode==1){
            horizontalScrollview2.setVisibility(View.VISIBLE);
            horizontalScrollview2.scrollTo(0, 0);
        }else if (mode==2){
            horizontalScrollview3.setVisibility(View.VISIBLE);
            horizontalScrollview3.scrollTo(0, 0);
        }else if (mode==3){
            horizontalScrollview4.setVisibility(View.VISIBLE);
            horizontalScrollview4.scrollTo(0, 0);
        }else if (mode==4){
            horizontalScrollview5.setVisibility(View.VISIBLE);
            horizontalScrollview5.scrollTo(0, 0);
        }else if (mode==5) {
            horizontalScrollview6.setVisibility(View.VISIBLE);
            horizontalScrollview6.scrollTo(0, 0);
        } else if (mode==6){
            horizontalScrollview7.setVisibility(View.VISIBLE);
            horizontalScrollview7.scrollTo(0, 0);
        } else if (mode==7){
            horizontalScrollview8.setVisibility(View.VISIBLE);
            horizontalScrollview8.scrollTo(0, 0);
        } else if (mode==8){
            horizontalScrollview9.setVisibility(View.VISIBLE);
            horizontalScrollview9.scrollTo(0, 0);
        } else if (mode==9){
            horizontalScrollview10.setVisibility(View.VISIBLE);
            horizontalScrollview10.scrollTo(0, 0);
        } else if (mode==10){
            horizontalScrollview11.setVisibility(View.VISIBLE);
            horizontalScrollview11.scrollTo(0, 0);
        }else if (mode==12){
            horizontalScrollview12.setVisibility(View.VISIBLE);
            horizontalScrollview12.scrollTo(0, 0);
        }

        SharedPreferences share = getActivity().getSharedPreferences(sceneskey[mode], Context.MODE_PRIVATE);
        String mem = share.getString(sceneskey[mode], "");
        String[] color = getArray(mem); //see below for the getArray() method

        if (mem.equals("")) {
            imageNameArray = new Integer[]{0xff343a3f, 0xff346a3f, 0xff359a3f, 0xff443a3f, 0xff643a3f, 0xff843a3f};
        } else {
            for (int n = 0; n < color.length; n++) {
                imageNameArray[n] = Integer.valueOf(color[n]);
            }
        }

        horizontalScrollview1.setHorizontalScrollBarEnabled(false);
        addImagesToView(mode+1);
        getScrollMaxAmount(mode+1);
        startAutoScrolling(mode+1);
    }

    public void ScenesOnLongClick(int mode){
        TransitionManager.beginDelayedTransition(transitionsContainer, new Slide(Gravity.BOTTOM));
        text.setVisibility(View.VISIBLE);

        imageNameArray = new Integer[]{0,0,0,0,0,0};
        SharedPreferences share = getActivity().getSharedPreferences(sceneskey[mode], Context.MODE_PRIVATE);
        String mem = share.getString(sceneskey[mode], "");
        String[] color = getArray(mem); //see below for the getArray() method

        if(mode>5){
            share = getActivity().getSharedPreferences(scenetext[mode], Context.MODE_PRIVATE);
            mem = share.getString(scenetext[mode], "");
            modeselect.setText(mem);
        }else{ modeselect.setText(scenetext[mode]); }

        if (mem.equals("")) {
            imageNameArray = new Integer[]{0xff343a3f, 0xff346a3f, 0xff359a3f, 0xff443a3f, 0xff643a3f, 0xff843a3f};
            coloranimchange=imageNameArray;
        } else {
            for (int n = 0; n < color.length; n++) {
                imageNameArray[n] = Integer.valueOf(color[n]);
            }
            coloranimchange=imageNameArray;
        }

        share = getActivity().getSharedPreferences("transitiontime", Context.MODE_PRIVATE);
        mem = share.getString("transitiontime", "");
        color = getArray(mem);

        if (mem.equals("")) {
            transitionstime.setProgress(timedefault);
        } else {
            transitionstime.setProgress(Integer.valueOf(color[mode]));
        }


        color1.setBackgroundColor(imageNameArray[0]);
        color2.setBackgroundColor(imageNameArray[1]);
        color3.setBackgroundColor(imageNameArray[2]);
        color4.setBackgroundColor(imageNameArray[3]);
        color5.setBackgroundColor(imageNameArray[4]);
        color6.setBackgroundColor(imageNameArray[5]);

    }

    public static String[] getArray(String input) {
        return input.split("\\|\\$\\|SEPARATOR\\|\\$\\|");
    }

    @Override
    public void onDialogPositiveClick(ColorModel model) {
        touchedRGB =model.getTouchRGBDATA();

        if (touchedRGB != 0) {
            if (Animode == 1) {
                color1.setBackgroundColor(touchedRGB);
                coloranimchange[0] = touchedRGB;
            } else if (Animode == 2) {
                color2.setBackgroundColor(touchedRGB);
                coloranimchange[1] = touchedRGB;
            } else if (Animode == 3) {
                color3.setBackgroundColor(touchedRGB);
                coloranimchange[2] = touchedRGB;
            } else if (Animode == 4) {
                color4.setBackgroundColor(touchedRGB);
                coloranimchange[3] = touchedRGB;
            } else if (Animode == 5) {
                color5.setBackgroundColor(touchedRGB);
                coloranimchange[4] = touchedRGB;
            } else {
                color6.setBackgroundColor(touchedRGB);
                coloranimchange[5] = touchedRGB;
            }
        }
    }

    public void Save(int modeseleced,String mode){
        Toast.makeText(getActivity(),mode,Toast.LENGTH_SHORT).show();
        String save = "";
        SharedPreferences share = getActivity().getSharedPreferences(sceneskey[modeseleced], Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = share.edit();

        String save1="";
        SharedPreferences share1 = getActivity().getSharedPreferences("transitiontime", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor1 = share1.edit();

        for(int n=0; n<transitiontimeseek.length; n++)
        {
            save1 = save1 + transitiontimeseek[n] + "|$|SEPARATOR|$|";
        }
        editor1.putString("transitiontime", save1);
        editor1.apply();
        Toast.makeText(getActivity(), save1, Toast.LENGTH_SHORT).show();


        for (int n = 0; n < coloranimchange.length; n++) {
            save = save + coloranimchange[n] + "|$|SEPARATOR|$|";
        }
        editor.putString(sceneskey[modeseleced], save);
        editor.apply();
        Toast.makeText(getActivity(), save, Toast.LENGTH_SHORT).show();

    }
}