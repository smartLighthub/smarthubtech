package com.example.raviprmr21.hub20.Network;

import android.app.Application;

import java.util.ArrayList;

/**
 * Created by ${RaviParmar} on ${18/08/17}.
 */

    public class AplicationData extends Application {

    private ArrayList<String> ips = new ArrayList<>();
        private String[] ipsr = new String[0];
        private ArrayList<String> macs = new ArrayList<String>();
        private String[] macsr = new String[0];

        public boolean get_dev() {
            return false;
        }

        public String[] to_arr(ArrayList<String> arr) {
            String[] res = new String[arr.size()];
            for (int i = arr.size() - 1; i >= 0; i--) {
                res[i] = (String) arr.get(i);
            }
            return res;
        }

        public String[] get_ips() {
            return (String[]) this.ipsr.clone();
        }

        public String[] get_macs() {
            return (String[]) this.macsr.clone();
        }

        public void adresses_to_global(ArrayList<String> IPS, ArrayList<String> MACS) {
            for (int j = 0; j < IPS.size(); j++) {
                if (!this.ips.contains(IPS.get(j))) {
                    this.ips.add(IPS.get(j));
                    if (MACS != null) {
                        this.macs.add(MACS.get(j));
                    }
                }
            }
            this.ipsr = to_arr(this.ips);
            this.macsr = to_arr(this.macs);
        }

}
