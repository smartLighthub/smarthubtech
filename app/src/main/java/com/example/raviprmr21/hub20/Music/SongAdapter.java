package com.example.raviprmr21.hub20.Music;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.raviprmr21.hub20.Music.imageLoader.ImageLoader;
import com.example.raviprmr21.hub20.R;
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;

import java.util.ArrayList;

/**
 * Created by raviprmr21 on 13/6/17.
 */

class SongAdapter extends RecyclerView.Adapter<SongAdapter.SongHolder>
		implements FastScrollRecyclerView.SectionedAdapter{

	static ArrayList<LocalTrackModel> _songs;
	private Context context;
	private ImageLoader imgLoader;
	static int position;
	static public int size;
	private  ClickListener clickListener;

	@NonNull
	@Override
	public String getSectionName(int position) {
		final LocalTrackModel c =_songs.get(position);
		String titlename = c.getTitle();
		return titlename.substring(0,1); //displays it on cursor first char
	}

	class SongHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		TextView songname,artistname;
		ImageView Albumpic;
		SongHolder(View itemView) {
			super(itemView);
			songname = (TextView)itemView.findViewById(R.id.SongName);
			artistname =(TextView)itemView.findViewById(R.id.ArtistName);
			Albumpic=(ImageView)itemView.findViewById(R.id.AlbumPic);
			itemView.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			if(clickListener !=null)
			{
				clickListener.itemClicked(v,getAdapterPosition());
			}
		}
	}

	SongAdapter(Context context, ArrayList<LocalTrackModel> _songs) {
		SongAdapter._songs = _songs;
		this.context = context;
		imgLoader = new ImageLoader(context);
	}

	@Override
	public SongHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
		View myView = LayoutInflater.from(context).inflate(R.layout.localmusic_rowview,viewGroup,false);
		myView.setBackgroundColor(Color.TRANSPARENT); // make all the list transparent
		return new SongHolder(myView);
	}

	@Override
	public void onBindViewHolder(final SongHolder songHolder, final int i) {
		final LocalTrackModel c =_songs.get(songHolder.getAdapterPosition());
		songHolder.songname.setText(c.getTitle());
		songHolder.artistname.setText(c.getArtist());
		imgLoader.DisplayImage(c.getPath(),songHolder.Albumpic);
		size =getItemCount();
	}
	void setClickListener(ClickListener clickListener){

		this.clickListener =clickListener;
	}

	@Override
	public int getItemCount() {
		return _songs.size();
	}

	@Override
	public int getItemViewType(int position) {
		return super.getItemViewType(position);
	}

	interface  ClickListener {
		void itemClicked(View v, int position);
	}
}