package com.example.raviprmr21.hub20.View;

import com.example.raviprmr21.hub20.Model.CustomMiniDrawerItem;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.MiniDrawer;
import com.mikepenz.materialdrawer.interfaces.OnCheckedChangeListener;
import com.mikepenz.materialdrawer.model.MiniProfileDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SwitchDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

/**
 * Created by RaviParmar on 24.07.17.
 */
public class CustomMiniDrawer extends MiniDrawer {

    @Override
    public IDrawerItem generateMiniDrawerItem(IDrawerItem drawerItem) {
        if (drawerItem instanceof PrimaryDrawerItem) {
            return new CustomMiniDrawerItem((PrimaryDrawerItem) drawerItem);
        } else if (drawerItem instanceof SwitchDrawerItem) {
            return new CustomMiniDrawerItem((SwitchDrawerItem) drawerItem);
        }
        return null;
    }

    @Override
    public int getMiniDrawerType(IDrawerItem drawerItem) {
        if (drawerItem instanceof MiniProfileDrawerItem) {
            return PROFILE;
        } else if (drawerItem instanceof CustomMiniDrawerItem) {
            return ITEM;
        }
        return -1;
    }

}
